<?php
/*
Plugin Name: Mikago Core
Plugin URI: http://themeforest.net/user/blue_design
Description: Plugin to contain shortcodes and custom post types of the mikago theme.
Author: blue_design
Author URI: http://themeforest.net/user/blue_design/portfolio
Version: 1.0
Text Domain: mikago-core
*/

if( ! function_exists( 'mikago_block_direct_access' ) ) {
	function mikago_block_direct_access() {
		if( ! defined( 'ABSPATH' ) ) {
			exit( 'Forbidden' );
		}
	}
}

// Plugin URL
define( 'MIKAGO_PLUGIN_URL', plugins_url( '/', __FILE__ ) );

// Plugin PATH
define( 'MIKAGO_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'MIKAGO_PLUGIN_ASTS', MIKAGO_PLUGIN_URL . 'assets' );
define( 'MIKAGO_PLUGIN_IMGS', MIKAGO_PLUGIN_ASTS . '/images' );
define( 'MIKAGO_PLUGIN_INC', MIKAGO_PLUGIN_PATH . 'include' );

// DIRECTORY SEPARATOR
define ( 'DS' , DIRECTORY_SEPARATOR );

// Mikago Elementor Shortcode Path
define( 'MIKAGO_EM_SHORTCODE_BASE_PATH', MIKAGO_PLUGIN_PATH . 'elementor/' );
define( 'MIKAGO_EM_SHORTCODE_PATH', MIKAGO_EM_SHORTCODE_BASE_PATH . 'widgets/' );

/**
 * Check if Codestar Framework is Active or Not!
 */
function mikago_framework_active() {
  return ( defined( 'CS_VERSION' ) ) ? true : false;
}

/* MIKAGO_THEME_NAME_PLUGIN */
define('MIKAGO_THEME_NAME_PLUGIN', 'Mikago' );

// Initial File
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('mikago-core/mikago-core.php')) {

	// Custom Post Type
  require_once( MIKAGO_PLUGIN_INC . '/custom-post-type.php' );

  if ( is_plugin_active('kingcomposer/kingcomposer.php') ) {

    define( 'MIKAGO_KC_SHORTCODE_BASE_PATH', MIKAGO_PLUGIN_PATH . 'kc/' );
    define( 'MIKAGO_KC_SHORTCODE_PATH', MIKAGO_KC_SHORTCODE_BASE_PATH . 'shortcodes/' );
    // Shortcodes
    require_once( MIKAGO_KC_SHORTCODE_BASE_PATH . '/kc-setup.php' );
    require_once( MIKAGO_KC_SHORTCODE_BASE_PATH . '/library.php' );
  }

  // Theme Custom Shortcode
  require_once( MIKAGO_PLUGIN_INC . '/custom-shortcodes/theme-shortcodes.php' );
  require_once( MIKAGO_PLUGIN_INC . '/custom-shortcodes/custom-shortcodes.php' );

  // Importer
  require_once( MIKAGO_PLUGIN_INC . '/demo/importer.php' );


  if (class_exists('WP_Widget') && is_plugin_active('codestar-framework/cs-framework.php') ) {
    // Widgets

    require_once( MIKAGO_PLUGIN_INC . '/widgets/nav-widget.php' );
    require_once( MIKAGO_PLUGIN_INC . '/widgets/recent-posts.php' );
    require_once( MIKAGO_PLUGIN_INC . '/widgets/footer-posts.php' );
    require_once( MIKAGO_PLUGIN_INC . '/widgets/text-widget.php' );
    require_once( MIKAGO_PLUGIN_INC . '/widgets/widget-extra-fields.php' );

    // Elementor
    if(file_exists( MIKAGO_EM_SHORTCODE_BASE_PATH . '/em-setup.php' ) ){
      require_once( MIKAGO_EM_SHORTCODE_BASE_PATH . '/em-setup.php' );
      require_once( MIKAGO_EM_SHORTCODE_BASE_PATH . 'lib/fields/icons.php' );
    }
  }

  add_action('wp_enqueue_scripts', 'mikago_plugin_enqueue_scripts');
  function mikago_plugin_enqueue_scripts() {
    wp_enqueue_script('plugin-scripts', MIKAGO_PLUGIN_ASTS.'/plugin-scripts.js', array('jquery'), '', true);
  }

}

// Extra functions
require_once( MIKAGO_PLUGIN_INC . '/theme-functions.php' );