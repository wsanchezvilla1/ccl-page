<?php
/*
 * All Custom Shortcode for [theme_name] theme.
 * Author & Copyright: blue_design
 * URL: http://themeforest.net/user/blue_design
 */

if( ! function_exists( 'mikago_shortcodes' ) ) {
  function mikago_shortcodes( $options ) {

    $options       = array();

    /* Topbar Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Topbar Shortcodes', 'mikago'),
      'shortcodes' => array(

        // Topbar item
        array(
          'name'          => 'mikago_widget_topbars',
          'title'         => esc_html__('Topbar info', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_widget_topbar',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
            
          ),
          'clone_fields'  => array(

            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'mikago'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link Text', 'mikago'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'mikago'),
            ),
            array(
              'id'        => 'open_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'yes'     => esc_html__('Yes', 'mikago'),
              'no'     => esc_html__('No', 'mikago'),
            ),

          ),

        ),
       

      ),
    );

    /* Header Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Header Shortcodes', 'mikago'),
      'shortcodes' => array(

        // header Social
        array(
          'name'          => 'mikago_header_socials',
          'title'         => esc_html__('Header Social', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_header_social',
          'clone_title'   => esc_html__('Add New Social', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'mikago')
            ),
            array(
              'id'        => 'social_icon_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Color', 'mikago'),
            ),
            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'title'     => esc_html__('Social Link', 'mikago')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'yes'     => esc_html__('Yes', 'mikago'),
              'no'     => esc_html__('No', 'mikago'),
            ),

          ),

        ),
        // header Social End

        // header Middle Infos
        array(
          'name'          => 'mikago_header_middle_infos',
          'title'         => esc_html__('Header Middle Info', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_header_middle_info',
          'clone_title'   => esc_html__('Add New Info', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'mikago')
            ),
            array(
              'id'        => 'social_icon_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Color', 'mikago'),
            ),
            array(
              'id'        => 'address_text',
              'type'      => 'text',
              'title'     => esc_html__('Address Text', 'mikago')
            ),
            array(
              'id'        => 'address_desc',
              'type'      => 'text',
              'title'     => esc_html__('Address Details', 'mikago')
            ),
          ),

        ),
        // header Middle Infos End



      ),
    );

    /* Content Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Content Shortcodes', 'mikago'),
      'shortcodes' => array(

        // Spacer
        array(
          'name'          => 'vc_empty_space',
          'title'         => esc_html__('Spacer', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'height',
              'type'      => 'text',
              'title'     => esc_html__('Height', 'mikago'),
              'attributes' => array(
                'placeholder'     => '20px',
              ),
            ),

          ),
        ),
        // Spacer

        // Social Icons
        array(
          'name'          => 'mikago_socials',
          'title'         => esc_html__('Social Icons', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_social',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

            // Colors
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Colors', 'mikago')
            ),
            array(
              'id'        => 'icon_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Color', 'mikago'),
              'wrap_class' => 'column_third',
            ),
            array(
              'id'        => 'icon_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Icon Hover Color', 'mikago'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '!=', 'style-three'),
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Backrgound Color', 'mikago'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '!=', 'style-one'),
            ),
            array(
              'id'        => 'bg_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Backrgound Hover Color', 'mikago'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '==', 'style-two'),
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'mikago'),
              'wrap_class' => 'column_third',
              'dependency'  => array('social_select', '==', 'style-three'),
            ),

            // Icon Size
            array(
              'id'        => 'icon_size',
              'type'      => 'text',
              'title'     => esc_html__('Icon Size', 'mikago'),
              'wrap_class' => 'column_full',
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => esc_html__('Link', 'mikago')
            ),
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'mikago')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'on_text'     => esc_html__('Yes', 'mikago'),
              'off_text'     => esc_html__('No', 'mikago'),
            ),

          ),

        ),
        // Social Icons

        // Useful Links
        array(
          'name'          => 'mikago_useful_links',
          'title'         => esc_html__('Useful Links', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_useful_link',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'column_width',
              'type'      => 'select',
              'title'     => esc_html__('Column Width', 'mikago'),
              'options'        => array(
                'full-width' => esc_html__('One Column', 'mikago'),
                'half-width' => esc_html__('Two Column', 'mikago'),
                'third-width' => esc_html__('Three Column', 'mikago'),
              ),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'title_link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'mikago')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'on_text'     => esc_html__('Yes', 'mikago'),
              'off_text'     => esc_html__('No', 'mikago'),
            ),
            array(
              'id'        => 'link_title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'mikago')
            ),

          ),

        ),
        // Useful Links

        // Simple Image List
        array(
          'name'          => 'mikago_image_lists',
          'title'         => esc_html__('Simple Image List', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_image_list',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'get_image',
              'type'      => 'upload',
              'title'     => esc_html__('Image', 'mikago')
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
              'title'     => esc_html__('Link', 'mikago')
            ),
            array(
              'id'    => 'open_tab',
              'type'  => 'switcher',
              'std'   => false,
              'title' => esc_html__('Open link to new tab?', 'mikago')
            ),

          ),

        ),
        // Simple Image List

        // Simple Link
        array(
          'name'          => 'mikago_simple_link',
          'title'         => esc_html__('Simple Link', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'link_style',
              'type'      => 'select',
              'title'     => esc_html__('Link Style', 'mikago'),
              'options'        => array(
                'link-underline' => esc_html__('Link Underline', 'mikago'),
                'link-arrow-right' => esc_html__('Link Arrow (Right)', 'mikago'),
                'link-arrow-left' => esc_html__('Link Arrow (Left)', 'mikago'),
              ),
            ),
            array(
              'id'        => 'link_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Icon', 'mikago'),
              'value'      => 'fa fa-caret-right',
              'dependency'  => array('link_style', '!=', 'link-underline'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link Text', 'mikago'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'mikago'),
              'attributes' => array(
                'placeholder'     => 'http://',
              ),
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'on_text'     => esc_html__('Yes', 'mikago'),
              'off_text'     => esc_html__('No', 'mikago'),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

            // Normal Mode
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Normal Mode', 'mikago')
            ),
            array(
              'id'        => 'text_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Text Color', 'mikago'),
              'wrap_class' => 'column_half el-hav-border',
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'mikago'),
              'wrap_class' => 'column_half el-hav-border',
              'dependency'  => array('link_style', '==', 'link-underline'),
            ),
            // Hover Mode
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Hover Mode', 'mikago')
            ),
            array(
              'id'        => 'text_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Text Hover Color', 'mikago'),
              'wrap_class' => 'column_half el-hav-border',
            ),
            array(
              'id'        => 'border_hover_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Hover Color', 'mikago'),
              'wrap_class' => 'column_half el-hav-border',
              'dependency'  => array('link_style', '==', 'link-underline'),
            ),

            // Size
            array(
              'type'    => 'notice',
              'class'   => 'info',
              'content' => esc_html__('Font Sizes', 'mikago')
            ),
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => esc_html__('Text Size', 'mikago'),
              'attributes' => array(
                'placeholder'     => 'Eg: 14px',
              ),
            ),

          ),
        ),
        // Simple Link

        // Blockquotes
        array(
          'name'          => 'mikago_blockquote',
          'title'         => esc_html__('Blockquote', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'blockquote_style',
              'type'      => 'select',
              'title'     => esc_html__('Blockquote Style', 'mikago'),
              'options'        => array(
                '' => esc_html__('Select Blockquote Style', 'mikago'),
                'style-one' => esc_html__('Style One', 'mikago'),
                'style-two' => esc_html__('Style Two', 'mikago'),
              ),
            ),
            array(
              'id'        => 'text_size',
              'type'      => 'text',
              'title'     => esc_html__('Text Size', 'mikago'),
            ),
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
            array(
              'id'        => 'content_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Content Color', 'mikago'),
            ),
            array(
              'id'        => 'left_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Left Border Color', 'mikago'),
            ),
            array(
              'id'        => 'border_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Border Color', 'mikago'),
            ),
            array(
              'id'        => 'bg_color',
              'type'      => 'color_picker',
              'title'     => esc_html__('Background Color', 'mikago'),
            ),
            // Content
            array(
              'id'        => 'content',
              'type'      => 'textarea',
              'title'     => esc_html__('Content', 'mikago'),
            ),

          ),

        ),
        // Blockquotes

      ),
    );

    /* Widget Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Widget Shortcodes', 'mikago'),
      'shortcodes' => array(

        // widget Contact info
        array(
          'name'          => 'mikago_widget_contact_info',
          'title'         => esc_html__('Contact info', 'mikago'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
             array(
              'id'        => 'image_url',
              'type'      => 'image',
              'title'     => esc_html__('Image background', 'mikago'),
            ),
            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'mikago'),
            ),
            array(
              'id'        => 'desc',
              'type'      => 'textarea',
              'title'     => esc_html__('Description', 'mikago'),
            ),
            array(
              'id'        => 'mobile_text',
              'type'      => 'text',
              'title'     => esc_html__('Mobile Number', 'mikago'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link text', 'mikago'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'mikago'),
            ),

          ),
        ),

       // About widget Block
        array(
          'name'          => 'mikago_about_widget',
          'title'         => esc_html__('About Widget Block', 'mikago'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'mikago'),
            ),
            array(
              'id'        => 'image_url',
              'type'      => 'image',
              'title'     => esc_html__('About Block Image', 'mikago'),
            ),
            array(
              'id'        => 'desc',
              'type'      => 'textarea',
              'title'     => esc_html__('Description', 'mikago'),
            ),
            array(
              'id'        => 'link_text',
              'type'      => 'text',
              'title'     => esc_html__('Link text', 'mikago'),
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Link', 'mikago'),
            ),

          ),
        ),


      // Service Contact Widget
        array(
          'name'          => 'mikago_service_widget_contacts',
          'title'         => esc_html__('Service Feature Widget', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_service_widget_contact',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
            array(
              'id'        => 'contact_title',
              'type'      => 'text',
              'title'     => esc_html__('Title', 'mikago')
            ),
          ),
          'clone_fields'  => array(
           
             array(
              'id'        => 'info',
              'type'      => 'text',
              'title'     => esc_html__('Contact Info', 'mikago')
            ),

          ),

        ),
      // Service Contact Widget End
        // widget download-widget
        array(
          'name'          => 'mikago_download_widgets',
          'title'         => esc_html__('Download Widget', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_download_widget',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
          ),
          'clone_fields'  => array(

            array(
              'id'        => 'download_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Download Icon', 'mikago')
            ),
            array(
              'id'        => 'title',
              'type'      => 'text',
              'title'     => esc_html__('Download Title', 'mikago')
            ),
            array(
              'id'        => 'link',
              'type'      => 'text',
              'title'     => esc_html__('Download Link', 'mikago')
            ),

          ),

        ),

      ),
    );

    /* Footer Shortcodes */
    $options[]     = array(
      'title'      => esc_html__('Footer Shortcodes', 'mikago'),
      'shortcodes' => array(

        // Footer Menus
        array(
          'name'          => 'mikago_footer_menus',
          'title'         => esc_html__('Footer Menu Links', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_footer_menu',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(

            array(
              'id'        => 'menu_title',
              'type'      => 'text',
              'title'     => esc_html__('Menu Title', 'mikago')
            ),
            array(
              'id'        => 'menu_link',
              'type'      => 'text',
              'title'     => esc_html__('Menu Link', 'mikago')
            ),
            array(
              'id'        => 'target_tab',
              'type'      => 'switcher',
              'title'     => esc_html__('Open New Tab?', 'mikago'),
              'on_text'     => esc_html__('Yes', 'mikago'),
              'off_text'     => esc_html__('No', 'mikago'),
            ),

          ),

        ),
        // Footer Menus
        array(
          'name'          => 'footer_infos',
          'title'         => esc_html__('footer logo and Text', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'footer_info',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(
            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),
            array(
              'id'        => 'footer_logo',
              'type'      => 'image',
              'title'     => esc_html__('Footer logo', 'mikago'),
            ),
            array(
              'id'        => 'desc',
              'type'      => 'textarea',
              'title'     => esc_html__('Description', 'mikago'),
            ),
            
          ),
          'clone_fields'  => array(
            array(
              'id'        => 'social_icon',
              'type'      => 'icon',
              'title'     => esc_html__('Social Icon', 'mikago')
            ),
            array(
              'id'        => 'social_link',
              'type'      => 'text',
              'title'     => esc_html__('Social Link', 'mikago')
            ),
          ),

        ),

      // footer contact info
      array(
        'name'          => 'mikago_footer_contact_infos',
        'title'         => esc_html__('Contact info', 'mikago'),
        'view'          => 'clone',
        'clone_id'      => 'mikago_footer_contact_info',
        'clone_title'   => esc_html__('Add New', 'mikago'),
        'fields'        => array(

          array(
            'id'        => 'custom_class',
            'type'      => 'text',
            'title'     => esc_html__('Custom Class', 'mikago'),
          ),
          array(
            'id'        => 'title',
            'type'      => 'textarea',
            'title'     => esc_html__('Heading Title', 'mikago'),
          ),

        ),
        'clone_fields'  => array(

          array(
            'id'        => 'item_title',
            'type'      => 'text',
            'title'     => esc_html__('Contact info item title', 'mikago')
          ),
          array(
            'id'        => 'item',
            'type'      => 'text',
            'title'     => esc_html__('Contact info item', 'mikago')
          ),
        ),

      ),

      // footer Address
       array(
          'name'          => 'mikago_footer_address_item',
          'title'         => esc_html__('Address', 'mikago'),
          'view'          => 'clone',
          'clone_id'      => 'mikago_footer_address_items',
          'clone_title'   => esc_html__('Add New', 'mikago'),
          'fields'        => array(

            array(
              'id'        => 'custom_class',
              'type'      => 'text',
              'title'     => esc_html__('Custom Class', 'mikago'),
            ),

          ),
          'clone_fields'  => array(
            array(
              'id'        => 'item',
              'type'      => 'text',
              'title'     => esc_html__('Address item', 'mikago')
            ),
          ),
        ),

      ),
    );

  return $options;

  }
  add_filter( 'cs_shortcode_options', 'mikago_shortcodes' );
}