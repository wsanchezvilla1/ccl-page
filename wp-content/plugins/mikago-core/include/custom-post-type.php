<?php

/**
 * Initialize Custom Post Type - Mikago Theme
 */

function mikago_custom_post_type() {

  // Service - Start
  $service_slug = 'service';
  $services = esc_html__('Service', 'mikago-core');

  // Register custom post type - Service
  register_post_type('service',
    array(
      'labels' => array(
        'name' => $services,
        'singular_name' => sprintf(esc_html__('%s Post', 'mikago-core' ), $services),
        'all_items' => sprintf(esc_html__('%s', 'mikago-core' ), $services),
        'add_new' => esc_html__('Add New', 'mikago-core') ,
        'add_new_item' => sprintf(esc_html__('Add New %s', 'mikago-core' ), $services),
        'edit' => esc_html__('Edit', 'mikago-core') ,
        'edit_item' => sprintf(esc_html__('Edit %s', 'mikago-core' ), $services),
        'new_item' => sprintf(esc_html__('New %s', 'mikago-core' ), $services),
        'view_item' => sprintf(esc_html__('View %s', 'mikago-core' ), $services),
        'search_items' => sprintf(esc_html__('Search %s', 'mikago-core' ), $services),
        'not_found' => esc_html__('Nothing found in the Database.', 'mikago-core') ,
        'not_found_in_trash' => esc_html__('Nothing found in Trash', 'mikago-core') ,
        'parent_item_colon' => ''
      ) ,
      'public' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'show_ui' => true,
      'query_var' => true,
      'menu_position' => 11,
      'menu_icon' => 'dashicons-welcome-add-page',
      'rewrite' => array(
        'slug' => $service_slug,
        'with_front' => false
      ),
      'has_archive' => true,
      'capability_type' => 'post',
      'hierarchical' => true,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
        'excerpt',
        'revisions',
      )
    )
  );
  // Service - End

  // Add Category Taxonomy for our Custom Post Type - Service
  register_taxonomy(
    'service_category',
    'service',
    array(
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'labels' => array(
        'name' => sprintf(esc_html__( '%s Categories', 'shiptar-core' ), $services),
        'singular_name' => sprintf(esc_html__('%s Category', 'shiptar-core'), $services),
        'search_items' =>  sprintf(esc_html__( 'Search %s Categories', 'shiptar-core'), $services),
        'all_items' => sprintf(esc_html__( 'All %s Categories', 'shiptar-core'), $services),
        'parent_item' => sprintf(esc_html__( 'Parent %s Category', 'shiptar-core'), $services),
        'parent_item_colon' => sprintf(esc_html__( 'Parent %s Category:', 'shiptar-core'), $services),
        'edit_item' => sprintf(esc_html__( 'Edit %s Category', 'shiptar-core'), $services),
        'update_item' => sprintf(esc_html__( 'Update %s Category', 'shiptar-core'), $services),
        'add_new_item' => sprintf(esc_html__( 'Add New %s Category', 'shiptar-core'), $services),
        'new_item_name' => sprintf(esc_html__( 'New %s Category Name', 'shiptar-core'), $services)
      ),
      'rewrite' => array( 'slug' => $service_slug . '_cat' ),
    )
  );

  // Project - Start
  $project_slug = 'project';
  $projects = esc_html__('Project', 'mikago-core');

  // Register custom post type - project
  register_post_type('project',
    array(
      'labels' => array(
        'name' => $projects,
        'singular_name' => sprintf(esc_html__('%s Post', 'mikago-core' ), $projects),
        'all_items' => sprintf(esc_html__('%s', 'mikago-core' ), $projects),
        'add_new' => esc_html__('Add New', 'mikago-core') ,
        'add_new_item' => sprintf(esc_html__('Add New %s', 'mikago-core' ), $projects),
        'edit' => esc_html__('Edit', 'mikago-core') ,
        'edit_item' => sprintf(esc_html__('Edit %s', 'mikago-core' ), $projects),
        'new_item' => sprintf(esc_html__('New %s', 'mikago-core' ), $projects),
        'view_item' => sprintf(esc_html__('View %s', 'mikago-core' ), $projects),
        'search_items' => sprintf(esc_html__('Search %s', 'mikago-core' ), $projects),
        'not_found' => esc_html__('Nothing found in the Database.', 'mikago-core') ,
        'not_found_in_trash' => esc_html__('Nothing found in Trash', 'mikago-core') ,
        'parent_item_colon' => ''
      ) ,
      'public' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => false,
      'show_ui' => true,
      'query_var' => true,
      'menu_position' => 12,
      'menu_icon' => 'dashicons-portfolio',
      'rewrite' => array(
        'slug' => $project_slug,
        'with_front' => false
      ),
      'has_archive' => true,
      'capability_type' => 'post',
      'hierarchical' => true,
      'supports' => array(
        'title',
        'editor',
        'thumbnail',
      )
    )
  );
  // project - End

 // Add Category Taxonomy for our Custom Post Type - Project
  register_taxonomy(
    'project_category',
    'project',
    array(
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_admin_column' => true,
      'show_in_nav_menus' => true,
      'labels' => array(
        'name' => sprintf(esc_html__( '%s Categories', 'shiptar-core' ), $projects),
        'singular_name' => sprintf(esc_html__('%s Category', 'shiptar-core'), $projects),
        'search_items' =>  sprintf(esc_html__( 'Search %s Categories', 'shiptar-core'), $projects),
        'all_items' => sprintf(esc_html__( 'All %s Categories', 'shiptar-core'), $projects),
        'parent_item' => sprintf(esc_html__( 'Parent %s Category', 'shiptar-core'), $projects),
        'parent_item_colon' => sprintf(esc_html__( 'Parent %s Category:', 'shiptar-core'), $projects),
        'edit_item' => sprintf(esc_html__( 'Edit %s Category', 'shiptar-core'), $projects),
        'update_item' => sprintf(esc_html__( 'Update %s Category', 'shiptar-core'), $projects),
        'add_new_item' => sprintf(esc_html__( 'Add New %s Category', 'shiptar-core'), $projects),
        'new_item_name' => sprintf(esc_html__( 'New %s Category Name', 'shiptar-core'), $projects)
      ),
      'rewrite' => array( 'slug' => $project_slug . '_cat' ),
    )
  );


}


// After Theme Setup
function mikago_custom_flush_rules() {
	// Enter post type function, so rewrite work within this function
	mikago_custom_post_type();
	// Flush it
	flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'mikago_custom_flush_rules');
add_action('init', 'mikago_custom_post_type');


/* ---------------------------------------------------------------------------
 * Custom columns - Service
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-service_columns", "mikago_service_edit_columns");
function mikago_service_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = esc_html__('Title', 'mikago-core' );
  $new_columns['thumbnail'] = esc_html__('Image', 'mikago-core' );
  $new_columns['date'] = esc_html__('Date', 'mikago-core' );

  return $new_columns;
}

add_action('manage_service_posts_custom_column', 'mikago_manage_service_columns', 10, 2);
function mikago_manage_service_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}


/* ---------------------------------------------------------------------------
 * Custom columns - case
 * --------------------------------------------------------------------------- */
add_filter("manage_edit-project_columns", "mikago_project_edit_columns");
function mikago_project_edit_columns($columns) {
  $new_columns['cb'] = '<input type="checkbox" />';
  $new_columns['title'] = esc_html__('Title', 'mikago-core' );
  $new_columns['thumbnail'] = esc_html__('Image', 'mikago-core' );
  $new_columns['date'] = esc_html__('Date', 'mikago-core' );

  return $new_columns;
}

add_action('manage_project_posts_custom_column', 'mikago_manage_project_columns', 10, 2);
function mikago_manage_project_columns( $column_name ) {
  global $post;

  switch ($column_name) {

    /* If displaying the 'Image' column. */
    case 'thumbnail':
      echo get_the_post_thumbnail( $post->ID, array( 100, 100) );
    break;

    /* Just break out of the switch statement for everything else. */
    default :
      break;
    break;

  }
}
