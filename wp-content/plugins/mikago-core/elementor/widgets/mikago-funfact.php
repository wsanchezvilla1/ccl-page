<?php
/*
 * Elementor Mikago Funfact Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Funfact extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_funfact';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Funfact', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-sort-numeric-asc';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Funfact widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_funfact'];
	}
	
	/**
	 * Register Mikago Funfact widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_funfact',
			[
				'label' => esc_html__( 'Funfact Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'section_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'mikago-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'mikago-core'),
                'flaticon' => esc_html__('Flaticon', 'mikago-core'),
            ],
            'default' => 'flaticon',
        ]
    );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'mikago-core' ),
            'type' => Controls_Manager::ICON,
            'options' => mikago_themify_icons(),
            'include' => mikago_include_themify_icons(),
            'default' => 'ti-panel',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'mikago-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => mikago_flaticons(),
            'include'    => mikago_include_flaticons(),
            'default'    => 'flaticon-staff',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'funfact_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'funfact_number',
			[
				'label' => esc_html__( 'Funfact Number', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '250', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type funfact Number here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'funfact_plus',
			[
				'label' => esc_html__( 'Funfact Plus/Percentage', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '+', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type funfact Plus/Percentage here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'funfactItems_groups',
			[
				'label' => esc_html__( 'Funfact Items', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'funfact_title' => esc_html__( 'Funfact', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ funfact_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .fun-fact-section h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Funfact Number
		$this->start_controls_section(
			'funfact_number_style',
			[
				'label' => esc_html__( 'Number', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_number_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .fun-fact-section .grid h3, .fun-fact-section .grid h3 span',
			]
		);
		$this->add_control(
			'number_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section .grid h3, .fun-fact-section .grid h3 span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'number_padding',
			[
				'label' => __( 'Number Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section .grid h3, .fun-fact-section .grid h3 span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Funfact Title
		$this->start_controls_section(
			'funfact_title_style',
			[
				'label' => esc_html__( 'Funfact Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'ntrsvt_funfact_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .fun-fact-section .grid h3 + p',
			]
		);
		$this->add_control(
			'funfact_title',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section .grid h3 + p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'funfact_title_padding',
			[
				'label' => __( 'Number Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .fun-fact-section .grid h3 + p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Funfact widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$funfactItems_groups = !empty( $settings['funfactItems_groups'] ) ? $settings['funfactItems_groups'] : [];

		$section_title = !empty( $settings['section_title'] ) ? $settings['section_title'] : '';

		// Turn output buffer on
		ob_start();
		?>
		<section class="fun-fact-section">
				<?php if( $section_title ) { echo '<h2>'.esc_html( $section_title ).'</h2>'; } ?>
		    <div class="fun-fact-grids clearfix">
					<?php 	// Group Param Output
						if( is_array( $funfactItems_groups ) && !empty( $funfactItems_groups ) ){
						foreach ( $funfactItems_groups as $each_item ) { 

						$icon_type = !empty( $each_item['icon_type'] ) ? $each_item['icon_type'] : '';
						$funfact_title = !empty( $each_item['funfact_title'] ) ? $each_item['funfact_title'] : '';
						$funfact_number = !empty( $each_item['funfact_number'] ) ? $each_item['funfact_number'] : '';
						$funfact_plus = !empty( $each_item['funfact_plus'] ) ? $each_item['funfact_plus'] : '';

						switch ($each_item['icon_type']) {
					        case 'ticon':
					            $icon = !empty($each_item['ticon']) ? $each_item['ticon'] : '';
					            break;
					        case 'flaticon':
					            $icon = !empty($each_item['flaticon']) ? $each_item['flaticon'] : '';
					            break;
					    }

					   $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';

						?>
		        <div class="grid">
			        	<div class="icon">
	                 <?php if( $icon_class ) { echo '<i '.$icon_class.'></i>'; } ?>
	              </div>
		            <div class="info">
		            	<?php 
		            		if( $funfact_number ) { echo '<h3><span class="odometer" data-count="'.esc_html( $funfact_number ).'">'.esc_html__( '00','mikago-core' ).'</span>'.esc_html( $funfact_plus ).'</h3>'; } 
		            		if( $funfact_title ) { echo '<p>'.esc_html( $funfact_title ).'</p>'; } 
		            	?>
		            </div>
		        </div>
		      <?php }
					} ?>
		    </div>
		</section>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Funfact widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Funfact() );