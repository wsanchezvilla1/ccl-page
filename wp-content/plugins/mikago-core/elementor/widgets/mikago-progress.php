<?php
/*
 * Elementor Mikago Progress Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Progress extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_progress';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Progress', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-bar-chart';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Progress widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_progress'];
	}
	
	/**
	 * Register Mikago Progress widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_progress',
			[
				'label' => esc_html__( 'Progress Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'sub_title',
			[
				'label' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Sub Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'progress_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'progress_content',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'default' => esc_html__( 'your content text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'progress_title',
			[
				'label' => esc_html__( 'Progress Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'progress_parcent',
			[
				'label' => esc_html__( 'Progress Percentage', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '85%', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type progress percentage here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'progressItems_groups',
			[
				'label' => esc_html__( 'Progress Items', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'progress_title' => esc_html__( 'Progress', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ progress_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .achievement-container .section-title-s2 span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .achievement-container .section-title-s2 span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Sub Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .achievement-container .section-title-s2 span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .achievement-container .section-title-s2 h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .achievement-container .section-title-s2 h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .achievement-container .section-title-s2 h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .achievement-container p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .achievement-container p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .achievement-container p ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Progress Title
		$this->start_controls_section(
			'section_skil_style',
			[
				'label' => esc_html__( 'Progress Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_progress_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq-achievement-section .skills h6',
			]
		);
		$this->add_control(
			'progress_title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-achievement-section .skills h6' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Progress Color
		$this->start_controls_section(
			'section_skil_color_style',
			[
				'label' => esc_html__( 'Progress Color', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'progress_bg_color',
			[
				'label' => esc_html__( 'Prograss BG Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-achievement-section .skills .progress-bar,.faq-achievement-section .skills .progress-bar:after' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .faq-achievement-section .progresss .progress > span' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'progress_color',
			[
				'label' => esc_html__( 'Prograss Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-achievement-section .skills .progress' => 'background-color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'progress_parcentage_color',
			[
				'label' => esc_html__( 'Prograss Parcentage  Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-achievement-section .skills .progress > span' => 'color: {{VALUE}};'
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Progress widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$progressItems_groups = !empty( $settings['progressItems_groups'] ) ? $settings['progressItems_groups'] : [];

		$sub_title = !empty( $settings['sub_title'] ) ? $settings['sub_title'] : '';
		$progress_title = !empty( $settings['progress_title'] ) ? $settings['progress_title'] : '';
		$progress_content = !empty( $settings['progress_content'] ) ? $settings['progress_content'] : '';


		// Turn output buffer on
		ob_start();
		?>
		<div class="faq-achievement-section">
			<div class="achievement-container">
	   	 <div class="section-title-s2">
	       <?php 
          if( $sub_title ) { echo '<span>'.esc_html( $sub_title ).'</span>'; } 
          if( $progress_title ) { echo '<h2>'.esc_html( $progress_title ).'</h2>'; } 
         ?>
	    	</div>
		   <?php 
          if( $progress_content ) { echo '<p>'.esc_html( $progress_content ).'</p>'; } 
        ?>
		    <div class="skills">
		       <?php 	// Group Param Output
							if( is_array( $progressItems_groups ) && !empty( $progressItems_groups ) ){
							foreach ( $progressItems_groups as $each_items ) { 
							$progress_title = !empty( $each_items['progress_title'] ) ? $each_items['progress_title'] : '';
							$progress_parcent = !empty( $each_items['progress_parcent'] ) ? $each_items['progress_parcent'] : '';
						?>
		        <div class="skill">
		            <?php if( $progress_title ) { echo '<h6>'.esc_html( $progress_title ).'</h6>'; }
		            if ( $progress_parcent ) { ?>
		            <div class="progress">
		                <div class="progress-bar" data-percent="<?php echo esc_attr( $progress_parcent ) ?>"></div>
		            </div>
		            <?php } ?>
		        </div>
						<?php }
					} ?>
		    	</div>
				</div>
			</div>
			<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Progress widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Progress() );