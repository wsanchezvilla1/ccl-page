<?php
/*
 * Elementor Mikago Feature Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Feature extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_feature';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Feature', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-file-signature';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Feature widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_feature'];
	}
	
	/**
	 * Register Mikago Feature widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_feature',
			[
				'label' => esc_html__( 'Feature Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'feature_style',
			[
				'label' => esc_html__( 'Feature Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
					'style-three' => esc_html__( 'Style Three', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your feature style.', 'mikago-core' ),
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'mikago-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'mikago-core'),
                'flaticon' => esc_html__('Flaticon', 'mikago-core'),
            ],
            'default' => 'flaticon',
        ]
    );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'mikago-core' ),
            'type' => Controls_Manager::ICON,
            'options' => mikago_themify_icons(),
            'include' => mikago_include_themify_icons(),
            'default' => 'ti-panel',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'mikago-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => mikago_flaticons(),
            'include'    => mikago_include_flaticons(),
            'default'    => 'flaticon-staff',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'feature_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'feature_content',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'default' => esc_html__( 'your content text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'link_text',
			[
				'label' => esc_html__( 'Button/Link Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Button Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type btn text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'feature_link',
			[
				'label' => esc_html__( 'Link', 'mikago-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'mikago-core' ),
				'label_block' => true,
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		$this->add_control(
			'featureIcons_groups',
			[
				'label' => esc_html__( 'Feature Icons', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'feature_title' => esc_html__( 'Feature', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ feature_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		


		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_icon_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikago-feature .feature-grids .grid .header .fi:before',
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .feature-grids .grid .header .fi:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Icon Backround Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
						'feature_style' => array('style-one'),
					],
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .feature-grids .grid .header' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'feature_border_color',
			[
				'label' => esc_html__( 'Feature Border Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
						'feature_style' => array('style-three'),
					],
				'selectors' => [
					'{{WRAPPER}} .features-section-s3 .feature-grids .grid' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();// end: Section
		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikago-feature .details h3',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .details h3' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .details h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Contant
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikago-feature .details p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .details p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .details p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Read More
		$this->start_controls_section(
			'section_link_style',
			[
				'label' => esc_html__( 'Read More', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'link_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-feature .details a' => 'color: {{VALUE}};',
				],
			]
		);
		
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Feature widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$featureIcons_groups = !empty( $settings['featureIcons_groups'] ) ? $settings['featureIcons_groups'] : [];
		$feature_style = !empty( $settings['feature_style'] ) ? $settings['feature_style'] : '';

	
		if ( $feature_style === 'style-one' ) {
			$feature_area = 'features-section';
		} elseif ( $feature_style === 'style-two' ) {
			$feature_area = 'features-section-s2';
		} else {
			$feature_area = 'features-section-s3';
		}

		// Turn output buffer on
		ob_start();
		?>
		<section class="mikago-feature <?php echo esc_attr( $feature_area ); ?>">
      <div class="feature-grids clearfix">
          <?php 	// Group Param Output
						if( is_array( $featureIcons_groups ) && !empty( $featureIcons_groups ) ){
							foreach ( $featureIcons_groups as $each_item ) { 

							$icon_type = !empty( $each_item['icon_type'] ) ? $each_item['icon_type'] : '';
							$feature_title = !empty( $each_item['feature_title'] ) ? $each_item['feature_title'] : '';
							$feature_content = !empty( $each_item['feature_content'] ) ? $each_item['feature_content'] : '';

							$link_text = !empty( $each_item['link_text'] ) ? $each_item['link_text'] : '';
							$feature_link = !empty( $each_item['feature_link']['url'] ) ? $each_item['feature_link']['url'] : '';
							$feature_link_external = !empty( $each_item['feature_link']['is_external'] ) ? 'target="_blank"' : '';
							$feature_link_nofollow = !empty( $each_item['feature_link']['nofollow'] ) ? 'rel="nofollow"' : '';
							$feature_link_attr = !empty( $feature_link ) ?  $feature_link_external.' '.$feature_link_nofollow : '';

							switch ($each_item['icon_type']) {
					        case 'ticon':
					            $icon = !empty($each_item['ticon']) ? $each_item['ticon'] : '';
					            break;
					        case 'flaticon':
					            $icon = !empty($each_item['flaticon']) ? $each_item['flaticon'] : '';
					            break;
					    }

					    $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';

							?>
              <div class="grid">
			            <div class="header">
			                <?php 
				                if( $icon_class ) { echo '<i '.$icon_class.'></i>'; } 
				                if($feature_style === 'style-one') {
				                	if( $feature_title ) { echo '<h3>'.esc_html( $feature_title ).'</h3>'; }
				                }
			                ?>

			            </div>
			            <?php if ( $feature_style === 'style-two' || $feature_style === 'style-three' ) { ?>
				            <div class="details">
			                 <?php
	                       if( $feature_title ) { echo '<h3>'.esc_html( $feature_title ).'</h3>'; }
	                       if( $feature_content ) { echo '<p>'.esc_html( $feature_content ).'</p>'; }
	                       if( $feature_link ) { echo '<a href="'.esc_url( $feature_link ).'" class="read-more" '.esc_attr( $feature_link_attr ).'>'.esc_html( $link_text ).'</a>'; }
	                      ?>
				            </div>
			            <?php } ?>
			        </div>
            <?php }
						} ?>
        </div>
		</section>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Feature widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Feature() );