<?php
/*
 * Elementor Mikago Contact Form 7 Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Contact_Form extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_contact_form';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Contact Form', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-wpforms';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Contact Form widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	/*
	public function get_script_depends() {
		return ['tmx-mikago_contact_form'];
	}
	 */
	
	/**
	 * Register Mikago Contact Form widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_contact_form',
			[
				'label' => esc_html__( 'Form Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'contact_style',
			[
				'label' => esc_html__( 'Contact Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your contact style.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'form_title',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Default title', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your title here', 'mikago-core' ),
			]
		);
		
		$this->add_control(
			'form_subtitle',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'default' => esc_html__( 'Default Sub title', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your Sub title here', 'mikago-core' ),
			]
		);
		$this->add_control(
			'form_content',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
				'default' => esc_html__( 'Default content', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'mikago-core' ),
			]
		);
		
		$this->add_control(
			'form_id',
			[
				'label' => esc_html__( 'Select contact form', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => Controls_Helper_Output::get_posts('wpcf7_contact_form'),
			]
		);
		$this->end_controls_section();// end: Section

		// Title Style

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sascont_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .track-contact .track-trace h3, .contact-pg-section .section-title-s3 h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace h3, .contact-pg-section .section-title-s3 h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_pad',
			[
				'label' => __( 'Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace h3, .contact-pg-section .section-title-s3 h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Sub Title Style

		$this->start_controls_section(
			'section_sub_title_style',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sascont_sub_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .track-contact .track-trace label, .contact-pg-section .section-title-s3 span',
			]
		);
		$this->add_control(
			'sub_title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace label, .contact-pg-section .section-title-s3 span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'sub_title_pad',
			[
				'label' => __( 'Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace label, .contact-pg-section .section-title-s3 span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content Style

		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sascont_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .track-contact .track-trace p,.contact-pg-section .section-title-s3 p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace p,.contact-pg-section .section-title-s3 p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_pad',
			[
				'label' => __( 'Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .track-contact .track-trace p,.contact-pg-section .section-title-s3 p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		$this->start_controls_section(
			'section_form_style',
			[
				'label' => esc_html__( 'Form', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'form_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-pg-section .contact-form input[type="text"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="email"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="date"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="time"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="number"], 
				{{WRAPPER}} .contact-pg-section .contact-form textarea, 
				{{WRAPPER}} .contact-pg-section .contact-form select, 
				{{WRAPPER}} .contact-pg-section .contact-form .form-control, 
				{{WRAPPER}} .track-contact .track-trace select, 
				{{WRAPPER}} .track-contact .track-trace input',
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'form_border',
				'label' => esc_html__( 'Border', 'mikago-core' ),
				'selector' => '{{WRAPPER}} .contact-pg-section .contact-form input[type="text"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="email"], 
				{{WRAPPER}} .contact-pg-section .contact-forminput[type="date"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="time"], 
				{{WRAPPER}} .contact-pg-section .contact-form input[type="number"], 
				{{WRAPPER}} .contact-pg-section .contact-form textarea, 
				{{WRAPPER}} .contact-pg-section .contact-form select, 
				{{WRAPPER}} .contact-pg-section .contact-form .form-control, 
				{{WRAPPER}} .contact-pg-section .contact-form .nice-select,
				{{WRAPPER}} .track-contact .track-trace select, 
				{{WRAPPER}} .track-contact .track-trace input',

			]
		);
		$this->add_control(
			'placeholder_text_color',
			[
				'label' => __( 'Placeholder Text Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form input:not([type="submit"])::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form input:not([type="submit"])::-moz-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form input:not([type="submit"])::-ms-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form input:not([type="submit"])::-o-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form textarea::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form textarea::-moz-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form textarea::-ms-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .contact-pg-section .contact-form textarea::-o-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .track-contact .track-trace input::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} .track-contact .track-trace select::-webkit-input-placeholder' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'label_color',
			[
				'label' => __( 'Label Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form label' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form input[type="text"], 
					{{WRAPPER}} .contact-pg-section .contact-form input[type="email"], 
					{{WRAPPER}} .contact-pg-section .contact-form input[type="date"], 
					{{WRAPPER}} .contact-pg-section .contact-form input[type="time"], 
					{{WRAPPER}} .contact-pg-section .contact-form input[type="number"], 
					{{WRAPPER}} .contact-pg-section .contact-form textarea, 
					{{WRAPPER}} .contact-pg-section .contact-form select, 
					{{WRAPPER}} .contact-pg-section .contact-form .form-control, 
					{{WRAPPER}} .track-contact .track-trace input, 
					{{WRAPPER}} .contact-pg-section .contact-form .nice-select' => 'color: {{VALUE}} !important;',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn',
			]
		);
		$this->add_responsive_control(
			'btn_width',
			[
				'label' => esc_html__( 'Width', 'mikago-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 1,
					],
				],
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn' => 'min-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'btn_margin',
			[
				'label' => __( 'Margin', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_border_radius',
			[
				'label' => __( 'Border Radius', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs( 'button_style' );
			$this->start_controls_tab(
				'button_normal',
				[
					'label' => esc_html__( 'Normal', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_border',
					'label' => esc_html__( 'Border', 'mikago-core' ),
					'selector' => '{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit,.track-contact .track-trace .wpcf7-submit.submit-btn',
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_hover',
				[
					'label' => esc_html__( 'Hover', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_hover_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit:hover,.track-contact .track-trace .wpcf7-submit.submit-btn:hover' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_hover_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit:hover,.track-contact .track-trace .wpcf7-submit.submit-btn:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_hover_border',
					'label' => esc_html__( 'Border', 'mikago-core' ),
					'selector' => '{{WRAPPER}} .contact-pg-section .contact-form .wpcf7-form-control.wpcf7-submit:hover,.track-contact .track-trace .wpcf7-submit.submit-btn:hover',
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Contact Form widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$contact_style = !empty( $settings['contact_style'] ) ? $settings['contact_style'] : '';
		$form_id = !empty( $settings['form_id'] ) ? $settings['form_id'] : '';
		$form_title = !empty( $settings['form_title'] ) ? $settings['form_title'] : '';
		$form_subtitle = !empty( $settings['form_subtitle'] ) ? $settings['form_subtitle'] : '';
		$form_content = !empty( $settings['form_content'] ) ? $settings['form_content'] : '';



		// Turn output buffer on
		ob_start();

		if ( $contact_style == 'style-one' ) { ?>
	    <section class="contact-pg-section">
		    <div class="container">
		        <div class="row">
		            <div class="col col-md-8 col-md-offset-2">
		                <div class="section-title-s3">
		                   <?php 
				            			if( $form_title ) { echo '<span>'.esc_html( $form_title ).'</span>'; } 
				            			if( $form_subtitle ) { echo '<h2>'.esc_html( $form_subtitle ).'</h2>'; } 
				            			if( $form_content ) { echo '<p>'.esc_html( $form_content ).'</p>'; } 
				            		?>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col col-xs-12">
		                <div class="contact-form">
		                    <?php echo do_shortcode( '[contact-form-7 id="'. $form_id .'"]' ); ?> 
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
	  <?php } else { ?>
			<div class="track-contact">
		    <div class="container">
		        <div class="row">
		            <div class="col col-md-4">
		                <div class="section-title-s4">
		                	 <?php 
				            			if( $form_title ) { echo '<span>'.esc_html( $form_title ).'</span>'; } 
				            			if( $form_subtitle ) { echo '<h2>'.esc_html( $form_subtitle ).'</h2>'; } 
				            			if( $form_content ) { echo '<p>'.esc_html( $form_content ).'</p>'; } 
				            		?>
		                </div>
		            </div>
		            <div class="col col-md-8">
		                <div class="track-trace-quote-form clearfix">
		                    <div class="quote-form">
		                       <?php echo do_shortcode( '[contact-form-7 id="'. $form_id .'"]' ); ?> 
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div> <!-- end container -->
		</div>
	   <?php } 

		// Return outbut buffer
		echo ob_get_clean();
		
		}
		


	/**
	 * Render Contact Form widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Contact_Form() );