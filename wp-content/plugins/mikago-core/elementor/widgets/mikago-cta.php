<?php
/*
 * Elementor Mikago CTA Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_CTA extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_cta';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'CTA', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-bullhorn';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago CTA widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	/*
	public function get_script_depends() {
		return ['tmx-mikago_cta'];
	}
	*/
	
	/**
	 * Register Mikago CTA widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_CTA',
			[
				'label' => esc_html__( 'CTA Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'cta_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'cta_content',
			[
				'label' => esc_html__( 'Content Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Content Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type content text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'mikago-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'mikago-core'),
                'flaticon' => esc_html__('Flaticon', 'mikago-core'),
            ],
            'default' => 'ticon',
        ]
    );
     $this->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'mikago-core' ),
            'type' => Controls_Manager::ICON,
            'options' => mikago_themify_icons(),
            'include' => mikago_include_themify_icons(),
            'default' => 'fi ti-face-smile',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $this->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'mikago-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => mikago_flaticons(),
            'include'    => mikago_include_flaticons(),
            'default'    => 'flaticon-staff',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$this->add_control(
			'cta_number',
			[
				'label' => esc_html__( 'CTA Number', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'CTA Number', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type cta number here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn_text',
			[
				'label' => esc_html__( 'Button/Link Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Button Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type btn text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn_link',
			[
				'label' => esc_html__( 'Button Link', 'mikago-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$this->end_controls_section();// end: Section

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'mikago_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta-section-s2 h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 h2' => 'color: {{VALUE}};',
				],
			]
		);	
		
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,				
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'mikago_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta-section-s2 p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 p' => 'color: {{VALUE}};',
				],
			]
		);	
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 .fi:before' => 'color: {{VALUE}};',
				],
			]
		);	
		$this->add_control(
			'number_color',
			[
				'label' => esc_html__( 'Number Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 h5' => 'color: {{VALUE}};',
				],
			]
		);	
		
		$this->add_control(
			'content_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,				
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Button Style
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .cta-section-s2 .theme-btn-s2',
			]
		);
		$this->add_responsive_control(
			'button_min_width',
			[
				'label' => esc_html__( 'Width', 'mikago-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 500,
						'step' => 1,
					],
				],
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 .theme-btn-s2' => 'min-width: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'btn_style' => array('style-one','style-two'),
				],
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 .theme-btn-s2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'button_border_radius',
			[
				'label' => __( 'Border Radius', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'condition' => [
					'btn_style' => array('style-one','style-two'),
				],
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .cta-section-s2 .theme-btn-s2' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->start_controls_tabs( 'button_style' );
			$this->start_controls_tab(
				'button_normal',
				[
					'label' => esc_html__( 'Normal', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2',
					],
				]
			);
			$this->add_control(
				'button_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'link_border_color',
				[
					'label' => esc_html__( 'Link Border Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2' => 'border-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_border',
					'label' => esc_html__( 'Border', 'mikago-core' ),
					'selector' => '{{WRAPPER}} .cta-section .theme-btn-s2',
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_hover',
				[
					'label' => esc_html__( 'Hover', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_hover_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2:hover' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_bg_hover_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'link_border_hover_color',
				[
					'label' => esc_html__( 'Link Border Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cta-section-s2 .theme-btn-s2:after' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'button_hover_border',
					'label' => esc_html__( 'Border', 'mikago-core' ),
					'selector' => '{{WRAPPER}} .cta-section-s2 .theme-btn-s2:hover',
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render CTA widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();


		$cta_title = !empty( $settings['cta_title'] ) ? $settings['cta_title'] : '';
		$cta_content = !empty( $settings['cta_content'] ) ? $settings['cta_content'] : '';
		$cta_number = !empty( $settings['cta_number'] ) ? $settings['cta_number'] : '';
		$icon_type = !empty( $settings['icon_type'] ) ? $settings['icon_type'] : '';
		$bg_image = !empty( $settings['bg_image']['id'] ) ? $settings['bg_image']['id'] : '';	

	
		$btn_text = !empty( $settings['btn_text'] ) ? $settings['btn_text'] : '';
		$btn_link = !empty( $settings['btn_link']['url'] ) ? $settings['btn_link']['url'] : '';
		$btn_external = !empty( $settings['btn_link']['is_external'] ) ? 'target="_blank"' : '';
		$btn_nofollow = !empty( $settings['btn_link']['nofollow'] ) ? 'rel="nofollow"' : '';
		$btn_link_attr = !empty( $btn_link ) ?  $btn_external.' '.$btn_nofollow : '';

		$image_url = wp_get_attachment_url( $bg_image );

		$cta_title = $cta_title ? '<h2>'.esc_html( $cta_title ).'</h2>' : '';
		$cta_content = $cta_content ? '<p>'.esc_html( $cta_content ).'</p>' : '';
		$cta_number = $cta_number ? '<h5><a href="tel:'.esc_html( $cta_number ).'">'.esc_html( $cta_number ).'</a></h5>' : '';
		$ntrsv_button = $btn_link ? '<a href="'.esc_url($btn_link).'" '.$btn_link_attr.' class="theme-btn-s2" >'.esc_html( $btn_text ).'</a>' : '';



		switch ($settings['icon_type']) {
        case 'ticon':
            $icon = !empty($settings['ticon']) ? $settings['ticon'] : '';
            break;
        case 'flaticon':
            $icon = !empty($settings['flaticon']) ? $settings['flaticon'] : '';
            break;
    }

    $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';


		$e_uniqid       = uniqid();
    $inline_style   = '';

    if ( $image_url ) {
      $inline_style .= '.cta-section-'.$e_uniqid .'.cta-section:before {';
      $inline_style .= ( $image_url ) ? 'background-image: url( '. $image_url .' );' : '';
      $inline_style .= '}';
    }
    

     // add inline style
    add_inline_style( $inline_style );
    $styled_class  = 'cta-section-'.$e_uniqid.' ';

	
		// Turn output buffer on
		ob_start(); ?>
		<section class="cta-section-s2">
	    <div class="container">
	        <div class="row">
	            <div class="col col-lg-6 col-sm-8">
	                <div class="text">
	                    <?php echo $cta_title.$cta_content ?>
	                </div>
	            </div>
	            <div class="col col-lg-6 col-sm-4">
	                <div class="contact-info">
	                    <div class="icon">
	                      <?php if( $icon_class ) { echo '<i '.$icon_class.'></i>'; }  ?>
	                    </div>
	                     <?php echo $cta_number.$ntrsv_button; ?>
	                </div>
	            </div>
	        </div>
	    </div> <!-- end container -->
	</section>
	<?php // Return outbut buffer
		echo ob_get_clean();
		
	}
	/**
	 * Render CTA widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_CTA() );