<?php
/*
 * Elementor Mikago Project Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Project extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_project';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Project', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-briefcase';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Project widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_project'];
	}
	
	/**
	 * Register Mikago Project widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){


		$posts = get_posts( 'post_type="project"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'mikago' ) ] = 0;
    }
		
		
		$this->start_controls_section(
			'section_project',
			[
				'label' => esc_html__( 'Project Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'project_style',
			[
				'label' => esc_html__( 'Project Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your project style.', 'mikago-core' ),
			]
		);
		$this->end_controls_section();// end: Section
		

		$this->start_controls_section(
			'section_project_listing',
			[
				'label' => esc_html__( 'Listing Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'project_limit',
			[
				'label' => esc_html__( 'Project Limit', 'mikago-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'project_order',
			[
				'label' => __( 'Order', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'mikago-core' ),
					'DESC' => esc_html__( 'Desending', 'mikago-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'project_orderby',
			[
				'label' => __( 'Order By', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'mikago-core' ),
					'ID' => esc_html__( 'ID', 'mikago-core' ),
					'author' => esc_html__( 'Author', 'mikago-core' ),
					'title' => esc_html__( 'Title', 'mikago-core' ),
					'date' => esc_html__( 'Date', 'mikago-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'project_show_category',
			[
				'label' => __( 'Certain Categories?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'project_category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'project_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_project_button',
			[
				'label' => esc_html__( 'Project Button', 'mikago-core' ),
			]
		);
		$this->add_control(
			'project_button',
			[
				'label' => esc_html__( 'Buttton', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'button_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'View all cases', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
				'condition' => [
						'project_button' => array('true'),
					],
			]
		);
		
		$this->add_control(
			'button_link',
			[
				'label' => esc_html__( 'Title link', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '#', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title link here', 'mikago-core' ),
				'label_block' => true,
				'condition' => [
						'project_button' => array('true'),
					],
			]
		);
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Section', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'section_bottom_color',
			[
				'label' => esc_html__( 'Section bottom Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section:before' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'section_bottom_padding',
			[
				'label' => esc_html__( 'Section bottom Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .portfolio-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
	
		$this->end_controls_section();// end: Section
		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .portfolio-section .inner h3 a',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .inner h3 a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .inner h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .inner .cat' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_bgcolor',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .inner .cat' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Overly
		$this->start_controls_section(
			'section_overly_style',
			[
				'label' => esc_html__( 'Overly', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => esc_html__( 'Overly Background', 'mikago-core' ),
				'description' => esc_html__( 'Project Overly Color', 'mikago-core' ),
				'types' => [ 'gradient','classic' ],
				'selector' => '{{WRAPPER}} .portfolio-section .portfolio-grids .grid .details',
			]
		);
		$this->end_controls_section();// end: Section

		// Navigation
		$this->start_controls_section(
			'section_nav_style',
			[
				'label' => esc_html__( 'Button', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'project_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'nav_color',
			[
				'label' => esc_html__( 'Navigation Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .all-portfolio .theme-btn-s2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'nav_bg_color',
			[
				'label' => esc_html__( 'Navigation background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .all-portfolio .theme-btn-s2' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Navigation Hover
		$this->start_controls_section(
			'section_nav_hover_style',
			[
				'label' => esc_html__( 'Button Hover', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'project_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'nav_hover_color',
			[
				'label' => esc_html__( 'Navigation Hover Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .all-portfolio .theme-btn-s2:hover' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'nav_hover_bg_color',
			[
				'label' => esc_html__( 'Navigation Hover Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .portfolio-section .all-portfolio .theme-btn-s2:hover' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

	}

	/**
	 * Render Project widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$project_style = !empty( $settings['project_style'] ) ? $settings['project_style'] : '';

		$button_title = !empty( $settings['button_title'] ) ? $settings['button_title'] : '';
		$button_link = !empty( $settings['button_link'] ) ? $settings['button_link'] : '';

		$project_limit = !empty( $settings['project_limit'] ) ? $settings['project_limit'] : '';
		$project_order = !empty( $settings['project_order'] ) ? $settings['project_order'] : '';
		$project_orderby = !empty( $settings['project_orderby'] ) ? $settings['project_orderby'] : '';
		$project_show_category = !empty( $settings['project_show_category'] ) ? $settings['project_show_category'] : [];
		$project_show_id = !empty( $settings['project_show_id'] ) ? $settings['project_show_id'] : [];

		// Turn output buffer on
		ob_start();

		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($project_show_id) {
			$project_show_id = json_encode( $project_show_id );
			$project_show_id = str_replace(array( '[', ']' ), '', $project_show_id);
			$project_show_id = str_replace(array( '"', '"' ), '', $project_show_id);
      $project_show_id = explode(',',$project_show_id);
    } else {
      $project_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'project',
		  'posts_per_page' => (int)$project_limit,
		  'category_name' => implode(',', $project_show_category),
		  'orderby' => $project_orderby,
		  'order' => $project_order,
      'post__in' => $project_show_id,
		);

		$mikago_project = new \WP_Query( $args );

		if ( $project_style === 'style-one' ) {
			$project_container = 'portfolio-container';
			$project_grid_wrap = 'portfolio-grids portfolio-slider';
		} else {
			$project_container = 'container';
			$project_grid_wrap = 'portfolio-grids portfolio-slider-s2';
		}

		?>
		<section class="portfolio-section">
	    <div class="<?php echo esc_attr( $project_container ); ?>">
	    	<div class="<?php echo esc_attr( $project_grid_wrap ); ?>">
	 			<?php 
					if ( $mikago_project->have_posts()) : while ( $mikago_project->have_posts()) : $mikago_project->the_post();
					
					$project_options = get_post_meta( get_the_ID(), 'project_options', true );
          $project_title = isset( $project_options['project_title']) ? $project_options['project_title'] : '';
          $project_subtitle = isset( $project_options['project_subtitle']) ? $project_options['project_subtitle'] : '';
          $project_image = isset( $project_options['project_image']) ? $project_options['project_image'] : '';

          global $post;
          $image_url = wp_get_attachment_url( $project_image );
          $image_alt = get_post_meta( $project_image , '_wp_attachment_image_alt', true);

					?>
	        <div class="grid">
              <div class="img-holder">
                <?php 
                	if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; }
                ?>
              </div>
              <div class="details">
                  <div class="inner">
                  	<?php 
			            		if( $project_title ) { echo '<h3><a href="'.esc_url( get_permalink() ).'">'.esc_html( $project_title ).'</a></h3>'; }
			            		if( $project_subtitle ) { echo '<p class="cat">'.esc_html( $project_subtitle ).'</p>'; }
			            		?>
                  </div>
              </div>
          </div>
		       <?php 
					  endwhile;
					  endif;
					  wp_reset_postdata();
					 ?>
	    		</div>
	    		<?php if ( $button_title && $button_link  ) { ?>
	    			<div class="all-portfolio">
              <a href="<?php echo esc_url( $button_link ); ?>" class="theme-btn-s2"><?php echo esc_html( $button_title ); ?></a>
           </div>
	    		<?php } ?>
	    	</div>
			</section>
			<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Project widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Project() );