<?php
/*
 * Elementor Mikago About Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_About extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_about';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'About', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-address-card';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago About widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_about'];
	}
	
	/**
	 * Register Mikago About widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_about',
			[
				'label' => esc_html__( 'About Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'about_style',
			[
				'label' => esc_html__( 'About Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your about style.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'sub_title',
			[
				'label' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Sub Type title text here', 'mikago-core' ),
				'label_block' => true,
				'condition' => [
						'about_style' => array('style-one','style-two'),
					],
			]
		);
		$this->add_control(
			'about_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
				'condition' => [
						'about_style' => array('style-one','style-two'),
					],
			]
		);
		$this->add_control(
			'about_content',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'default' => esc_html__( 'your content text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'mikago-core' ),
				'type' => Controls_Manager::WYSIWYG,
				'label_block' => true,
				'condition' => [
						'about_style' => array('style-one','style-two'),
					],
			]
		);
		$this->add_control(
			'btn_text',
			[
				'label' => esc_html__( 'Button Text', 'mikago-core' ),
				'default' => esc_html__( 'button text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type button Text here', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn_link',
			[
				'label' => esc_html__( 'Button Link', 'mikago-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn2_text',
			[
				'label' => esc_html__( 'Button 2 Text', 'mikago-core' ),
				'default' => esc_html__( 'button text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type button Text here', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
			]
		);
		$this->add_control(
			'btn2_link',
			[
				'label' => esc_html__( 'Button 2 Link', 'mikago-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'https://your-link.com',
				'default' => [
					'url' => '',
				],
				'label_block' => true,
			]
		);
		$this->add_control(
			'video_link',
			[
				'label' => esc_html__( 'Video Link', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Video Link', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type video link here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'about_image',
			[
				'label' => esc_html__( 'About Image', 'mikago-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'description' => esc_html__( 'Set your image.', 'mikago-core'),
			]
		);
		
		$repeater = new Repeater();
		$repeater->add_control(
			'contact_title',
			[
				'label' => esc_html__( 'Contact Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Contact Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type contact text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'contact_content',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'default' => esc_html__( 'your content text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type your content here', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'contact_image',
			[
				'label' => esc_html__( 'About Image', 'mikago-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'description' => esc_html__( 'Set your image.', 'mikago-core'),
			]
		);
		$this->add_control(
			'aboutItems_groups',
			[
				'label' => esc_html__( 'About Item', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'contact_title' => esc_html__( 'About', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ contact_title }}}',
				'condition' => [
						'about_style' => array('style-one'),
					],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sub_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikado-about .section-title span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikado-about .section-title span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Sub Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikado-about .section-title span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
	
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikado-about .section-title h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikado-about .section-title h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikado-about .section-title h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'section_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikado-about .details p,.mikado-about .details h4',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikado-about .details p,.mikado-about .details h4' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_br_color',
			[
				'label' => esc_html__( 'Border Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .contact-info,about-section .contact-info > div:last-child' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Video Icon
		$this->start_controls_section(
			'section_video_style',
			[
				'label' => esc_html__( 'Video', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'video_icon_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .video-holder .fi:before, .about-section-s2 .video-holder .fi:before' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'video_icon_border_color',
			[
				'label' => esc_html__( 'Border Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .video-holder a, .about-section-s2 .video-holder a' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .about-section .video-holder a, .about-section-s2 .video-holder a' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section



		// Button One
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button One', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_one_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s2',
			]
		);
		
		$this->start_controls_tabs( 'button_one_style' );
			$this->start_controls_tab(
				'button_one_normal',
				[
					'label' => esc_html__( 'Normal', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_one_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s2' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_one_bgcolor',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s2' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_one_hover',
				[
					'label' => esc_html__( 'Hover', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_one_hover_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s2:hover' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_one_hover_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s2:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section


		// Button Two
		$this->start_controls_section(
			'section_button_two_style',
			[
				'label' => esc_html__( 'Button Two', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_two_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s3',
			]
		);
		
		$this->start_controls_tabs( 'button_two_style' );
			$this->start_controls_tab(
				'button_two_normal',
				[
					'label' => esc_html__( 'Normal', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_two_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s3' => 'color: {{VALUE}};',
					],
				]
			);
			$this->add_control(
				'button_two_bgcolor',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s3' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->end_controls_tab();  // end:Normal tab
			
			$this->start_controls_tab(
				'button_two_hover',
				[
					'label' => esc_html__( 'Hover', 'mikago-core' ),
				]
			);
			$this->add_control(
				'button_two_hover_color',
				[
					'label' => esc_html__( 'Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s3:hover' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'button_two_hover_bg_color',
				[
					'label' => esc_html__( 'Background Color', 'mikago-core' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .mikado-about .details .btns a.theme-btn-s3:hover' => 'background-color: {{VALUE}};',
					],
				]
			);
			$this->end_controls_tab();  // end:Hover tab
		$this->end_controls_tabs(); // end tabs
		
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render About widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$aboutItems_groups = !empty( $settings['aboutItems_groups'] ) ? $settings['aboutItems_groups'] : [];
		$about_style = !empty( $settings['about_style'] ) ? $settings['about_style'] : '';

		$sub_title = !empty( $settings['sub_title'] ) ? $settings['sub_title'] : '';
		$about_title = !empty( $settings['about_title'] ) ? $settings['about_title'] : '';
		$about_content = !empty( $settings['about_content'] ) ? $settings['about_content'] : '';
		$video_link = !empty( $settings['video_link'] ) ? $settings['video_link'] : '';


		$bg_image = !empty( $settings['about_image']['id'] ) ? $settings['about_image']['id'] : '';	

		$button_text = !empty( $settings['btn_text'] ) ? $settings['btn_text'] : '';	
		$button_link = !empty( $settings['btn_link']['url'] ) ? $settings['btn_link']['url'] : '';
		$button_link_external = !empty( $settings['btn_link']['is_external'] ) ? 'target="_blank"' : '';
		$button_link_nofollow = !empty( $settings['btn_link']['nofollow'] ) ? 'rel="nofollow"' : '';
		$button_link_attr = !empty( $button_link ) ?  $button_link_external.' '.$button_link_nofollow : '';

		$button2_text = !empty( $settings['btn2_text'] ) ? $settings['btn2_text'] : '';	
		$button2_link = !empty( $settings['btn2_link']['url'] ) ? $settings['btn2_link']['url'] : '';
		$button2_link_external = !empty( $settings['btn2_link']['is_external'] ) ? 'target="_blank"' : '';
		$button2_link_nofollow = !empty( $settings['btn2_link']['nofollow'] ) ? 'rel="nofollow"' : '';
		$button2_link_attr = !empty( $button2_link ) ?  $button2_link_external.' '.$button2_link_nofollow : '';

		
		$mikago_button = $button_link ? '<a href="'.esc_url($button_link).'" '.$button_link_attr.' class="theme-btn-s2">'.esc_html( $button_text ).'</a>' : '';
		$mikago_button2 = $button2_link ? '<a href="'.esc_url($button2_link).'" '.$button2_link_attr.' class="theme-btn-s3">'.esc_html( $button2_text ).'</a>' : '';

		// Image
		$image_url = wp_get_attachment_url( $bg_image );
		$image_alt = get_post_meta( $settings['about_image']['id'], '_wp_attachment_image_alt', true);

		if ( $about_style === 'style-two' ) {
			$about_wrapper = 'about-section-s2';
		} else {
			$about_wrapper = 'about-section';
		}


		// Turn output buffer on
		ob_start(); ?>
		<section class="mikado-about <?php echo esc_attr( $about_wrapper ); ?>">
		    <div class="container">
		        <div class="row">
		            <div class="col col-md-5">
		                <div class="img-holder">
		                     <?php if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_url( $image_alt ).'">'; } ?>
		                    <div class="video-holder">
		                        <?php if( $video_link ) { echo '<a href="'.esc_url( $video_link ).'" class="video-btn" data-type="iframe" tabindex="0"><i class="fi flaticon-play-button-3"></i></a>'; } ?>
		                    </div>
		                </div>
		            </div>
		            <div class="col col-md-7">
		                <div class="details">
		                    <div class="section-title">
		                      <?php 
				                    if( $sub_title ) { echo '<span>'.esc_html( $sub_title ).'</span>'; } 
				                   	if( $about_title ) { echo '<h2>'.esc_html( $about_title ).'</h2>'; } 
				                 	?>
		                    </div>
		                     <?php 
		                     if( $about_content ) { echo wp_kses_post( $about_content ); } 
		                     if ( $about_style == 'style-one' ) { ?>
		                     <div class="contact-info">
													<?php 	// Group Param Output
														if( is_array( $aboutItems_groups ) && !empty( $aboutItems_groups ) ){
														foreach ( $aboutItems_groups as $each_item ) {  

														$contact_title = !empty( $each_item['contact_title'] ) ? $each_item['contact_title'] : '';
														$contact_content = !empty( $each_item['contact_content'] ) ? $each_item['contact_content'] : '';
														$contact_image = !empty( $each_item['contact_image']['id'] ) ? $each_item['contact_image']['id'] : '';	

														$contact_url = wp_get_attachment_url( $contact_image );
														$contact_alt = get_post_meta( $each_item['contact_image']['id'], '_wp_attachment_image_alt', true);

														?>
		                        <div>
		                        	<?php
		                        	  if( $contact_url ) { echo '<img src="'.esc_url( $contact_url ).'" alt="'.esc_url( $contact_alt ).'">'; } 
						                    if( $contact_title ) { echo '<h4>'.esc_html( $contact_title ).'</h4>'; } 
						                   	if( $contact_content ) { echo '<p>'.wp_kses_post( $contact_content ).'</p>'; } 
						                 	?>
		                        </div>
		                        <?php } 
		                         } ?>
			                    </div>
		                     <?php } ?>
		                    <div class="btns">
		                       <?php echo $mikago_button.$mikago_button2 ?>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div> <!-- end container -->
		</section>
		<?php // Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render About widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_About() );