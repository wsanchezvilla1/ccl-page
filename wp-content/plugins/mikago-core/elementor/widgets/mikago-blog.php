<?php
/*
 * Elementor Mikago Blog Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Blog extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_blog';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Blog', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-newspaper-o';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Blog widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_blog'];
	}
	
	/**
	 * Register Mikago Blog widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){

		$posts = get_posts( 'post_type="post"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'mikago' ) ] = 0;
    }
		
			
		$this->start_controls_section(
			'section_blog',
			[
				'label' => __( 'Blog Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_style',
			[
				'label' => __( 'Blog Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One(List)', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two(Grid)', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your blog style.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_column',
			[
				'label' => __( 'Columns', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'condition' => [
					'blog_style' => 'style-two',
				],
				'frontend_available' => true,
				'options' => [
					'col-2' => esc_html__( 'Column Two', 'mikago-core' ),
					'col-3' => esc_html__( 'Column Three', 'mikago-core' ),
				],
				'default' => 'col-3',
				'description' => esc_html__( 'Select your blog column.', 'mikago-core' ),
			]
		);		
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_blog_metas',
			[
				'label' => esc_html__( 'Meta\'s Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_image',
			[
				'label' => esc_html__( 'Image', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_date',
			[
				'label' => esc_html__( 'Date', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_comments',
			[
				'label' => esc_html__( 'Comments', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_author',
			[
				'label' => esc_html__( 'Author', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'blog_tags',
			[
				'label' => esc_html__( 'Tags', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
				'condition' => [
						'blog_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'read_more_txt',
			[
				'label' => esc_html__( 'Read More Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Type your Read More text here', 'mikago-core' ),
				'condition' => [
					'blog_style' => array('style-one'),
				],
			]
		);
		$this->end_controls_section();// end: Section


		$this->start_controls_section(
			'section_blog_listing',
			[
				'label' => esc_html__( 'Listing Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_limit',
			[
				'label' => esc_html__( 'Blog Limit', 'mikago-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_order',
			[
				'label' => __( 'Order', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'mikago-core' ),
					'DESC' => esc_html__( 'Desending', 'mikago-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'blog_orderby',
			[
				'label' => __( 'Order By', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'mikago-core' ),
					'ID' => esc_html__( 'ID', 'mikago-core' ),
					'author' => esc_html__( 'Author', 'mikago-core' ),
					'title' => esc_html__( 'Title', 'mikago-core' ),
					'date' => esc_html__( 'Date', 'mikago-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'blog_show_category',
			[
				'label' => __( 'Certain Categories?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'blog_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->add_control(
			'short_content',
			[
				'label' => esc_html__( 'Excerpt Length', 'mikago-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'step' => 1,
				'default' => 55,
				'description' => esc_html__( 'How many words you want in short content paragraph.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'blog_pagination',
			[
				'label' => esc_html__( 'Pagination', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .blog-section h3 a,.blog-pg-section .post h3 a',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-section h3 a,.blog-pg-section .post h3 a,.blog-pg-section .entry-meta li a,.blog-pg-section .entry-meta li' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .blog-section h3 a,.blog-pg-section .post h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Meta
		$this->start_controls_section(
			'section_meta_style',
			[
				'label' => esc_html__( 'Meta', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_meta_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .blog-section .entry-meta ul li, .blog-section .entry-meta ul li a,.blog-pg-section .entry-meta li',
			]
		);
		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-section .entry-meta ul li, .blog-pg-section .entry-meta li a,.blog-pg-section .entry-meta li' => 'color: {{VALUE}};',
					'{{WRAPPER}} .blog-section .entry-meta ul > li + li:before,.blog-pg-section .entry-meta:before' => 'background-color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'meta_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .blog-section .entry-meta ul li,.blog-section .entry-meta ul li a,.blog-pg-section .entry-meta li' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section


		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'blog_style' => array('style-one'),
					],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'ntrsvt_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .blog-pg-section .post p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-pg-section .post p' => 'color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} ..blog-pg-section .post p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Button
		$this->start_controls_section(
			'section_button_style',
			[
				'label' => esc_html__( 'Button', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'blog_style' => array('style-one'),
					],
			]
		);
	
		$this->add_control(
			'button_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-pg-section a.theme-btn' => 'color: {{VALUE}};'
				],
			]
		);
		
		$this->add_control(
			'button_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-pg-section a.theme-btn' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .blog-pg-section a.theme-btn:after' => 'border-color: {{VALUE}};'
				],
			]
		);
		
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Blog widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();

		$blog_style = !empty( $settings['blog_style'] ) ? $settings['blog_style'] : '';
		$blog_column = !empty( $settings['blog_column'] ) ? $settings['blog_column'] : '';
		$read_more_txt = !empty( $settings['read_more_txt'] ) ? $settings['read_more_txt'] : '';

		$blog_limit = !empty( $settings['blog_limit'] ) ? $settings['blog_limit'] : '';
		$blog_image  = ( isset( $settings['blog_image'] ) && ( 'true' == $settings['blog_image'] ) ) ? true : false;
		$blog_date  = ( isset( $settings['blog_date'] ) && ( 'true' == $settings['blog_date'] ) ) ? true : false;
		$blog_comments  = ( isset( $settings['blog_comments'] ) && ( 'true' == $settings['blog_comments'] ) ) ? true : false;
		$blog_author  = ( isset( $settings['blog_author'] ) && ( 'true' == $settings['blog_author'] ) ) ? true : false;
		$blog_tags  = ( isset( $settings['blog_tags'] ) && ( 'true' == $settings['blog_tags'] ) ) ? true : false;

		$blog_order = !empty( $settings['blog_order'] ) ? $settings['blog_order'] : '';
		$blog_orderby = !empty( $settings['blog_orderby'] ) ? $settings['blog_orderby'] : '';
		$blog_show_category = !empty( $settings['blog_show_category'] ) ? $settings['blog_show_category'] : [];
		$blog_show_id = !empty( $settings['blog_show_id'] ) ? $settings['blog_show_id'] : [];
		$short_content = !empty( $settings['short_content'] ) ? $settings['short_content'] : '';
		$blog_pagination  = ( isset( $settings['blog_pagination'] ) && ( 'true' == $settings['blog_pagination'] ) ) ? true : false;
		
		if ( $blog_style === 'style-one') {
			$blog_area = 'blogs-section';
			$blog_class = 'blog-grids';
		} else {
			$blog_area = 'blog-about-section';
			$blog_class = 'blog-grid clearfix';
		}
		
		$excerpt_length = $short_content ? $short_content : '55';
		$read_more_txt = $read_more_txt ? $read_more_txt : esc_html__( 'Read More', 'mikago-core' );

		 if ( $blog_style === 'style-one' ) {
		  	$post_wrapper = 'blog-pg-section page-post';
		  	$post_content = 'blog-content';
		  } else {
		  	$post_wrapper = 'blog-section';
		  	$post_content = 'blog-grids clearfix';
		  }
		// Turn output buffer on
		ob_start();


		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($blog_show_id) {
			$blog_show_id = json_encode( $blog_show_id );
			$blog_show_id = str_replace(array( '[', ']' ), '', $blog_show_id);
			$blog_show_id = str_replace(array( '"', '"' ), '', $blog_show_id);
      $blog_show_id = explode(',',$blog_show_id);
    } else {
      $blog_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'post',
		  'posts_per_page' => (int)$blog_limit,
		  'category_name' => implode(',', $blog_show_category),
		  'orderby' => $blog_orderby,
		  'order' => $blog_order,
      'post__in' => $blog_show_id,
		);

		$mikago_post = new \WP_Query( $args ); ?>

			<div class="<?php echo esc_attr( $post_wrapper ); ?>">
				<div class="<?php echo esc_attr( $post_content ); ?>">
				<div class="row">
					<div class="col col-xs-12">
	       <?php 
				  if ($mikago_post->have_posts()) : while ($mikago_post->have_posts()) : $mikago_post->the_post();
				  $large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
				  $large_alt = get_post_meta( get_post_thumbnail_id(get_the_ID()) , '_wp_attachment_image_alt', true); 
				  $large_image = $large_image[0];  

				  $post_options = get_post_meta( get_the_ID(), 'post_options', true );
					$grid_image = isset( $post_options['grid_image'] ) ? $post_options['grid_image'] : '';
					$image_url = wp_get_attachment_url( $grid_image );
          $image_alt = get_post_meta( $grid_image , '_wp_attachment_image_alt', true); 


				  if ( $blog_style === 'style-one' ) {
				  	$featured_img = $large_image;
				  	$featured_alt = $large_alt;
				  } else {
						$featured_img = $image_url;
						$featured_alt = $image_alt;
				  }

				  if ( $blog_style === 'style-one' ) { ?>
		        <div class="post format-standard-image">
		        		<?php if ($large_image && $blog_image) { ?>
			            <div class="entry-media">
			                <img src="<?php echo esc_url($featured_img); ?>" alt="<?php echo esc_attr( $featured_alt); ?>">
			            </div>
	           		<?php } ?>
		            <ul  class="entry-meta">
                	<?php if ( $blog_date ) { ?><li><i class="ti-time"></i><?php echo get_the_date('d M, Y'); ?></li><?php } 
                 	
                 	 if ( $blog_tags ) {
			               $last_tag = get_the_tags();
											if ( !empty( $last_tag ) ) {
											  $last_tag = end( $last_tag );
											  echo '<li><i class="ti-book"></i> '.esc_html( $last_tag->name ).'</li>';
										}
		              }
			            if (  $blog_comments ) { ?>
									  <li>
											 <a class="mikago-comment" href="<?php echo esc_url( get_comments_link() ); ?>">
											 	<i class="ti-comment-alt"></i>
						              <?php printf( esc_html( _nx( 'Comment (%1$s)', 'Comments (%1$s)', get_comments_number(), 'comments title', 'aequity' ) ), '<span class="comment">'.number_format_i18n( get_comments_number() ).'</span>','<span>' . get_the_title() . '</span>' ); ?>
						          </a>
			              </li>
								 <?php	} ?>
                </ul>
		           <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h3>
		            <p><?php echo wp_trim_words( get_the_content(), $excerpt_length, '' ); ?></p>
		            <a href="<?php echo esc_url( get_permalink() ); ?>" class="theme-btn"><?php echo esc_html( $read_more_txt ); ?></a>
		        </div>
	       	<?php } else { ?>
	       <div class="grid">
				    <div class="entry-media">
				        <img src="<?php echo esc_url($featured_img); ?>" alt="<?php echo esc_attr( $featured_alt); ?>">
				        <div class="date">
				            <p><?php echo get_the_date('d'); ?><span><?php echo get_the_date('M'); ?></span></p>
				        </div>
				    </div>
				    <div class="details">
				        <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html(get_the_title()); ?></a></h3>
				         <ul>
			          	<?php if ( $blog_author ) { ?>
			              <li>
			              	<i class="ti-user"></i>
			              	 <?php
			                 		printf( esc_html__(' By:','aequity') .' <a href="%1$s" rel="author">%2$s</a>',
					                esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
					                get_the_author()
					             ); 
			                ?>
			              </li>
			            <?php } 
			            if (  $blog_comments ) { ?>
									  <li>
											 <a class="aequity-comment" href="<?php echo esc_url( get_comments_link() ); ?>">
											 	<i class="ti-comment-alt"></i>
						              <?php printf( esc_html( _nx( 'Comment (%1$s)', 'Comments (%1$s)', get_comments_number(), 'comments title', 'aequity' ) ), '<span class="comment">'.number_format_i18n( get_comments_number() ).'</span>','<span>' . get_the_title() . '</span>' ); ?>
						          </a>
			              </li>
									<?php	} ?>
			          </ul>
				    </div>
					</div>
	       	<?php } 
				  endwhile;
				  endif;
				  wp_reset_postdata();
					if ($blog_pagination) { ?>
					  <div class="page-pagination-wrap">
					  <?php 	echo '<div class="paginations">';
							$big = 999999999;
							echo paginate_links( array(
                'base'      => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                'format'    => '?paged=%#%',
                'total'     => $mikago_post->max_num_pages,
                'show_all'  => false,
                'current'   => max( 1, $my_page ),
								'prev_text'    => '<div class="fi flaticon-back"></div>',
								'next_text'    => '<div class="fi flaticon-next"></div>',
                'mid_size'  => 1,
                'type'      => 'list'
              ) );
	        	echo '</div>'; ?>
					  </div>
					<?php } ?>
					</div>
				</div>
		   </div>
		</div>

		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Blog widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Blog() );