<?php
/*
 * Elementor Mikago Accordion  Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Accordion  extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_accordion';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Accordion ', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-plus-square';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Accordion  widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_accordion'];
	}
	
	/**
	 * Register Mikago Accordion  widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_accordion',
			[
				'label' => esc_html__( 'Accordion  Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'section_subtitle',
			[
				'label' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type sub title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'section_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'active_tabs',
			[
				'label' => __( 'Active Accordion', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'mikago-core' ),
				'label_off' => __( 'Hide', 'mikago-core' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		$repeater->add_control(
			'accordion_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'accordion_content',
			[
				'label' => esc_html__( 'Content Text', 'mikago-core' ),
				'type' => Controls_Manager::WYSIWYG,
				'default' => esc_html__( 'Content Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type content text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		
		$this->add_control(
			'accordionItems_groups',
			[
				'label' => esc_html__( 'Accordion  Items', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'accordion_title' => esc_html__( 'Accordion ', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ accordion_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq-page-section .section-title-s2 span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-page-section .section-title-s2 span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .faq-page-section .section-title-s2 span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .faq-page-section .section-title-s2 h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .faq-page-section .section-title-s2 h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .faq-page-section .section-title-s2 h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Accordion Title
		$this->start_controls_section(
			'section_accordion_title_style',
			[
				'label' => esc_html__( 'Accordion Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_accordion_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .theme-accordion-s1 .panel-heading a',
			]
		);
		$this->add_control(
			'accordion_title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .theme-accordion-s1 .panel-heading a,.theme-accordion-s1 .panel-heading a:before' => 'color: {{VALUE}};',
					'{{WRAPPER}} .theme-accordion-s1 .panel-heading a:before' => 'border-color: {{VALUE}};'
				],
			]
		);
		$this->add_control(
			'accordion_title_border_color',
			[
				'label' => esc_html__( 'Border Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .theme-accordion-s1 .panel' => 'border-color: {{VALUE}};'
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Accordion Content
		$this->start_controls_section(
			'section_accordion_content_style',
			[
				'label' => esc_html__( 'Accordion Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_accordion_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .theme-accordion-s1 .panel-heading + .panel-collapse > .panel-body p',
			]
		);
		$this->add_control(
			'accordion_content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .theme-accordion-s1 .panel-heading + .panel-collapse > .panel-body p' => 'color: {{VALUE}};'
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Accordion  widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$accordionItems_groups = !empty( $settings['accordionItems_groups'] ) ? $settings['accordionItems_groups'] : [];

		$section_subtitle = !empty( $settings['section_subtitle'] ) ? $settings['section_subtitle'] : '';
		$section_title = !empty( $settings['section_title'] ) ? $settings['section_title'] : '';

		$section_title = preg_replace('~\s*<br ?/?>\s*~',"<br />",$section_title);
    $section_title = nl2br($section_title);
		

		// Turn output buffer on
		ob_start();
		?>
		<section class="faq-page-section">
      <div class="section-title-s2">
    	 <?php 
      		if( $section_subtitle ) { echo '<span>'.esc_html( $section_subtitle ).'</span>'; }
      		if( $section_title ) { echo '<h2>'.wp_kses_post( $section_title ).'</h2>'; } 
      	?>
      </div>
      <div class="faq-section">
          <div class="panel-group faq-accordion theme-accordion-s1" id="accordion">
             <?php 	// Group Param Output
							if( is_array( $accordionItems_groups ) && !empty( $accordionItems_groups ) ){
								$id = 1;
								foreach ( $accordionItems_groups as $each_items ) { 
								$id++;
								$accordion_title = !empty( $each_items['accordion_title'] ) ? $each_items['accordion_title'] : '';
								$accordion_content = !empty( $each_items['accordion_content'] ) ? $each_items['accordion_content'] : '';
								$active_tabs = !empty( $each_items['active_tabs'] ) ? $each_items['active_tabs'] : '';

								 if ( $active_tabs == 'yes') {
	                $active_class = 'in';
	                $heade_class = '';
	              } else {
	                $active_class = '';
	                $heade_class = 'collapsed';
	              }

							?>
              <div class="panel panel-default">
                  <div class="panel-heading">
											<?php if ( $accordion_title ) {
												echo'<a class="'.esc_attr( $heade_class ).'" data-toggle="collapse" data-parent="#accordion" href="#ac'.esc_attr( $id ).'" aria-expanded="true">'.esc_html( $accordion_title ).'</a>';
											} ?>
                  </div>
                  <?php if ( $accordion_content ) { ?>
                  <div id="ac<?php echo esc_attr( $id ); ?>" class="panel-collapse collapse <?php echo esc_attr( $active_class ); ?>">
                      <div class="panel-body">
                           <?php echo wp_kses_post( $accordion_content ); ?>
                      </div>
                  </div>
                  <?php } ?>
              </div>
              <?php }
							} ?>
          </div>
      </div>
		</section>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Accordion  widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Accordion () );