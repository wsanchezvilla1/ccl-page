<?php
/*
 * Elementor Mikago Testimonial Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Testimonial extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_testimonial';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Testimonial', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-quote-right';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Testimonial widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_testimonial'];
	}
	
	/**
	 * Register Mikago Testimonial widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_testimonial',
			[
				'label' => esc_html__( 'Testimonial Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'testimonial_style',
			[
				'label' => esc_html__( 'Testimonial Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your testimonial style.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'section_subtitle',
			[
				'label' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Sub Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type sub title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'sction_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'testi_image',
			[
				'label' => esc_html__( 'Testimonial Image', 'mikago-core' ),
				'type' => Controls_Manager::MEDIA,
				'frontend_available' => true,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				'description' => esc_html__( 'Set your image.', 'mikago-core'),
				'condition' => [
						'testimonial_style' => array('style-two'),
					],
			]
		);
		$repeater = new Repeater();
		$repeater->add_control(
			'testimonial_title',
			[
				'label' => esc_html__( 'Testimonial Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'testimonial_subtitle',
			[
				'label' => esc_html__( 'Testimonial Sub Title', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Testimonial Sub Title', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type testimonial Sub title here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'testimonial_content',
			[
				'label' => esc_html__( 'Testimonial Content', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Testimonial Content', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type testimonial Content here', 'mikago-core' ),
				'label_block' => true,
			]
		);
	  $repeater->add_control(
			'bg_image',
			[
				'label' => esc_html__( 'Testimonial Image', 'mikago-core' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
				
			]
		);
		$this->add_control(
			'testimonialItems_groups',
			[
				'label' => esc_html__( 'Testimonial Items', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'testimonial_title' => esc_html__( 'Testimonial', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ testimonial_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Sub Title
		$this->start_controls_section(
			'section_subtitle_style',
			[
				'label' => esc_html__( 'Section Sub Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'ntrsv_subtitle_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonials-section .section-title-s3 > span, .testimonials-section-s2 .section-title-s3 > span',
			]
		);
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .section-title-s3 > span, .testimonials-section-s2 .section-title-s3 > span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'subtitle_padding',
			[
				'label' => __( 'Sub Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .section-title-s3 > span, .testimonials-section-s2 .section-title-s3 > span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

	
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Section Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonials-section .section-title-s3 > h2, .testimonials-section-s2 .section-title-s3 > h2',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .section-title-s3 > h2, .testimonials-section-s2 .section-title-s3 > h2' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .section-title-s3 > h2, .testimonials-section-s2 .section-title-s3 > h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'ntrsv_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .testimonials-section .quote p,.testimonials-section-s2 .quote p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .quote p,.testimonials-section-s2 .quote p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Content Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .quote p,.testimonials-section-s2 .quote p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'content_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .quote' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .testimonials-section .quote:before' => 'border-top-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_name_color',
			[
				'label' => esc_html__( 'Client Name Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .client-info h5,.testimonials-section-s2 .client-info h5' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_client_title_color',
			[
				'label' => esc_html__( 'Client Title Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section .client-info span,.testimonials-section-s2 .client-info p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
		// Dot Color
		$this->start_controls_section(
			'section_quote_style',
			[
				'label' => esc_html__( 'Dot', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'dot_color',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section-s2 .owl-theme .owl-dots .owl-dot span' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'dot_active_color',
			[
				'label' => esc_html__( 'Active Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-section-s2 .owl-theme .owl-dots .owl-dot.active span' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		
	}

	/**
	 * Render Testimonial widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$testimonialItems_groups = !empty( $settings['testimonialItems_groups'] ) ? $settings['testimonialItems_groups'] : [];
		$testimonial_style = !empty( $settings['testimonial_style'] ) ? $settings['testimonial_style'] : '';

		$section_subtitle = !empty( $settings['section_subtitle'] ) ? $settings['section_subtitle'] : '';
		$sction_title = !empty( $settings['sction_title'] ) ? $settings['sction_title'] : '';
		$testi_image = !empty( $settings['testi_image']['id'] ) ? $settings['testi_image']['id'] : '';	


		// Image
		$testi_single_url = wp_get_attachment_url( $testi_image );
		$testi_single_alt = get_post_meta( $settings['testi_image']['id'], '_wp_attachment_image_alt', true);


		// Turn output buffer on
		ob_start();

		if ( $testimonial_style === 'style-one' ) { ?>
		<section class="testimonials-section">
	    <div class="container">
	        <div class="row">
	            <div class="col col-lg-6 col-lg-offset-3">
	                <div class="section-title-s3">
	                  <?php 
	                    if( $section_subtitle ) { echo '<span>'.esc_html( $section_subtitle ).'</span>'; }
	                    if( $sction_title ) { echo '<h2>'.esc_html( $sction_title ).'</h2>'; } 
	                  ?>
	                </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col col-xs-12">
	                <div class="testimonial-grids clearfix">
	                   <?php 	// Group Param Output
											if( is_array( $testimonialItems_groups ) && !empty( $testimonialItems_groups ) ){
											foreach ( $testimonialItems_groups as $each_items ) { 

											$testimonial_title = !empty( $each_items['testimonial_title'] ) ? $each_items['testimonial_title'] : '';
											$testimonial_subtitle = !empty( $each_items['testimonial_subtitle'] ) ? $each_items['testimonial_subtitle'] : '';
											$testimonial_content = !empty( $each_items['testimonial_content'] ) ? $each_items['testimonial_content'] : '';

											$image_url = wp_get_attachment_url( $each_items['bg_image']['id'] );
											$image_alt = get_post_meta( $each_items['bg_image']['id'], '_wp_attachment_image_alt', true);

											?>
	                    <div class="grid">
	                        <div class="quote">
	                             <?php if( $testimonial_content ) { echo '<p>'.esc_html( $testimonial_content ).'</p>'; } ?>
	                        </div>
	                        <div class="client-info">
	                            <div class="img-holder">
	                              <?php 
				                        	if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; }
				                        ?>
	                            </div>
	                            <div class="details">
	                                <?php 
					                        	if( $testimonial_title ) { echo '<h5>'.esc_html( $testimonial_title ).'</h5>'; } 
					                        	if( $testimonial_subtitle ) { echo '<span>'.esc_html( $testimonial_subtitle ).'</span>'; }
					                        ?>
	                            </div>
	                        </div>
	                    </div>
	                   <?php }
											} ?>
	                </div>
	            </div>
	        </div>
	    </div> <!-- end container -->
		</section>
		<?php } else { ?>
		<section class="testimonials-section-s2">
		    <div class="container">
		        <div class="row">
		            <div class="col col-lg-6 col-lg-offset-3">
		                <div class="section-title-s3">
		                   <?php 
		                    if( $section_subtitle ) { echo '<span>'.esc_html( $section_subtitle ).'</span>'; }
		                    if( $sction_title ) { echo '<h2>'.esc_html( $sction_title ).'</h2>'; } 
		                  ?>
		                </div>
		            </div>
		        </div>
		        <div class="row">
		            <div class="col col-md-4">
		                <div class="testimonial-left-img-holder">
		                   <?php 
                        	if( $testi_single_url ) { echo '<img src="'.esc_url( $testi_single_url ).'" alt="'.esc_attr( $testi_single_alt ).'">'; }
                        ?>
		                </div>
		            </div>
		            <div class="col col-md-8">
		                <div class="testimonial-grids testimonial-slider clearfix">
		                  <?php 	// Group Param Output
												if( is_array( $testimonialItems_groups ) && !empty( $testimonialItems_groups ) ){
												foreach ( $testimonialItems_groups as $each_items ) { 

												$testimonial_title = !empty( $each_items['testimonial_title'] ) ? $each_items['testimonial_title'] : '';
												$testimonial_subtitle = !empty( $each_items['testimonial_subtitle'] ) ? $each_items['testimonial_subtitle'] : '';
												$testimonial_content = !empty( $each_items['testimonial_content'] ) ? $each_items['testimonial_content'] : '';

												$image_url = wp_get_attachment_url( $each_items['bg_image']['id'] );
												$image_alt = get_post_meta( $each_items['bg_image']['id'], '_wp_attachment_image_alt', true);
												?>
		                    <div class="grid">
		                        <div class="quote">
		                            <i class="fi flaticon-quote"></i>
		                            <?php if( $testimonial_content ) { echo '<p>'.esc_html( $testimonial_content ).'</p>'; } ?>
		                        </div>
		                        <div class="client-info">
		                            <div class="img-holder">
		                             <?php 
					                        if( $image_url ) { echo '<img src="'.esc_url( $image_url ).'" alt="'.esc_attr( $image_alt ).'">'; }
					                       ?>
		                            </div>
		                            <div class="details">
		                               <?php 
						                        	if( $testimonial_title ) { echo '<h5>'.esc_html( $testimonial_title ).'</h5>'; } 
						                        	if( $testimonial_subtitle ) { echo '<span>'.esc_html( $testimonial_subtitle ).'</span>'; }
						                        ?>
		                            </div>
		                        </div>
		                    </div>
		                   <?php }
												} ?>
		                </div>
		            </div>
		        </div>
		    </div> <!-- end container -->
		</section>
		<?php } 
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Testimonial widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Testimonial() );