<?php
/*
 * Elementor Mikago ContactInfo Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_ContactInfo extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_contactinfo';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'ContactInfo', 'mikago-core' );
	}

	/**
	 * Retrieve the widget item.
	*/
	public function get_item() {
		return 'fa fa-telegram';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago ContactInfo widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_contactinfo'];
	}
	
	/**
	 * Register Mikago ContactInfo widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){
		
		$this->start_controls_section(
			'section_contactinfo',
			[
				'label' => esc_html__( 'ContactInfo Options', 'mikago-core' ),
			]
		);
		
		$repeater = new Repeater();
		$repeater->add_control(
        'icon_type',
        [
            'label' => esc_html__( 'Icon Type', 'mikago-core' ),
            'type' => Controls_Manager::SELECT,
            'options' => [
                'ticon' => esc_html__('Themify Icon', 'mikago-core'),
                'flaticon' => esc_html__('Flaticon', 'mikago-core'),
            ],
            'default' => 'ticon',
        ]
    );
     $repeater->add_control(
        'ticon',
        [
            'label' => esc_html__( 'Themify Icon', 'mikago-core' ),
            'type' => Controls_Manager::ICON,
            'options' => mikago_themify_icons(),
            'include' => mikago_include_themify_icons(),
            'default' => 'ti-email',
            'condition' => [
                'icon_type' => 'ticon'
            ]
        ]
    );
    $repeater->add_control(
        'flaticon',
        [
            'label'      => esc_html__( 'Flaticon', 'mikago-core' ),
            'type'       => Controls_Manager::ICON,
            'options'    => mikago_flaticons(),
            'include'    => mikago_include_flaticons(),
            'default'    => 'flaticon-staff',
            'condition'  => [
                'icon_type' => 'flaticon'
            ]
        ]
    );
		$repeater->add_control(
			'contact_title',
			[
				'label' => esc_html__( 'Title Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Title Text', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type title text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'contact_desc',
			[
				'label' => esc_html__( 'Contact Info Desctription', 'mikago-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'default' => esc_html__( 'Contact Info Desctription', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type contact desctription here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'contactinfoItems_groups',
			[
				'label' => esc_html__( 'Contact Info Items', 'mikago-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'contact_title' => esc_html__( 'Contact Info', 'mikago-core' ),
					],
					
				],
				'fields' =>  $repeater->get_controls(),
				'title_field' => '{{{ contact_title }}}',
			]
		);
		$this->end_controls_section();// end: Section
		

		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon Style', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_icon_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-info-section .contact-info .icon i',
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info .icon i' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Icon Backround Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info .icon ' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section
		

		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'sasban_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-info-section .contact-info h5',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info h5' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info h5' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'ntrsvt_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .contact-info-section .contact-info p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => __( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .contact-info-section .contact-info p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render ContactInfo widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$contactinfoItems_groups = !empty( $settings['contactinfoItems_groups'] ) ? $settings['contactinfoItems_groups'] : [];
		
		// Turn output buffer on
		ob_start();
		?>
		<section class="contact-info-section contact-pg-section">
		    <div class="contact-info clearfix">
		      <?php 	// Group Param Output
					if( is_array( $contactinfoItems_groups ) && !empty( $contactinfoItems_groups ) ){
						foreach ( $contactinfoItems_groups as $each_items ) { 
						$icon_type = !empty( $each_items['icon_type'] ) ? $each_items['icon_type'] : '';
						$contact_title = !empty( $each_items['contact_title'] ) ? $each_items['contact_title'] : '';
						$contact_desc = !empty( $each_items['contact_desc'] ) ? $each_items['contact_desc'] : '';

						$contact_desc = preg_replace('~\s*<br ?/?>\s*~',"<br />",$contact_desc);
      			$contact_desc = nl2br($contact_desc);

      			switch ($each_items['icon_type']) {
				        case 'ticon':
				            $icon = !empty($each_items['ticon']) ? $each_items['ticon'] : '';
				            break;
				        case 'flaticon':
				            $icon = !empty($each_items['flaticon']) ? $each_items['flaticon'] : '';
				            break;
				    }

				    $icon_class = (!empty($icon)) ? "class='fi $icon'" : '';
						?>
		        <div>
		            <div class="icon">
		                <?php if( $icon_class ) { echo '<i '.$icon_class.'></i>'; }  ?>
		            </div>
		             <?php 	
		             		if( $contact_title ) { echo '<h5>'.esc_html( $contact_title ).'</h5>'; } 
		             		if( $contact_desc ) { echo '<p>'.wp_kses_post( $contact_desc ).'</p>'; } 
		             ?>
		        </div>
					<?php }
					} ?>
		    </div>
		</section>
		<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render ContactInfo widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_ContactInfo() );