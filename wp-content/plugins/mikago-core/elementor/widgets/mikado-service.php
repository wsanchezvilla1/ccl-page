<?php
/*
 * Elementor Mikago Service Widget
 * Author & Copyright: blue_design
*/

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Mikago_Service extends Widget_Base{

	/**
	 * Retrieve the widget name.
	*/
	public function get_name(){
		return 'tmx-mikago_service';
	}

	/**
	 * Retrieve the widget title.
	*/
	public function get_title(){
		return esc_html__( 'Service', 'mikago-core' );
	}

	/**
	 * Retrieve the widget icon.
	*/
	public function get_icon() {
		return 'fa fa-telegram';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	*/
	public function get_categories() {
		return ['blue_design-category'];
	}

	/**
	 * Retrieve the list of scripts the Mikago Service widget depended on.
	 * Used to set scripts dependencies required to run the widget.
	*/
	public function get_script_depends() {
		return ['tmx-mikago_service'];
	}
	
	/**
	 * Register Mikago Service widget controls.
	 * Adds different input fields to allow the user to change and customize the widget settings.
	*/
	protected function _register_controls(){


		$posts = get_posts( 'post_type="service"&numberposts=-1' );
    $PostID = array();
    if ( $posts ) {
      foreach ( $posts as $post ) {
        $PostID[ $post->ID ] = $post->ID;
      }
    } else {
      $PostID[ __( 'No ID\'s found', 'mikago' ) ] = 0;
    }
		
		
		$this->start_controls_section(
			'section_service',
			[
				'label' => esc_html__( 'Service Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'service_style',
			[
				'label' => esc_html__( 'Service Style', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'style-one' => esc_html__( 'Style One', 'mikago-core' ),
					'style-two' => esc_html__( 'Style Two', 'mikago-core' ),
				],
				'default' => 'style-one',
				'description' => esc_html__( 'Select your service style.', 'mikago-core' ),
			]
		);
		$this->end_controls_section();// end: Section
		

		$this->start_controls_section(
			'section_service_listing',
			[
				'label' => esc_html__( 'Listing Options', 'mikago-core' ),
			]
		);
		$this->add_control(
			'service_limit',
			[
				'label' => esc_html__( 'Service Limit', 'mikago-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 3,
				'description' => esc_html__( 'Enter the number of items to show.', 'mikago-core' ),
			]
		);
		$this->add_control(
			'service_order',
			[
				'label' => __( 'Order', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'ASC' => esc_html__( 'Asending', 'mikago-core' ),
					'DESC' => esc_html__( 'Desending', 'mikago-core' ),
				],
				'default' => 'DESC',
			]
		);
		$this->add_control(
			'service_orderby',
			[
				'label' => __( 'Order By', 'mikago-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'none' => esc_html__( 'None', 'mikago-core' ),
					'ID' => esc_html__( 'ID', 'mikago-core' ),
					'author' => esc_html__( 'Author', 'mikago-core' ),
					'title' => esc_html__( 'Title', 'mikago-core' ),
					'date' => esc_html__( 'Date', 'mikago-core' ),
				],
				'default' => 'date',
			]
		);
		$this->add_control(
			'service_show_category',
			[
				'label' => __( 'Certain Categories?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => Controls_Helper_Output::get_terms_names( 'service_category'),
				'multiple' => true,
			]
		);
		$this->add_control(
			'service_show_id',
			[
				'label' => __( 'Certain ID\'s?', 'mikago-core' ),
				'type' => Controls_Manager::SELECT2,
				'default' => [],
				'options' => $PostID,
				'multiple' => true,
			]
		);
		$this->add_control(
			'short_content',
			[
				'label' => esc_html__( 'Excerpt Length', 'mikago-core' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 1,
				'step' => 1,
				'default' => 16,
				'description' => esc_html__( 'How many words you want in short content paragraph.', 'mikago-core' ),
			]
		);
		
		$this->end_controls_section();// end: Section

		$this->start_controls_section(
			'section_btn',
			[
				'label' => esc_html__( 'Service Read More', 'mikago-core' ),
				'condition' => [
						'service_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'service_more',
			[
				'label' => esc_html__( 'All Servie', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'service_btn_text',
			[
				'label' => esc_html__( 'Read More', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'Read More', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type Read More Text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->end_controls_section();// end: Section

		$this->start_controls_section(
			'section_nextprev',
			[
				'label' => esc_html__( 'Carousel nav', 'mikago-core' ),
				'condition' => [
						'service_style' => array('style-two'),
					],
			]
		);
		$this->add_control(
			'carousel_dots',
			[
				'label' => esc_html__( 'Dots', 'mikago-core' ),
				'type' => Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'mikago-core' ),
				'label_off' => esc_html__( 'Hide', 'mikago-core' ),
				'return_value' => 'true',
				'default' => 'true',
			]
		);
		$this->add_control(
			'all_service',
			[
				'label' => esc_html__( 'More Service Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'View More Service', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type Read More Text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'link_text',
			[
				'label' => esc_html__( 'Link Text', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( 'here', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type Read More Link Text here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'link',
			[
				'label' => esc_html__( 'Link', 'mikago-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => esc_html__( '#', 'mikago-core' ),
				'placeholder' => esc_html__( 'Type Read More Link here', 'mikago-core' ),
				'label_block' => true,
			]
		);
		$this->end_controls_section();// end: Section

		// Icon
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'icon_size',
			[
				'label' => esc_html__( 'Font Size', 'mikago-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 75,
						'step' => 1,
					],
				],
				'size_units' => [ 'px' ],
				'selectors' => [
					'{{WRAPPER}} .mikago-service .fi:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-service .fi:before' => 'color: {{VALUE}};',
				],
			]
		);
		
		$this->end_controls_section();// end: Section
		
		
		// Title
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => esc_html__( 'Title', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_title_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikago-service h3 a',
			]
		);
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-service h3 a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'title_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikago-service h3 a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Conent
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => esc_html__( 'Content', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label' => esc_html__( 'Typography', 'mikago-core' ),
				'name' => 'mikago_content_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .mikago-service p',
			]
		);
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-service p' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'content_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-section-s2 .grid' => 'background-color: {{VALUE}};',
				],
				'condition' => [
						'service_style' => array('style-two'),
					],
			]
		);
		$this->add_control(
			'content_border_color',
			[
				'label' => esc_html__( 'Border Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-section-s2 .grid' => 'border-color: {{VALUE}};',
				],
				'condition' => [
						'service_style' => array('style-two'),
					],
			]
		);
		$this->add_control(
			'content_padding',
			[
				'label' => esc_html__( 'Title Padding', 'mikago-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .mikago-service p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		// Read More
		$this->start_controls_section(
			'section_readmore_style',
			[
				'label' => esc_html__( 'Read More', 'mikago-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
						'service_style' => array('style-one'),
					],
			]
		);
		$this->add_control(
			'more_color',
			[
				'label' => esc_html__( 'Color', 'mikago-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .mikago-service a.more' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();// end: Section

		
	}

	/**
	 * Render Service widget output on the frontend.
	 * Written in PHP and used to generate the final HTML.
	*/
	protected function render() {
		$settings = $this->get_settings_for_display();
		$service_style = !empty( $settings['service_style'] ) ? $settings['service_style'] : '';
		$service_limit = !empty( $settings['service_limit'] ) ? $settings['service_limit'] : '';
		$carousel_dots  = ( isset( $settings['carousel_dots'] ) && ( 'true' == $settings['carousel_dots'] ) ) ? true : false;
		$carousel_dots = $carousel_dots ? ' data-dots="true"' : ' data-dots="false"';
		$service_more = !empty( $settings['service_more'] ) ? $settings['service_more'] : '';
		$service_btn_text = !empty( $settings['service_btn_text'] ) ? $settings['service_btn_text'] : '';

		$all_service = !empty( $settings['all_service'] ) ? $settings['all_service'] : '';
		$link_text = !empty( $settings['link_text'] ) ? $settings['link_text'] : '';
		$link = !empty( $settings['link'] ) ? $settings['link'] : '';

		$service_order = !empty( $settings['service_order'] ) ? $settings['service_order'] : '';
		$service_orderby = !empty( $settings['service_orderby'] ) ? $settings['service_orderby'] : '';
		$service_show_category = !empty( $settings['service_show_category'] ) ? $settings['service_show_category'] : [];
		$service_show_id = !empty( $settings['service_show_id'] ) ? $settings['service_show_id'] : [];
		$short_content = !empty( $settings['short_content'] ) ? $settings['short_content'] : '';
	
		$excerpt_length = $short_content ? $short_content : '16';

		// Turn output buffer on
		ob_start();

		// Pagination
		global $paged;
		if( get_query_var( 'paged' ) )
		  $my_page = get_query_var( 'paged' );
		else {
		  if( get_query_var( 'page' ) )
			$my_page = get_query_var( 'page' );
		  else
			$my_page = 1;
		  set_query_var( 'paged', $my_page );
		  $paged = $my_page;
		}

    if ($service_show_id) {
			$service_show_id = json_encode( $service_show_id );
			$service_show_id = str_replace(array( '[', ']' ), '', $service_show_id);
			$service_show_id = str_replace(array( '"', '"' ), '', $service_show_id);
      $service_show_id = explode(',',$service_show_id);
    } else {
      $service_show_id = '';
    }

		$args = array(
		  // other query params here,
		  'paged' => $my_page,
		  'post_type' => 'service',
		  'posts_per_page' => (int)$service_limit,
		  'category_name' => implode(',', $service_show_category),
		  'orderby' => $service_orderby,
		  'order' => $service_order,
      'post__in' => $service_show_id,
		);

		$mikago_service = new \WP_Query( $args );

		if ( $service_style == 'style-one' ) {
			$service_wrap = 'services-section';
			$slide_class = 'service-grids clearfix';
		} else {
			$service_wrap = 'services-section-s2';
			$slide_class = 'service-grids services-slider clearfix';
		}

		?>
		<section class="mikago-service <?php echo esc_attr( $service_wrap ); ?>">
        <div class="<?php echo esc_attr( $slide_class ); ?>" <?php echo $carousel_dots; ?>>
	 			<?php 
					if ( $mikago_service->have_posts()) : while ( $mikago_service->have_posts()) : $mikago_service->the_post();
					
					$service_options = get_post_meta( get_the_ID(), 'service_options', true );
	        $service_title = isset($service_options['service_title']) ? $service_options['service_title'] : '';
	        $service_icon = isset($service_options['service_icon']) ? $service_options['service_icon'] : '';
	        
					?>
			        <div class="grid">
			            <div class="icon">
			            	<?php 
		            			if( $service_icon ) { echo '<i class="fi '.esc_attr( $service_icon ).'"></i>'; }
		            		?>
			            </div>
			            	<?php 
		            			if( $service_title ) { echo '<h3><a href="'.esc_url( get_permalink() ).'">'.esc_html( $service_title ).'</a></h3>'; }
		            		?>
			             <p><?php echo wp_trim_words( get_the_content(), $excerpt_length, ' ' ); ?></p>
			             	<?php 
		            			if( $service_more ) { echo ' <a href="'.esc_url( get_permalink() ).'" class="more">'.esc_html( $service_btn_text ).'</a>'; }
		            		?>
			        </div>
			       <?php 
						  endwhile;
						  endif;
						  wp_reset_postdata();
						 ?>
			  </div>
			  <?php if ( $all_service ) { ?>
			  <div class="all-services">
		        <p><?php echo esc_html ( $all_service ); ?> <a href="<?php echo esc_url ( $link ); ?>"><?php echo esc_html ( $link_text ); ?></a></p>
		    </div>
			  <?php } ?>
		</section>
			<?php
			// Return outbut buffer
			echo ob_get_clean();	
		}
	/**
	 * Render Service widget output in the editor.
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	*/
	
	//protected function _content_template(){}
	
}
Plugin::instance()->widgets_manager->register_widget_type( new Mikago_Service() );