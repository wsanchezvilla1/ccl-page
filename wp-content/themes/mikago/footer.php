<?php
/*
 * The template for displaying the footer.
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */

$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
$mikago_ft_bg = cs_get_option('mikago_ft_bg');
$mikago_attachment = wp_get_attachment_image_src( $mikago_ft_bg , 'full' );

if ( $mikago_attachment ) {
	$bg_url = ' style="';
	$bg_url .= ( $mikago_attachment ) ? 'background-image: url( '. esc_url( $mikago_attachment ) .' );' : '';
	$bg_url .= '"';
} else {
	$bg_url = '';
}

if ( $mikago_meta ) {
	$mikago_hide_footer  = $mikago_meta['hide_footer'];
} else { $mikago_hide_footer = ''; }
if ( !$mikago_hide_footer ) { // Hide Footer Metabox
	$hide_copyright = cs_get_option('hide_copyright');
?>
	<!-- Footer -->
	<footer class="site-footer clearfix" <?php echo esc_url( $bg_url ); ?>>
		<?php
			$footer_widget_block = cs_get_option( 'footer_widget_block' );
			if ( $footer_widget_block ) {
	      get_template_part( 'theme-layouts/footer/footer', 'widgets' );
	    }
			if ( !$hide_copyright ) {
      	get_template_part( 'theme-layouts/footer/footer', 'copyright' );
	    }
    ?>
	</footer>
	<!-- Footer -->
<?php } // Hide Footer Metabox ?>
</div><!--mikago-theme-wrapper -->
<?php wp_footer(); ?>
</body>
</html>
