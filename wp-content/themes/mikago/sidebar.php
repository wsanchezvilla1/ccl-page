<?php
/*
 * The sidebar containing the main widget area.
 * Author & Copyright: blue_design
 * URL: http://themeforest.net/user/blue_design
 */
$mikago_blog_widget = cs_get_option( 'blog_widget' );
$mikago_single_blog_widget = cs_get_option( 'single_blog_widget' );
$mikago_service_widget = cs_get_option( 'single_service_widget' );
$mikago_service_sidebar_position = cs_get_option( 'service_sidebar_position' );
$mikago_project_sidebar_position = cs_get_option( 'project_sidebar_position' );
$mikago_project_widget = cs_get_option( 'single_project_widget' );
$mikago_blog_sidebar_position = cs_get_option( 'blog_sidebar_position' );
$mikago_sidebar_position = cs_get_option( 'single_sidebar_position' );
$woo_widget = cs_get_option('woo_widget');
$mikago_page_layout_shop = cs_get_option( 'woo_sidebar_position' );
$shop_sidebar_position = ( is_woocommerce_shop() ) ? $mikago_page_layout_shop : '';
if ( is_home() || is_archive() || is_search() ) {
	$mikago_blog_sidebar_position = $mikago_blog_sidebar_position;
} else {
	$mikago_blog_sidebar_position = '';
}
if ( is_single() ) {
	$mikago_sidebar_position = $mikago_sidebar_position;
} else {
	$mikago_sidebar_position = '';
}

if ( is_singular( 'service' ) ) {
	$mikago_service_sidebar_position = $mikago_service_sidebar_position;
} else {
	$mikago_service_sidebar_position = '';
}

if ( is_singular( 'project' ) ) {
	$mikago_project_sidebar_position = $mikago_project_sidebar_position;
} else {
	$mikago_project_sidebar_position = '';
}
if ( is_page() ) {
	// Page Layout Options
	$mikago_page_layout = get_post_meta( get_the_ID(), 'page_layout_options', true );
	if ( $mikago_page_layout ) {
		$mikago_page_sidebar_pos = $mikago_page_layout['page_layout'];
	} else {
		$mikago_page_sidebar_pos = '';
	}
} else {
	$mikago_page_sidebar_pos = '';
}
if (isset($_GET['sidebar'])) {
  $mikago_blog_sidebar_position = $_GET['sidebar'];
}
// sidebar class
if ( $mikago_sidebar_position === 'sidebar-left' || $mikago_page_sidebar_pos == 'left-sidebar' || $mikago_blog_sidebar_position === 'sidebar-left' ) {
	$col_class = 'col-md-pull-8';
} else {
	$col_class = '';
}

if ( $mikago_service_sidebar_position === 'sidebar-left' || $mikago_project_sidebar_position === 'sidebar-left'  ) {
	$atn_push_class = ' col-lg-pull-9';
} else {
	$atn_push_class = '';
}

if ( is_singular( 'service' ) ) {
	$service_col = ' col-lg-3 ';
	$sidebar_class = 'service-sidebar blog-sidebar';
} elseif ( is_singular( 'project' ) ) {
	$service_col = ' col-lg-4 ';
	$sidebar_class = 'project-sidebar';
} else {
	$service_col = '';
	$sidebar_class = 'blog-sidebar';
}

if (  $shop_sidebar_position == 'left-sidebar' ) {
	$shop_push_class = ' col-lg-pull-9';
} else {
	$shop_push_class = '';
}

if (  class_exists( 'WooCommerce' ) && is_shop() ) {
	$shop_col = ' shop-sidebar col-lg-4';
} else {
	$shop_col = '';
}

?>
<div class="col-md-4 <?php echo esc_attr( $col_class.$service_col.$atn_push_class.$shop_col.$shop_push_class ); ?>">
	<div class="<?php echo esc_attr( $sidebar_class ); ?>">
		<?php
		if (is_page() && isset( $mikago_page_layout['page_sidebar_widget'] ) && !empty( $mikago_page_layout['page_sidebar_widget'] ) ) {
			if ( is_active_sidebar( $mikago_page_layout['page_sidebar_widget'] ) ) {
				dynamic_sidebar( $mikago_page_layout['page_sidebar_widget'] );
			}
		} elseif (!is_page() && $mikago_blog_widget && !$mikago_single_blog_widget) {
			if ( is_active_sidebar( $mikago_blog_widget ) ) {
				dynamic_sidebar( $mikago_blog_widget );
			}
		}  elseif ( $mikago_service_widget && is_singular( 'service' ) ) {
			if ( is_active_sidebar( $mikago_service_widget ) ) {
				dynamic_sidebar( $mikago_service_widget );
			}
		} elseif ( $mikago_project_widget && is_singular( 'project' ) ) {
			if ( is_active_sidebar( $mikago_project_widget ) ) {
				dynamic_sidebar( $mikago_project_widget );
			}
		}  elseif (is_woocommerce_shop() && $woo_widget) {
			if (is_active_sidebar($woo_widget)) {
				dynamic_sidebar($woo_widget);
			}
		} elseif ( is_single() && $mikago_single_blog_widget ) {
			if ( is_active_sidebar( $mikago_single_blog_widget ) ) {
				dynamic_sidebar( $mikago_single_blog_widget );
			}
		} else {
			if ( is_active_sidebar( 'sidebar-1' ) ) {
				dynamic_sidebar( 'sidebar-1' );
			}
		} ?>
	</div>
</div><!-- #secondary -->
