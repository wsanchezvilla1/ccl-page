<?php
/*
 * The template for displaying all pages.
 * Author & Copyright: blue_design
 * URL: http://themeforest.net/user/blue_design
 */
$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
if ( $mikago_meta ) {
	$mikago_content_padding = $mikago_meta['content_spacings'];
} else { $mikago_content_padding = 'section-padding'; }
// Top and Bottom Padding
if ( $mikago_content_padding && $mikago_content_padding !== 'padding-default' ) {
	$mikago_content_top_spacings = isset( $mikago_meta['content_top_spacings'] ) ? $mikago_meta['content_top_spacings'] : '';
	$mikago_content_bottom_spacings = isset( $mikago_meta['content_bottom_spacings'] ) ? $mikago_meta['content_bottom_spacings'] : '';
	if ( $mikago_content_padding === 'padding-custom' ) {
		$mikago_content_top_spacings = $mikago_content_top_spacings ? 'padding-top:'. mikago_check_px( $mikago_content_top_spacings ) .';' : '';
		$mikago_content_bottom_spacings = $mikago_content_bottom_spacings ? 'padding-bottom:'. mikago_check_px($mikago_content_bottom_spacings) .';' : '';
		$mikago_custom_padding = $mikago_content_top_spacings . $mikago_content_bottom_spacings;
	} else {
		$mikago_custom_padding = '';
	}
	$padding_class = '';
} else {
	$mikago_custom_padding = '';
	$padding_class = '';
}

// Page Layout
$page_layout_options = get_post_meta( get_the_ID(), 'page_layout_options', true );
if ( $page_layout_options ) {
	$mikago_page_layout = $page_layout_options['page_layout'];
	$page_sidebar_widget = $page_layout_options['page_sidebar_widget'];
} else {
	$mikago_page_layout = 'right-sidebar';
	$page_sidebar_widget = '';
}
$page_sidebar_widget = $page_sidebar_widget ? $page_sidebar_widget : 'sidebar-1';
if ( $mikago_page_layout === 'extra-width' ) {
	$mikago_page_column = 'extra-width';
	$mikago_page_container = 'container-fluid';
} elseif ( $mikago_page_layout === 'full-width' ) {
	$mikago_page_column = 'col-md-12';
	$mikago_page_container = 'container ';
} elseif( ( $mikago_page_layout === 'left-sidebar' || $mikago_page_layout === 'right-sidebar' ) && is_active_sidebar( $page_sidebar_widget ) ) {
	if( $mikago_page_layout === 'left-sidebar' ){
		$mikago_page_column = 'col-md-8 order-12';
	} else {
		$mikago_page_column = 'col-md-8';
	}
	$mikago_page_container = 'container ';
} else {
	$mikago_page_column = 'col-md-12';
	$mikago_page_container = 'container ';
}
$mikago_theme_page_comments = cs_get_option( 'theme_page_comments' );
get_header();
?>
<div class="page-wrap <?php echo esc_attr( $padding_class ); ?>">
	<div class="<?php echo esc_attr( $mikago_page_container.''.$mikago_content_padding.' '.$mikago_page_layout ); ?>" style="<?php echo esc_attr( $mikago_custom_padding ); ?>">
		<div class="row">
			<div class="<?php echo esc_attr( $mikago_page_column ); ?>">
				<div class="page-wraper clearfix">
				<?php
				while ( have_posts() ) : the_post();
					the_content();
					if ( !$mikago_theme_page_comments && ( comments_open() || get_comments_number() ) ) :
						comments_template();
					endif;
				endwhile; // End of the loop.
				?>
				</div>
				<div class="page-link-wrap">
					<?php mikago_wp_link_pages(); ?>
				</div>
			</div>
			<?php
			// Sidebar
			if( ($mikago_page_layout === 'left-sidebar' || $mikago_page_layout === 'right-sidebar') && is_active_sidebar( $page_sidebar_widget )  ) {
				get_sidebar();
			}
			?>
		</div>
	</div>
</div>
<?php
get_footer();