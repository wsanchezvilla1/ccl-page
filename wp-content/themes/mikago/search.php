<?php
/*
 * The search template file.
 * Author & Copyright: blue_design
 * URL: http://themeforest.net/user/blue_design
 */
get_header();
	// Metabox
	$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
	$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
	$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
	$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
	if ( $mikago_meta ) {
		$mikago_content_padding = isset( $mikago_meta['content_spacings'] ) ? $mikago_meta['content_spacings'] : '';
	} else { $mikago_content_padding = ''; }
	// Padding - Metabox
	if ($mikago_content_padding && $mikago_content_padding !== 'padding-default') {
		$mikago_content_top_spacings = $mikago_meta['content_top_spacings'];
		$mikago_content_bottom_spacings = $mikago_meta['content_bottom_spacings'];
		if ($mikago_content_padding === 'padding-custom') {
			$mikago_content_top_spacings = $mikago_content_top_spacings ? 'padding-top:'. mikago_check_px($mikago_content_top_spacings) .';' : '';
			$mikago_content_bottom_spacings = $mikago_content_bottom_spacings ? 'padding-bottom:'. mikago_check_px($mikago_content_bottom_spacings) .';' : '';
			$mikago_custom_padding = $mikago_content_top_spacings . $mikago_content_bottom_spacings;
		} else {
			$mikago_custom_padding = '';
		}
	} else {
		$mikago_custom_padding = '';
	}
	// Theme Options
	$mikago_sidebar_position = cs_get_option( 'blog_sidebar_position' );
	$mikago_sidebar_position = $mikago_sidebar_position ?$mikago_sidebar_position : 'sidebar-right';
	$mikago_blog_widget = cs_get_option( 'blog_widget' );
	$mikago_blog_widget = $mikago_blog_widget ? $mikago_blog_widget : 'sidebar-1';

	if (isset($_GET['sidebar'])) {
	  $mikago_sidebar_position = $_GET['sidebar'];
	}

	$mikago_sidebar_position = $mikago_sidebar_position ? $mikago_sidebar_position : 'sidebar-right';

	// Sidebar Position
	if ( $mikago_sidebar_position === 'sidebar-hide' ) {
		$layout_class = 'col col col-md-10 col-md-offset-1';
		$mikago_sidebar_class = 'hide-sidebar';
	} elseif ( $mikago_sidebar_position === 'sidebar-left' && is_active_sidebar( $mikago_blog_widget ) ) {
		$layout_class = 'col col-md-8 col-md-push-3';
		$mikago_sidebar_class = 'left-sidebar';
	} elseif( is_active_sidebar( $mikago_blog_widget ) ) {
		$layout_class = 'col col-md-8';
		$mikago_sidebar_class = 'right-sidebar';
	} else {
		$layout_class = 'col col-md-12';
		$mikago_sidebar_class = 'hide-sidebar';
	}
	?>
<div class="blog-pg-section section-padding">
	<div class="container <?php echo esc_attr( $mikago_content_padding .' '. $mikago_sidebar_class ); ?>" style="<?php echo esc_attr( $mikago_custom_padding ); ?>">
		<div class="row">
			<div class="<?php echo esc_attr( $layout_class ); ?>">
				<div class="blog-content">
				<?php
				if ( have_posts() ) :
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						get_template_part( 'theme-layouts/post/content', 'search');
					endwhile;
				else :
					get_template_part( 'theme-layouts/post/content', 'none' );
				endif;
				mikago_posts_navigation();
		    wp_reset_postdata(); ?>
		    </div>
			</div><!-- Content Area -->
			<?php
			if ( $mikago_sidebar_position !== 'sidebar-hide' && is_active_sidebar( $mikago_blog_widget ) ) {
				get_sidebar(); // Sidebar
			} ?>
		</div>
	</div>
</div>
<?php
get_footer();