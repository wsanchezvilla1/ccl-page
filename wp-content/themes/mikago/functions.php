<?php
/*
 * Mikago Theme's Functions
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */

/**
 * Define - Folder Paths
 */

define( 'MIKAGO_THEMEROOT_URI', get_template_directory_uri() );
define( 'MIKAGO_CSS', MIKAGO_THEMEROOT_URI . '/assets/css' );
define( 'MIKAGO_IMAGES', MIKAGO_THEMEROOT_URI . '/assets/images' );
define( 'MIKAGO_SCRIPTS', MIKAGO_THEMEROOT_URI . '/assets/js' );
define( 'MIKAGO_FRAMEWORK', get_template_directory() . '/includes' );
define( 'MIKAGO_LAYOUT', get_template_directory() . '/theme-layouts' );
define( 'MIKAGO_CS_IMAGES', MIKAGO_THEMEROOT_URI . '/includes/theme-options/framework-extend/images' );
define( 'MIKAGO_CS_FRAMEWORK', get_template_directory() . '/includes/theme-options/framework-extend' ); // Called in Icons field *.json
define( 'MIKAGO_ADMIN_PATH', get_template_directory() . '/includes/theme-options/cs-framework' ); // Called in Icons field *.json

/**
 * Define - Global Theme Info's
 */
if (is_child_theme()) { // If Child Theme Active
	$mikago_theme_child = wp_get_theme();
	$mikago_get_parent = $mikago_theme_child->Template;
	$mikago_theme = wp_get_theme($mikago_get_parent);
} else { // Parent Theme Active
	$mikago_theme = wp_get_theme();
}
define('MIKAGO_NAME', $mikago_theme->get( 'Name' ));
define('MIKAGO_VERSION', $mikago_theme->get( 'Version' ));
define('MIKAGO_BRAND_URL', $mikago_theme->get( 'AuthorURI' ));
define('MIKAGO_BRAND_NAME', $mikago_theme->get( 'Author' ));

/**
 * All Main Files Include
 */
require_once( MIKAGO_FRAMEWORK . '/init.php' );