<?php
// Metabox
global $post;
$mikago_id    = ( isset( $post ) ) ? $post->ID : false;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
$mikago_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $mikago_id : false;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
  if ($mikago_meta) {
    $mikago_topbar_options = $mikago_meta['topbar_options'];
  } else {
    $mikago_topbar_options = '';
  }

  if ( $mikago_meta ) {
    $mikago_header_design  = $mikago_meta['select_header_design'];
  } else {
    $mikago_header_design  = cs_get_option( 'select_header_design' );
  }

 if ( $mikago_header_design === 'default' ) {
    $mikago_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $mikago_header_design_actual = ( $mikago_header_design ) ? $mikago_header_design : cs_get_option('select_header_design');
  }
  
$mikago_header_design_actual = $mikago_header_design_actual ? $mikago_header_design_actual : 'style_two';

// Define Theme Options and Metabox varials in right way!
if ($mikago_meta) {
  if ($mikago_topbar_options === 'custom' && $mikago_topbar_options !== 'default') {
    $mikago_top_left          = $mikago_meta['top_left'];
    $mikago_top_right          = $mikago_meta['top_right'];
    $mikago_hide_topbar        = $mikago_topbar_options;
    $mikago_topbar_bg          = $mikago_meta['topbar_bg'];
    if ($mikago_topbar_bg) {
      $mikago_topbar_bg = 'background-color: '. $mikago_topbar_bg .';';
    } else {$mikago_topbar_bg = '';}
  } else {
    $mikago_top_left          = cs_get_option('top_left');
    $mikago_top_right          = cs_get_option('top_right');
    $mikago_hide_topbar        = cs_get_option('top_bar');
    $mikago_topbar_bg          = '';
  }
} else {
  // Theme Options fields
  $mikago_top_left         = cs_get_option('top_left');
  $mikago_top_right          = cs_get_option('top_right');
  $mikago_hide_topbar        = cs_get_option('top_bar');
  $mikago_topbar_bg          = '';
}
// All options
if ( $mikago_meta && $mikago_topbar_options === 'custom' && $mikago_topbar_options !== 'default' ) {
  $mikago_top_right = ( $mikago_top_right ) ? $mikago_meta['top_right'] : cs_get_option('top_right');
  $mikago_top_left = ( $mikago_top_left ) ? $mikago_meta['top_left'] : cs_get_option('top_left');
} else {
  $mikago_top_right = cs_get_option('top_right');
  $mikago_top_left = cs_get_option('top_left');
}
if ( $mikago_meta && $mikago_topbar_options !== 'default' ) {
  if ( $mikago_topbar_options === 'hide_topbar' ) {
    $mikago_hide_topbar = 'hide';
  } else {
    $mikago_hide_topbar = 'show';
  }
} else {
  $mikago_hide_topbar_check = cs_get_option( 'top_bar' );
  if ( $mikago_hide_topbar_check === true ) {
     $mikago_hide_topbar = 'hide';
  } else {
     $mikago_hide_topbar = 'show';
  }
}
if ( $mikago_meta ) {
  $mikago_topbar_bg = ( $mikago_topbar_bg ) ? $mikago_meta['topbar_bg'] : '';
} else {
  $mikago_topbar_bg = '';
}
if ( $mikago_topbar_bg ) {
  $mikago_topbar_bg = 'background-color: '. $mikago_topbar_bg .';';
} else { $mikago_topbar_bg = ''; }


if( $mikago_hide_topbar === 'show' && ( $mikago_top_left || $mikago_top_right ) ) {
?>
 <div class="topbar" style="<?php echo esc_attr( $mikago_topbar_bg ); ?>">
    <div class="container">
        <div class="row">
            <div class="col col-sm-3">
               <?php echo do_shortcode( $mikago_top_left ); ?>
            </div>
            <div class="col col-sm-9">
                <?php echo do_shortcode( $mikago_top_right ); ?>
            </div>
        </div>
    </div>
</div> <!-- end topbar -->
<?php } // Hide Topbar - From Metabox