<?php
	// Metabox
	$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
	$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
	$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
	$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
	if ($mikago_meta && is_page()) {
		$mikago_title_bar_padding = $mikago_meta['title_area_spacings'];
	} else { $mikago_title_bar_padding = ''; }
	// Padding - Theme Options
	if ($mikago_title_bar_padding && $mikago_title_bar_padding !== 'padding-default') {
		$mikago_title_top_spacings = $mikago_meta['title_top_spacings'];
		$mikago_title_bottom_spacings = $mikago_meta['title_bottom_spacings'];
		if ($mikago_title_bar_padding === 'padding-custom') {
			$mikago_title_top_spacings = $mikago_title_top_spacings ? 'padding-top:'. mikago_check_px($mikago_title_top_spacings) .';' : '';
			$mikago_title_bottom_spacings = $mikago_title_bottom_spacings ? 'padding-bottom:'. mikago_check_px($mikago_title_bottom_spacings) .';' : '';
			$mikago_custom_padding = $mikago_title_top_spacings . $mikago_title_bottom_spacings;
		} else {
			$mikago_custom_padding = '';
		}
	} else {
		$mikago_title_bar_padding = cs_get_option('title_bar_padding');
		$mikago_titlebar_top_padding = cs_get_option('titlebar_top_padding');
		$mikago_titlebar_bottom_padding = cs_get_option('titlebar_bottom_padding');
		if ($mikago_title_bar_padding === 'padding-custom') {
			$mikago_titlebar_top_padding = $mikago_titlebar_top_padding ? 'padding-top:'. mikago_check_px($mikago_titlebar_top_padding) .';' : '';
			$mikago_titlebar_bottom_padding = $mikago_titlebar_bottom_padding ? 'padding-bottom:'. mikago_check_px($mikago_titlebar_bottom_padding) .';' : '';
			$mikago_custom_padding = $mikago_titlebar_top_padding . $mikago_titlebar_bottom_padding;
		} else {
			$mikago_custom_padding = '';
		}
	}
	// Banner Type - Meta Box
	if ($mikago_meta && is_page()) {
		$mikago_banner_type = $mikago_meta['banner_type'];
	} else { $mikago_banner_type = ''; }
	// Header Style
	if ($mikago_meta) {
	  $mikago_header_design  = $mikago_meta['select_header_design'];
	  $mikago_hide_breadcrumbs  = $mikago_meta['hide_breadcrumbs'];
	} else {
	  $mikago_header_design  = cs_get_option('select_header_design');
	  $mikago_hide_breadcrumbs = cs_get_option('need_breadcrumbs');
	}
	if ( $mikago_header_design === 'default') {
	  $mikago_header_design_actual  = cs_get_option('select_header_design');
	} else {
	  $mikago_header_design_actual = ( $mikago_header_design ) ? $mikago_header_design : cs_get_option('select_header_design');
	}
	if ( $mikago_header_design_actual == 'style_three') {
		$overly_class = ' overly';
	} else {
		$overly_class = ' ';
	}
	// Overlay Color - Theme Options
		if ($mikago_meta && is_page()) {
			$mikago_bg_overlay_color = $mikago_meta['titlebar_bg_overlay_color'];
			$title_color = isset($mikago_meta['title_color']) ? $mikago_meta['title_color'] : '';
		} else { $mikago_bg_overlay_color = ''; }
		if (!empty($mikago_bg_overlay_color)) {
			$mikago_bg_overlay_color = $mikago_bg_overlay_color;
			$title_color = $title_color;
		} else {
			$mikago_bg_overlay_color = cs_get_option('titlebar_bg_overlay_color');
			$title_color = cs_get_option('title_color');
		}
		$e_uniqid        = uniqid();
		$inline_style  = '';
		if ( $mikago_bg_overlay_color ) {
		 $inline_style .= '.page-title-'.$e_uniqid .'.page-title {';
		 $inline_style .= ( $mikago_bg_overlay_color ) ? 'background-color:'. $mikago_bg_overlay_color.';' : '';
		 $inline_style .= '}';
		}
		if ( $title_color ) {
		 $inline_style .= '.page-title-'.$e_uniqid .'.page-title h2, .page-title-'.$e_uniqid .'.page-title .breadcrumb li, .page-title-'.$e_uniqid .'.page-title .breadcrumbs ul li a {';
		 $inline_style .= ( $title_color ) ? 'color:'. $title_color.';' : '';
		 $inline_style .= '}';
		}
		// add inline style
		add_inline_style( $inline_style );
		$styled_class  = ' page-title-'.$e_uniqid;
	// Background - Type
	if( $mikago_meta ) {
		$title_bar_bg = $mikago_meta['title_area_bg'];
	} else {
		$title_bar_bg = '';
	}
	$mikago_custom_header = get_custom_header();
	$header_text_color = get_theme_mod( 'header_textcolor' );
	$background_color = get_theme_mod( 'background_color' );
	if( isset( $title_bar_bg['image'] ) && ( $title_bar_bg['image'] ||  $title_bar_bg['color'] ) ) {
	  extract( $title_bar_bg );
	  $mikago_background_image       = ( ! empty( $image ) ) ? 'background-image: url(' . esc_url($image) . ');' : '';
	  $mikago_background_repeat      = ( ! empty( $image ) && ! empty( $repeat ) ) ? ' background-repeat: ' . esc_attr( $repeat) . ';' : '';
	  $mikago_background_position    = ( ! empty( $image ) && ! empty( $position ) ) ? ' background-position: ' . esc_attr($position) . ';' : '';
	  $mikago_background_size    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-size: ' . esc_attr($size) . ';' : '';
	  $mikago_background_attachment    = ( ! empty( $image ) && ! empty( $size ) ) ? ' background-attachment: ' . esc_attr( $attachment ) . ';' : '';
	  $mikago_background_color       = ( ! empty( $color ) ) ? ' background-color: ' . esc_attr( $color ) . ';' : '';
	  $mikago_background_style       = ( ! empty( $image ) ) ? $mikago_background_image . $mikago_background_repeat . $mikago_background_position . $mikago_background_size . $mikago_background_attachment : '';
	  $mikago_title_bg = ( ! empty( $mikago_background_style ) || ! empty( $mikago_background_color ) ) ? $mikago_background_style . $mikago_background_color : '';
	} elseif( $mikago_custom_header->url ) {
		$mikago_title_bg = 'background-image:  url('. esc_url( $mikago_custom_header->url ) .');';
	} else {
		$mikago_title_bg = '';
	}
	if($mikago_banner_type === 'hide-title-area') { // Hide Title Area
	} elseif($mikago_meta && $mikago_banner_type === 'revolution-slider') { // Hide Title Area
		echo do_shortcode($mikago_meta['page_revslider']);
	} else {
	?>
 <!-- start page-title -->
  <section class="page-title <?php echo esc_attr( $overly_class.$styled_class.' '.$mikago_banner_type ); ?>" style="<?php echo esc_attr( $mikago_title_bg ); ?>">
  		<div class="container">
      	<div class="row">
          <div class="col col-xs-12" style="<?php echo esc_attr( $mikago_custom_padding ); ?>" >
              <div class="title">
                <h2><?php echo mikago_title_area(); ?></h2>
            	</div>
           </div>
      	</div> <!-- end row -->
  		</div> <!-- end container -->
  </section>
  <!-- end page-title -->
<?php } ?>