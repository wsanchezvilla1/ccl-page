<?php
  // Metabox
  $mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
  $mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
  $mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
  $mikago_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $mikago_id : false;
  $mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );

  // Header Style
  if ( $mikago_meta ) {
    $mikago_header_design  = $mikago_meta['select_header_design'];
    $mikago_sticky_header = isset( $mikago_meta['sticky_header'] ) ? $mikago_meta['sticky_header'] : '' ;
    $mikago_search = isset( $mikago_meta['mikago_search'] ) ? $mikago_meta['mikago_search'] : '';
  } else {
    $mikago_header_design  = cs_get_option( 'select_header_design' );
    $mikago_sticky_header  = cs_get_option( 'sticky_header' );
    $mikago_search  = cs_get_option( 'mikago_search' );
  }

  $mikago_menu_cta  = cs_get_option( 'mikago_menu_cta' );

  if ( $mikago_header_design === 'default' ) {
    $mikago_header_design_actual  = cs_get_option( 'select_header_design' );
  } else {
    $mikago_header_design_actual = ( $mikago_header_design ) ? $mikago_header_design : cs_get_option('select_header_design');
  }
  $mikago_header_design_actual = $mikago_header_design_actual ? $mikago_header_design_actual : 'style_one';

  if ( $mikago_meta && $mikago_header_design !== 'default') {
   $mikago_search = isset( $mikago_meta['mikago_search'] ) ? $mikago_meta['mikago_search'] : '';
  } else {
    $mikago_search  = cs_get_option( 'mikago_search' );
  }


  if ( $mikago_menu_cta ) {
   $cta_class = ' has-cta ';
  } else {
    $cta_class = ' not-has-cta ';
  }
  if ( $mikago_search ) {
   $search_class = ' not-has-search ';
  } else {
    $search_class = ' has-search ';
  }
  if ( has_nav_menu( 'primary' ) ) {
     $menu_padding = ' has-menu ';
  } else {
     $menu_padding = ' dont-has-menu ';
  }
  if ($mikago_meta) {
    $mikago_choose_menu = $mikago_meta['choose_menu'];
  } else { $mikago_choose_menu = ''; }
  $mikago_choose_menu = $mikago_choose_menu ? $mikago_choose_menu : '';

?>
<!-- Navigation & Search -->
 <div class="container">
    <div class="nav-row clearfix">
      <div class="navbar-header">
          <button type="button" class="open-btn">
              <span class="sr-only"><?php echo esc_html__( 'Toggle navigation','mikago' ) ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <?php get_template_part( 'theme-layouts/header/logo' ); ?>
      </div>
    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder <?php echo esc_attr( $menu_padding.$cta_class.$search_class ); ?>">
     <button class="close-navbar"><i class="ti-close"></i></button>
       <?php
          wp_nav_menu(
            array(
              'menu'              => 'primary',
              'theme_location'    => 'primary',
              'container'         => '',
              'container_class'   => '',
              'container_id'      => '',
              'menu'              => $mikago_choose_menu,
              'menu_class'        => 'nav navbar-nav',
              'fallback_cb'       => '__return_false',
            )
          );
        ?>
      </div><!-- end of nav-collapse -->
      <?php if ( $mikago_header_design_actual == 'style_two' ) { ?>
      <div class="separator"></div>
     <?php } ?>
      <?php get_template_part( 'theme-layouts/header/search','bar' ); ?>
    </div>
    <?php if ( $mikago_header_design_actual == 'style_one' ) { ?>
      <div class="separator"></div>
     <?php } ?>
  </div><!-- end of container -->


