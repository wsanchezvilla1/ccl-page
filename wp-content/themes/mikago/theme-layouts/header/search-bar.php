<?php
$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true);

$mikago_menu_cta  = cs_get_option( 'mikago_menu_cta' );
$mikago_search  = cs_get_option( 'mikago_header_search' );

$cta_text  = cs_get_option( 'cta_text' );
$cta_link  = cs_get_option( 'cta_link' );

$cta_text = ( $cta_text ) ? $cta_text : esc_html__( 'Contact Us', 'mikago' );
$cta_link = ( $cta_link ) ? $cta_link : esc_html__( '#', 'mikago' );

if ( $mikago_menu_cta ) {
  $cta_class = 'has-cta';
} else {
  $cta_class = 'not-has-cta';
}
?>
<div class="search-contact <?php echo esc_attr( $cta_class ); ?>">
    <?php if ( !$mikago_search ) { ?>
    <div class="header-search-area">
        <div class="header-search-form">
          <form method="get" id="searchform" action="<?php echo esc_url( home_url('/') ); ?>" class="form" >
            <div>
              <input type="text" name="s" id="s2" class="form-control" placeholder="<?php echo esc_attr__( 'Search here','mikago' ); ?>">
            </div>
            <button type="submit" class="btn"><i class="ti-search"></i></button>
          </form>
        </div>
        <div>
            <button class="btn open-btn"><i class="ti-search"></i></button>
        </div>
    </div>
     <?php } 
     if ( $mikago_menu_cta ) { ?>
        <div class="contact">
            <a href="<?php echo esc_attr( $cta_link ); ?>" class="theme-btn"><?php echo esc_html( $cta_text ); ?></a>
        </div>
     <?php } ?>
</div>
