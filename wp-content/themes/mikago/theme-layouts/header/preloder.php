<?php
	// Logo Image
	// Metabox - Header Transparent
	$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
	$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
	$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
	$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox'. true );
?>
 <!-- start preloader -->
 <div class="preloader">
      <div class="sk-folding-cube">
          <div class="sk-cube1 sk-cube"></div>
          <div class="sk-cube2 sk-cube"></div>
          <div class="sk-cube4 sk-cube"></div>
          <div class="sk-cube3 sk-cube"></div>
      </div>
  </div>
  <!-- end preloader -->