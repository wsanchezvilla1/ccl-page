<?php
// Metabox
global $post;
$mikago_id    = ( isset( $post ) ) ? $post->ID : false;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
$mikago_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('service') ) ? $mikago_id : false;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
// Header Style
if ( $mikago_meta ) {
  $mikago_header_design  = $mikago_meta['select_header_design'];
} else {
  $mikago_header_design  = cs_get_option( 'select_header_design' );
}

if ( $mikago_header_design === 'default' ) {
  $mikago_header_design_actual  = cs_get_option( 'select_header_design' );
} else {
  $mikago_header_design_actual = ( $mikago_header_design ) ? $mikago_header_design : cs_get_option('select_header_design');
}
$mikago_header_design_actual = $mikago_header_design_actual ? $mikago_header_design_actual : 'style_one';

$mikago_logo = cs_get_option( 'mikago_logo' );
$mikago_trlogo = cs_get_option( 'mikago_trlogo' );
$logo_url = wp_get_attachment_url( $mikago_logo );
$logo_alt = get_post_meta( $mikago_logo, '_wp_attachment_image_alt', true );
$trlogo_url = wp_get_attachment_url( $mikago_trlogo );
$trlogo_alt = get_post_meta( $mikago_trlogo, '_wp_attachment_image_alt', true );

if ( $logo_url ) {
  $logo_url = $logo_url;
} else {
 $logo_url = MIKAGO_IMAGES.'/logo.png';
}

if ( $trlogo_url ) {
  $trlogo_url = $trlogo_url;
} else {
 $trlogo_url = MIKAGO_IMAGES.'/tr-logo.png';
}

if ( $mikago_header_design_actual == 'style_one' ) {
  $mikago_logo_url = $trlogo_url;
  $mikago_logo_alt = $trlogo_alt;
} else {
  $mikago_logo_url = $logo_url;
  $mikago_logo_alt = $logo_alt;
}

if ( has_nav_menu( 'primary' ) ) {
  $logo_padding = ' has_menu ';
}
else {
   $logo_padding = ' dont_has_menu ';
}


// Logo Spacings
// Logo Spacings
$mikago_brand_logo_top = cs_get_option( 'mikago_logo_top' );
$mikago_brand_logo_bottom = cs_get_option( 'mikago_logo_bottom' );
if ( $mikago_brand_logo_top ) {
  $mikago_brand_logo_top = 'padding-top:'. mikago_check_px( $mikago_brand_logo_top ) .';';
} else { $mikago_brand_logo_top = ''; }
if ( $mikago_brand_logo_bottom ) {
  $mikago_brand_logo_bottom = 'padding-bottom:'. mikago_check_px( $mikago_brand_logo_bottom ) .';';
} else { $mikago_brand_logo_bottom = ''; }
?>
<div class="site-logo <?php echo esc_attr( $logo_padding ); ?>"  style="<?php echo esc_attr( $mikago_brand_logo_top ); echo esc_attr( $mikago_brand_logo_bottom ); ?>">
   <?php if ( $mikago_logo ) {
    ?>
      <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
       <img src="<?php echo esc_url( $mikago_logo_url ); ?>" alt=" <?php echo esc_attr( $mikago_logo_alt ); ?>">
     </a>
   <?php } elseif( has_custom_logo() ) {
      the_custom_logo();
    } else {
    ?>
    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
       <img src="<?php echo esc_url( $mikago_logo_url ); ?>" alt="<?php echo get_bloginfo('name'); ?>">
     </a>
   <?php
  } ?>
</div>