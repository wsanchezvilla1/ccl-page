<?php
/**
 * Single Project.
 */
$mikago_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$mikago_large_image = $mikago_large_image[0];
$image_alt = get_post_meta( $mikago_large_image, '_wp_attachment_image_alt', true);
$project_options = get_post_meta( get_the_ID(), 'project_options', true );
$project_page_options = get_post_meta( get_the_ID(), 'project_page_options', true );
$project_infos = isset($project_options['project_infos']) ? $project_options['project_infos'] : '';
$info_title = isset($project_options['info_title']) ? $project_options['info_title'] : '';
$accordion_box = isset($project_page_options['accordion_box']) ? $project_page_options['accordion_box'] : '';

$project_single_image = isset($project_options['project_single_image']) ? $project_options['project_single_image'] : '';

$image_url = wp_get_attachment_url( $project_single_image );
$image_alt = get_post_meta( $project_single_image , '_wp_attachment_image_alt', true);


$show_related = cs_get_option('show_related');
$title = cs_get_option('title');
$title_desc = cs_get_option('title_desc');
$project_limit = cs_get_option('project_limit');

$mikago_prev_pro = cs_get_option('prev_service');
$mikago_next_pro = cs_get_option('next_servic');
$mikago_prev_pro = ($mikago_prev_pro) ? $mikago_prev_pro : esc_html__('Previous project', 'mikago');
$mikago_next_pro = ($mikago_next_pro) ? $mikago_next_pro : esc_html__('Next project', 'mikago');
$mikago_prev_post = get_previous_post( '', false);
$mikago_next_post = get_next_post( '', false);
?>
 <div class="col col-md-8">
    <div class="img-holder">
      <div class="project-single-img-holder">
          <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
      </div>
  </div>                    
</div>
<div class="col col-md-4">
    <div class="project clearfix">
      <div class="project-info">
          <ul>
              <?php   if( $info_title ) { echo '<h3>'.esc_html( $info_title ).'</h3>'; }  ?>
              <?php foreach ( $project_infos as $key => $project_info ) { ?>
                   <li><span><?php echo esc_html( $project_info['title'] ); ?></span><?php echo esc_html( $project_info['desc'] ); ?></li>
              <?php } ?>
          </ul>
      </div>
    </div>
</div>
<div class="col col-xs-12">
  <div class="content-area">
    <div class="project-details">
        <h2><?php echo get_the_title(); ?></h2>
         <?php the_content();?>
    </div>
    <div class="challange-solution-section">
        <!-- Nav tabs -->
        <div class="panel-group theme-accordion-s1" id="accordion">
        
          <?php   
              $id = 1; foreach ( $accordion_box as $key => $box) : if ( !empty( array_filter( $box ) ) ) :
              $accordion_title = isset($box['accordion_title']) ? $box['accordion_title'] : '';
              $accordion_desc = isset($box['accordion_desc']) ? $box['accordion_desc'] : '';
              $accordion_active = isset($box['accordion_active']) ? $box['accordion_active'] : '';

              $id++;
              if ( $accordion_active ) {
                $active_class = 'in';
                $heade_class = '';
              } else {
                $active_class = '';
                $heade_class = 'collapsed';
              }
            ?>
              <div class="panel panel-default">
                <?php if ( $accordion_title ) {
                    echo'<div class="panel-heading"><a class="'.esc_attr( $heade_class ).'" data-toggle="collapse" data-parent="#accordion" href="#ac'.esc_attr( $id ).'" aria-expanded="true">'.esc_html( $accordion_title ).'</a></div>';
                  } ?>
                <div id="ac<?php echo esc_attr( $id ); ?>" class="panel-collapse collapse <?php echo esc_attr( $active_class ); ?>">
                    <div class="panel-body">
                         <?php echo wp_kses_post( $accordion_desc ); ?>
                    </div>
                </div>
            </div>
          <?php endif;  endforeach; ?>
      </div>
    </div>
        <div class="related-projects portfolio-section">
    <?php if ( $show_related ) { 
      $project_limit = $project_limit ? $project_limit : '3';
      ?>
     <div class="row">
        <div class="col col-md-4">
            <div class="section-title-s2">
                <h2><?php echo esc_html( $title ); ?></h2>
            </div>
        </div>
        <div class="col col-lg-6 col-lg-offset-1 col-md-7">
            <div class="text">
                <p><?php echo esc_html( $title_desc ); ?></p>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col col-xs-12">
          <div class="portfolio-grids portfolio-slider-s2">
            <?php $args = array(
              'post_type' => 'project',
              'posts_per_page' => (int) $project_limit,
              'post__not_in' => array(get_the_ID()), );
              $mikago_project = new WP_Query( $args );
              if ($mikago_project->have_posts()) :  while ($mikago_project->have_posts()) : $mikago_project->the_post();
              $project_options = get_post_meta( get_the_ID(), 'project_options', true );
              $project_image = isset( $project_options['project_image']) ? $project_options['project_image'] : '';
              global $post;
              $image_url = wp_get_attachment_url( $project_image );
              $image_alt = get_post_meta( $project_image , '_wp_attachment_image_alt', true);

              $project_page_options = get_post_meta( get_the_ID(), 'project_page_options', true );
              $project_title = isset( $project_options['project_title']) ? $project_options['project_title'] : ''; 

              ?>
              <div class="grid">
                  <div class="img-holder">
                       <img src="<?php echo esc_url( $image_url ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
                  </div>
                  <div class="details">
                      <div class="inner">
                          <h3><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( $project_title ); ?></a></h3>
                      </div>
                  </div>
              </div>
            <?php
              endwhile;
              wp_reset_postdata();
              endif;
            ?>
          </div>
        </div>
    </div>
    <?php } ?>
    </div>
    <div class="prev-next-project">
    <?php if ($mikago_prev_post) { ?>
        <div>
            <a href="<?php echo esc_url(get_permalink($mikago_prev_post->ID)); ?>">
                <div class="icon">
                    <i class="fi flaticon-back"></i>
                </div>
                <span><?php echo esc_attr($mikago_prev_pro); ?></span>
                <h5><?php echo esc_attr($mikago_prev_post->post_title); ?></h5>
            </a>
        </div>
        <?php } 
        if ($mikago_next_post) { ?>
        <div>
            <a href="<?php echo esc_url(get_permalink( $mikago_next_post->ID)); ?>">
                <div class="icon">
                    <i class="fi flaticon-next"></i>
                </div>
                <span><?php echo esc_attr($mikago_next_pro); ?></span>
                <h5><?php echo esc_attr($mikago_next_post->post_title); ?></h5>
            </a>
        </div>
    <?php } ?>
    </div>
  </div> 
</div>
  
 