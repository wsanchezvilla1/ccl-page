<?php
/**
 * Single Post.
 */
$mikago_large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
$mikago_large_image = $mikago_large_image[0];
$image_alt = get_post_meta( $mikago_large_image, '_wp_attachment_image_alt', true);
$mikago_post_type = get_post_meta( get_the_ID(), 'post_type_metabox', true );
// Single Theme Option
$mikago_post_pagination_option = cs_get_option('single_post_pagination');
$mikago_single_featured_image = cs_get_option('single_featured_image');
$mikago_single_author_info = cs_get_option('single_author_info');
$mikago_single_share_option = cs_get_option('single_share_option');
$mikago_metas_hide = (array) cs_get_option( 'theme_metas_hide' );
?>
  <div <?php post_class('post clearfix'); ?>>
  	<?php if ( $mikago_large_image ) { ?>
  	  <div class="entry-media">
        <img src="<?php echo esc_url( $mikago_large_image ); ?>" alt="<?php echo esc_attr( $image_alt ); ?>">
   		</div>
  	<?php	} ?>
    <ul class="entry-meta">
        <li>
          <i class="ti-time"></i>
          <a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo esc_html( get_the_date() );  ?></a>
        </li>
        <li>
        <?php
          $postcats = get_the_category();
          $count=0;
          if ( $postcats ) {
             foreach( $postcats as $cat) {
                $count++;
                echo '<i class="ti-book"></i><a href="'.esc_url( get_category_link( $cat->term_id ) ).'">'.esc_html( $cat->name ).'</a>';
                if( $count >0 ) break;
             }
            }
          ?>
        </li>
        <li>
           <i class="ti-comment-alt"></i>
           <a class="mikago-comment" href="<?php echo esc_url( get_comments_link() ); ?>">
            <?php printf( esc_html( _nx( 'Comments (%1$s)', 'Comments (%1$s)', get_comments_number(), 'comments title', 'mikago' ) ), '<span class="comment">'.number_format_i18n( get_comments_number() ).'</span>','<span>' . get_the_title() . '</span>' ); ?>
          </a>
        </li>
    </ul>
    <div class="entry-details">
	     <?php
				the_content();
				echo mikago_wp_link_pages();
			 ?>
    </div>
</div>
<?php if( has_tag() || ( $mikago_single_share_option && function_exists('mikago_wp_share_option') ) ) { ?>
<div class="tag-share clearfix">
  <?php if( has_tag() ) { ?>
     <div class="tag">
          <?php
            echo '<span>'.esc_html__('Tags:','mikago').'</span>';
            $tag_list = get_the_tags();
            if($tag_list) {
              echo the_tags( ' <ul><li>', '</li><li>', '</li></ul>' );
           } ?>
      </div>
    <?php } 
  		if ( $mikago_single_share_option && function_exists('mikago_wp_share_option') ) {
  					echo mikago_wp_share_option();
  			}
  	 ?>
</div>
<?php
}
if( !$mikago_single_author_info ) {
	mikago_author_info();
	}
?>

