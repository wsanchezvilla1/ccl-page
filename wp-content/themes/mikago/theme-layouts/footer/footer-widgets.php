<?php if( mikago_footer_widgets() ) {
?>
<!-- Footer Widgets -->
<div class="upper-footer">
	<div class="footer-widget-area">
		<div class="container">
			<div class="row">
				<div class="separator"></div>
				<?php echo mikago_footer_widgets(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Footer Widgets -->
<?php
}