<?php
/*
 * The header for our theme.
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */
?><!DOCTYPE html>
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<?php
$mikago_viewport = cs_get_option( 'theme_responsive' );
if( !$mikago_viewport ) { ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php } $mikago_all_element_color  = cs_get_customize_option( 'all_element_colors' ); ?>
<meta name="msapplication-TileColor" content="<?php echo esc_attr( $mikago_all_element_color ); ?>">
<meta name="theme-color" content="<?php echo esc_attr( $mikago_all_element_color ); ?>">
<link rel="profile" href="//gmpg.org/xfn/11">
<?php
// Metabox
global $post;
$mikago_id    = ( isset( $post ) ) ? $post->ID : false;
$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
$mikago_id    = ( ! is_tag() && ! is_archive() && ! is_search() && ! is_404() && ! is_singular('testimonial') ) ? $mikago_id : false;
$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
// Theme Layout Width
$mikago_layout_width  = cs_get_option( 'theme_layout_width' );
$theme_preloder  = cs_get_option( 'theme_preloder' );
$mikago_layout_width_class = ( $mikago_layout_width === 'container' ) ? 'layout-boxed' : 'layout-full';
// Header Style
if ( $mikago_meta ) {
  $mikago_header_design  = $mikago_meta['select_header_design'];
  $mikago_sticky_header = isset( $mikago_meta['sticky_header'] ) ? $mikago_meta['sticky_header'] : '' ;
} else {
  $mikago_header_design  = cs_get_option( 'select_header_design' );
  $mikago_sticky_header  = cs_get_option( 'sticky_header' );
}

if ( $mikago_header_design === 'default' ) {
  $mikago_header_design_actual  = cs_get_option( 'select_header_design' );
} else {
  $mikago_header_design_actual = ( $mikago_header_design ) ? $mikago_header_design : cs_get_option('select_header_design');
}

$mikago_header_design_actual = $mikago_header_design_actual ? $mikago_header_design_actual : 'style_one';

if ( $mikago_header_design_actual == 'style_three' ) {
  $header_class = 'header-style-3';
} elseif ( $mikago_header_design_actual == 'style_two' ) {
  $header_class = 'header-style-2';
}  else {
  $header_class = 'header-style-1';
}


if ( $mikago_sticky_header ) {
  $mikago_sticky_header = $mikago_sticky_header ? ' sticky-menu-on ' : '';
} else {
  $mikago_sticky_header = '';
}
// Header Transparent
if ( $mikago_meta ) {
  $mikago_transparent_header = $mikago_meta['transparency_header'];
  $mikago_transparent_header = $mikago_transparent_header ? ' header-transparent' : ' dont-transparent';
  // Shortcode Banner Type
  $mikago_banner_type = ' '. $mikago_meta['banner_type'];
} else { $mikago_transparent_header = ' dont-transparent'; $mikago_banner_type = ''; }
wp_head();
?>
</head>
<body <?php body_class(); ?>>
<div class="page-wrapper <?php echo esc_attr( $mikago_layout_width_class ); ?>"> <!-- #mikago-theme-wrapper -->
<?php if( $theme_preloder ) {
 get_template_part( 'theme-layouts/header/preloder' );
 } ?>
 <header id="header" class="site-header <?php echo esc_attr( $header_class ); ?>">
      <?php  get_template_part( 'theme-layouts/header/top','bar' ); ?>
      <nav class="navigation <?php echo esc_attr( $mikago_sticky_header ); ?> navbar navbar-default">
        <?php get_template_part( 'theme-layouts/header/menu','bar' ); ?>
      </nav>
  </header>
  <?php
  // Title Area
  $mikago_need_title_bar = cs_get_option('need_title_bar');
  if ( !$mikago_need_title_bar ) {
    get_template_part( 'theme-layouts/header/title', 'bar' );
  }
