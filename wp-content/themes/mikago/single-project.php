<?php
/*
 * The template for displaying all single posts.
 * Author & Copyright: blue_design
 * URL: http://themeforest.net/user/blue_design
 */
get_header();
	// Metabox
	$mikago_id    = ( isset( $post ) ) ? $post->ID : 0;
	$mikago_id    = ( is_home() ) ? get_option( 'page_for_posts' ) : $mikago_id;
	$mikago_id    = ( is_woocommerce_shop() ) ? wc_get_page_id( 'shop' ) : $mikago_id;
	$mikago_meta  = get_post_meta( $mikago_id, 'page_type_metabox', true );
	if ( $mikago_meta ) {
		$mikago_content_padding = $mikago_meta['content_spacings'];
	} else { $mikago_content_padding = ''; }
	// Padding - Metabox
	if ( $mikago_content_padding && $mikago_content_padding !== 'padding-default' ) {
		$mikago_content_top_spacings = $mikago_meta['content_top_spacings'];
		$mikago_content_bottom_spacings = $mikago_meta['content_bottom_spacings'];
		if ( $mikago_content_padding === 'padding-custom' ) {
			$mikago_content_top_spacings = $mikago_content_top_spacings ? 'padding-top:'. mikago_check_px($mikago_content_top_spacings) .';' : '';
			$mikago_content_bottom_spacings = $mikago_content_bottom_spacings ? 'padding-bottom:'. mikago_check_px($mikago_content_bottom_spacings) .';' : '';
			$mikago_custom_padding = $mikago_content_top_spacings . $mikago_content_bottom_spacings;
		} else {
			$mikago_custom_padding = '';
		}
	} else {
		$mikago_custom_padding = '';
	}
	// Theme Options
	$mikago_sidebar_position = cs_get_option( 'project_sidebar_position' );
	$mikago_single_comment = cs_get_option( 'project_comment_form' );
	$mikago_sidebar_position = $mikago_sidebar_position ? $mikago_sidebar_position : 'sidebar-hide';
	// Sidebar Position
	if ( $mikago_sidebar_position === 'sidebar-hide' ) {
		$mikago_layout_class = 'col-md-12 col col-xs-12';
		$mikago_sidebar_class = 'hide-sidebar';
	} elseif ( $mikago_sidebar_position === 'sidebar-left' ) {
		$mikago_layout_class = 'col col-lg-9 col-lg-push-3';
		$mikago_sidebar_class = 'left-sidebar';
	} else {
		$mikago_layout_class = 'col-lg-9';
		$mikago_sidebar_class = 'right-sidebar';
	} ?>
<div class="project-sigle-section section-padding <?php echo esc_attr( $mikago_content_padding .' '. $mikago_sidebar_class ); ?>" style="<?php echo esc_attr( $mikago_custom_padding ); ?>">
	<div class="container">
		<div class="row">
			<div class="<?php echo esc_attr( $mikago_layout_class ); ?>">
				<div class="project-single-content">
					<?php
					if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							if ( post_password_required() ) {
									echo '<div class="password-form">'.get_the_password_form().'</div>';
								} else {
									get_template_part( 'theme-layouts/post/project', 'content' );
									$mikago_single_comment = !$mikago_single_comment ? comments_template() : '';

								}
						endwhile;
					else :
						get_template_part( 'theme-layouts/post/content', 'none' );
					endif; ?>
				</div><!-- Blog Div -->
				<?php
		    wp_reset_postdata(); ?>
			</div><!-- Content Area -->
				<?php
				if ( $mikago_sidebar_position !== 'sidebar-hide' ) {
					get_sidebar(); // Sidebar
				} ?>
		</div>
	</div>
</div>
<?php
get_footer();