<?php
/*
 * All CSS and JS files are enqueued from this file
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */

/**
 * Enqueue Files for FrontEnd
 */
function mikago_google_font_url() {
    $font_url = '';
    if ( 'off' !== esc_html__( 'on', 'mikago' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Muli:400,400i,600,700,900&display=swap' ), "//fonts.googleapis.com/css" );
    }
    return $font_url;
}

if ( ! function_exists( 'mikago_scripts_styles' ) ) {
  function mikago_scripts_styles() {

    // Styles
    wp_enqueue_style( 'themify-icons', MIKAGO_CSS .'/themify-icons.css', array(), '4.6.3', 'all' );
    wp_enqueue_style( 'flaticon', MIKAGO_CSS .'/flaticon.css', array(), '1.0.0', 'all' );
    wp_enqueue_style( 'bootstrap', MIKAGO_CSS .'/bootstrap.min.css', array(), '3.3.7', 'all' );
    wp_enqueue_style( 'animate', MIKAGO_CSS .'/animate.css', array(), '3.5.1', 'all' );
    wp_enqueue_style( 'odometer', MIKAGO_CSS .'/odometer.css', array(), '0.4.8', 'all' );
    wp_enqueue_style( 'owl-carousel', MIKAGO_CSS .'/owl.carousel.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'owl-theme', MIKAGO_CSS .'/owl.theme.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'slick', MIKAGO_CSS .'/slick.css', array(), '1.6.0', 'all' );
    wp_enqueue_style( 'swiper', MIKAGO_CSS .'/swiper.min.css', array(), '4.0.7', 'all' );
    wp_enqueue_style( 'slick-theme', MIKAGO_CSS .'/slick-theme.css', array(), '1.6.0', 'all' );
    wp_enqueue_style( 'owl-transitions', MIKAGO_CSS .'/owl.transitions.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'fancybox', MIKAGO_CSS .'/fancybox.css', array(), '2.0.0', 'all' );
    wp_enqueue_style( 'mikago-style', MIKAGO_CSS .'/styles.css', array(), MIKAGO_VERSION, 'all' );
    wp_enqueue_style( 'element', MIKAGO_CSS .'/elements.css', array(), MIKAGO_VERSION, 'all' );
    if ( !function_exists('cs_framework_init') ) {
      wp_enqueue_style('mikago-default-style', get_template_directory_uri() . '/style.css', array(),  MIKAGO_VERSION, 'all' );
    }
    wp_enqueue_style( 'mikago-default-google-fonts', esc_url( mikago_google_font_url() ), array(), MIKAGO_VERSION, 'all' );
    // Scripts
    wp_enqueue_script( 'bootstrap', MIKAGO_SCRIPTS . '/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );
    wp_enqueue_script( 'imagesloaded');
    wp_enqueue_script( 'isotope', MIKAGO_SCRIPTS . '/isotope.min.js', array( 'jquery' ), '2.2.2', true );
    wp_enqueue_script( 'fancybox', MIKAGO_SCRIPTS . '/fancybox.min.js', array( 'jquery' ), '2.1.5', true );
    wp_enqueue_script( 'masonry');
    wp_enqueue_script( 'owl-carousel', MIKAGO_SCRIPTS . '/owl-carousel.js', array( 'jquery' ), '2.0.0', true );
    wp_enqueue_script( 'jquery-easing', MIKAGO_SCRIPTS . '/jquery-easing.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'wow', MIKAGO_SCRIPTS . '/wow.min.js', array( 'jquery' ), '1.4.0', true );
    wp_enqueue_script( 'odometer', MIKAGO_SCRIPTS . '/odometer.min.js', array( 'jquery' ), '0.4.8', true );
    wp_enqueue_script( 'magnific-popup', MIKAGO_SCRIPTS . '/magnific-popup.js', array( 'jquery' ), '1.1.0', true );
    wp_enqueue_script( 'slick-slider', MIKAGO_SCRIPTS . '/slick-slider.js', array( 'jquery' ), '1.6.0', true );
    wp_enqueue_script( 'swiper', MIKAGO_SCRIPTS . '/swiper.min.js', array( 'jquery' ), '4.0.7', true );
    wp_enqueue_script( 'wc-quantity-increment', MIKAGO_SCRIPTS . '/wc-quantity-increment.js', array( 'jquery' ), '1.0.0', true );
    wp_enqueue_script( 'mikago-scripts', MIKAGO_SCRIPTS . '/scripts.js', array( 'jquery' ), MIKAGO_VERSION, true );
    // Comments
    wp_enqueue_script( 'mikago-inline-validate', MIKAGO_SCRIPTS . '/jquery.validate.min.js', array( 'jquery' ), '1.9.0', true );
    wp_add_inline_script( 'mikago-validate', 'jQuery(document).ready(function($) {$("#commentform").validate({rules: {author: {required: true,minlength: 2},email: {required: true,email: true},comment: {required: true,minlength: 10}}});});' );

    // Responsive Active
    $mikago_viewport = cs_get_option('theme_responsive');
    if( !$mikago_viewport ) {
      wp_enqueue_style( 'mikago-responsive', MIKAGO_CSS .'/responsive.css', array(), MIKAGO_VERSION, 'all' );
    }

    // Adds support for pages with threaded comments
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }

  }
  add_action( 'wp_enqueue_scripts', 'mikago_scripts_styles' );
}

/**
 * Enqueue Files for BackEnd
 */
if ( ! function_exists( 'mikago_admin_scripts_styles' ) ) {
  function mikago_admin_scripts_styles() {

    wp_enqueue_style( 'mikago-admin-main', MIKAGO_CSS . '/admin-styles.css', true );
    wp_enqueue_style( 'flaticon', MIKAGO_CSS . '/flaticon.css', true );
    wp_enqueue_style( 'themify-icons', MIKAGO_CSS . '/themify-icons.css', true );
    wp_enqueue_script( 'mikago-admin-scripts', MIKAGO_SCRIPTS . '/admin-scripts.js', true );

  }
  add_action( 'admin_enqueue_scripts', 'mikago_admin_scripts_styles' );
}
