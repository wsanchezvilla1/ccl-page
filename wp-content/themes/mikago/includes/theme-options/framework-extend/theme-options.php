<?php
/*
 * All Theme Options for Mikago theme.
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */

function mikago_settings( $settings ) {

  $settings           = array(
    'menu_title'      => MIKAGO_NAME . esc_html__(' Options', 'mikago'),
    'menu_slug'       => sanitize_title(MIKAGO_NAME) . '_options',
    'menu_type'       => 'theme',
    'menu_icon'       => 'dashicons-awards',
    'menu_position'   => '4',
    'ajax_save'       => false,
    'show_reset_all'  => true,
    'framework_title' => MIKAGO_NAME .' <small>V-'. MIKAGO_VERSION .' by <a href="'. MIKAGO_BRAND_URL .'" target="_blank">'. MIKAGO_BRAND_NAME .'</a></small>',
  );

  return $settings;

}
add_filter( 'cs_framework_settings', 'mikago_settings' );

// Theme Framework Options
function mikago_options( $options ) {

  $options      = array(); // remove old options

  // ------------------------------
  // Branding
  // ------------------------------
  $options[]   = array(
    'name'     => 'mikago_theme_branding',
    'title'    => esc_html__('Brand', 'mikago'),
    'icon'     => 'fa fa-trophy',
    'sections' => array(

      // brand logo tab
      array(
        'name'     => 'brand_logo',
        'title'    => esc_html__('Logo', 'mikago'),
        'icon'     => 'fa fa-star',
        'fields'   => array(

          // Site Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Site Logo', 'mikago')
          ),
          array(
            'id'    => 'mikago_logo',
            'type'  => 'image',
            'title' => esc_html__('Default Logo', 'mikago'),
            'info'  => esc_html__('Upload your default logo here. If you not upload, then site title will load in this logo location.', 'mikago'),
            'add_title' => esc_html__('Add Logo', 'mikago'),
          ),
          array(
            'id'    => 'mikago_trlogo',
            'type'  => 'image',
            'title' => esc_html__('Transparent Logo', 'mikago'),
            'info'  => esc_html__('Upload your Transparent logo here. If you not upload, then site title will load in this logo location.', 'mikago'),
            'add_title' => esc_html__('Add Logo', 'mikago'),
          ),
          array(
            'id'          => 'mikago_logo_top',
            'type'        => 'number',
            'title'       => esc_html__('Logo Top Space', 'mikago'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),
          array(
            'id'          => 'mikago_logo_bottom',
            'type'        => 'number',
            'title'       => esc_html__('Logo Bottom Space', 'mikago'),
            'attributes'  => array( 'placeholder' => 5 ),
            'unit'        => 'px',
          ),
          // WordPress Admin Logo
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('WordPress Admin Logo', 'mikago')
          ),
          array(
            'id'    => 'brand_logo_wp',
            'type'  => 'image',
            'title' => esc_html__('Login logo', 'mikago'),
            'info'  => esc_html__('Upload your WordPress login page logo here.', 'mikago'),
            'add_title' => esc_html__('Add Login Logo', 'mikago'),
          ),
        ) // end: fields
      ), // end: section
    ),
  );

  // ------------------------------
  // Layout
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_layout',
    'title'  => esc_html__('Layout', 'mikago'),
    'icon'   => 'fa fa-file-text'
  );

  $options[]      = array(
    'name'        => 'theme_general',
    'title'       => esc_html__('General', 'mikago'),
    'icon'        => 'fa fa-wrench',

    // begin: fields
    'fields'      => array(

      // -----------------------------
      // Begin: Responsive
      // -----------------------------
       array(
        'id'    => 'theme_responsive',
        'off_text'  => 'No',
        'on_text'  => 'Yes',
        'type'  => 'switcher',
        'title' => esc_html__('Responsive', 'mikago'),
        'info' => esc_html__('Turn on if you don\'t want your site to be responsive.', 'mikago'),
        'default' => false,
      ),
      array(
        'id'    => 'theme_preloder',
        'off_text'  => 'No',
        'on_text'  => 'Yes',
        'type'  => 'switcher',
        'title' => esc_html__('Preloder', 'mikago'),
        'info' => esc_html__('Turn off if you don\'t want your site to be Preloder.', 'mikago'),
        'default' => true,
      ),
      array(
        'id'    => 'theme_layout_width',
        'type'  => 'image_select',
        'title' => esc_html__('Full Width & Extra Width', 'mikago'),
        'info' => esc_html__('Boxed or Fullwidth? Choose your site layout width. Default : Full Width', 'mikago'),
        'options'      => array(
          'container'    => MIKAGO_CS_IMAGES .'/boxed-width.jpg',
          'container-fluid'    => MIKAGO_CS_IMAGES .'/full-width.jpg',
        ),
        'default'      => 'container-fluid',
        'radio'      => true,
      ),
       array(
        'id'    => 'theme_page_comments',
        'type'  => 'switcher',
        'title' => esc_html__('Hide Page Comments?', 'mikago'),
        'label' => esc_html__('Yes! Hide Page Comments.', 'mikago'),
        'on_text' => esc_html__('Yes', 'mikago'),
        'off_text' => esc_html__('No', 'mikago'),
        'default' => false,
      ),
      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Background Options', 'mikago'),
        'dependency' => array( 'theme_layout_width_container', '==', 'true' ),
      ),
      array(
        'id'             => 'theme_layout_bg_type',
        'type'           => 'select',
        'title'          => esc_html__('Background Type', 'mikago'),
        'options'        => array(
          'bg-image' => esc_html__('Image', 'mikago'),
          'bg-pattern' => esc_html__('Pattern', 'mikago'),
        ),
        'dependency' => array( 'theme_layout_width_container', '==', 'true' ),
      ),
      array(
        'id'    => 'theme_layout_bg_pattern',
        'type'  => 'image_select',
        'title' => esc_html__('Background Pattern', 'mikago'),
        'info' => esc_html__('Select background pattern', 'mikago'),
        'options'      => array(
          'pattern-1'    => MIKAGO_CS_IMAGES . '/pattern-1.png',
          'pattern-2'    => MIKAGO_CS_IMAGES . '/pattern-2.png',
          'pattern-3'    => MIKAGO_CS_IMAGES . '/pattern-3.png',
          'pattern-4'    => MIKAGO_CS_IMAGES . '/pattern-4.png',
          'custom-pattern'  => MIKAGO_CS_IMAGES . '/pattern-5.png',
        ),
        'default'      => 'pattern-1',
        'radio'      => true,
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type', '==|==', 'true|bg-pattern' ),
      ),
      array(
        'id'      => 'custom_bg_pattern',
        'type'    => 'upload',
        'title'   => esc_html__('Custom Pattern', 'mikago'),
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type|theme_layout_bg_pattern_custom-pattern', '==|==|==', 'true|bg-pattern|true' ),
        'info' => __('Select your custom background pattern. <br />Note, background pattern image will be repeat in all x and y axis. So, please consider all repeatable area will perfectly fit your custom patterns.', 'mikago'),
      ),
      array(
        'id'      => 'theme_layout_bg',
        'type'    => 'background',
        'title'   => esc_html__('Background', 'mikago'),
        'dependency' => array( 'theme_layout_width_container|theme_layout_bg_type', '==|==', 'true|bg-image' ),
      ),

    ), // end: fields

  );

  // ------------------------------
  // Header Sections
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_header_tab',
    'title'    => esc_html__('Header', 'mikago'),
    'icon'     => 'fa fa-bars',
    'sections' => array(

      // header design tab
      array(
        'name'     => 'header_design_tab',
        'title'    => esc_html__('Design', 'mikago'),
        'icon'     => 'fa fa-magic',
        'fields'   => array(

          // Header Select
          array(
            'id'           => 'select_header_design',
            'type'         => 'image_select',
            'title'        => esc_html__('Select Header Design', 'mikago'),
            'options'      => array(
              'style_one'    => MIKAGO_CS_IMAGES .'/hs-1.png',
              'style_two'    => MIKAGO_CS_IMAGES .'/hs-2.png',
              'style_three'    => MIKAGO_CS_IMAGES .'/hs-3.png',
            ),
            'attributes' => array(
              'data-depend-id' => 'header_design',
            ),
            'radio'        => true,
            'default'   => 'style_one',
            'info' => esc_html__('Select your header design, following options will may differ based on your selection of header design.', 'mikago'),
          ),
          // Header Select

          // Extra's
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Extra\'s', 'mikago'),
          ),
          array(
            'id'    => 'sticky_header',
            'type'  => 'switcher',
            'title' => esc_html__('Sticky Header', 'mikago'),
            'info' => esc_html__('Turn On if you want your naviagtion bar on sticky.', 'mikago'),
            'default' => true,
          ),
          array(
            'id'    => 'mikago_menu_cta',
            'type'  => 'switcher',
            'title' => esc_html__('Header CTA', 'mikago'),
            'info' => esc_html__('Turn On if you want to Show Header CTA .', 'mikago'),
            'default' => false,
          ),
          array(
            'id'    => 'cta_text',
            'type'  => 'text',
            'title' => esc_html__('CTA text', 'mikago'),
            'info' => esc_html__('Write Header navigation Bar Cal To Action here.', 'mikago'),
            'default' => true,
            'dependency'  => array('mikago_menu_cta', '==', true),
          ),
          array(
            'id'    => 'cta_link',
            'type'  => 'text',
            'title' => esc_html__('CTA link', 'mikago'),
            'info' => esc_html__('Write Header navigation Bar Cal To Action Link here.', 'mikago'),
            'default' => true,
            'dependency'  => array('mikago_menu_cta', '==', true),
          ),
          array(
            'id'    => 'mikago_header_search',
            'type'  => 'switcher',
            'title' => esc_html__('Header Search', 'mikago'),
            'info' => esc_html__('Turn On if you want to Show Header Search .', 'mikago'),
            'default' => false,
          ),
        )
      ),

      // header top bar
      array(
        'name'     => 'header_top_bar_tab',
        'title'    => esc_html__('Top Bar', 'mikago'),
        'icon'     => 'fa fa-minus',
        'fields'   => array(

          array(
            'id'          => 'top_bar',
            'type'        => 'switcher',
            'title'       => esc_html__('Hide Top Bar', 'mikago'),
            'on_text'     => esc_html__('Yes', 'mikago'),
            'off_text'    => esc_html__('No', 'mikago'),
            'default'     => true,
          ),
          array(
            'id'          => 'top_left',
            'title'       => esc_html__('Top left Block', 'mikago'),
            'desc'        => esc_html__('Top bar left block.', 'mikago'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
          array(
            'id'          => 'top_right',
            'title'       => esc_html__('Top Right Block', 'mikago'),
            'desc'        => esc_html__('Top bar right block.', 'mikago'),
            'type'        => 'textarea',
            'shortcode'   => true,
            'dependency'  => array('top_bar', '==', false),
          ),
        )
      ),

      // header banner
      array(
        'name'     => 'header_banner_tab',
        'title'    => esc_html__('Title Bar (or) Banner', 'mikago'),
        'icon'     => 'fa fa-bullhorn',
        'fields'   => array(

          // Title Area
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Title Area', 'mikago')
          ),
          array(
            'id'      => 'need_title_bar',
            'type'    => 'switcher',
            'title'   => esc_html__('Title Bar ?', 'mikago'),
            'label'   => esc_html__('If you want to Hide title bar in your sub-pages, please turn this ON.', 'mikago'),
            'default'    => false,
          ),
          array(
            'id'             => 'title_bar_padding',
            'type'           => 'select',
            'title'          => esc_html__('Padding Spaces Top & Bottom', 'mikago'),
            'options'        => array(
              'padding-default' => esc_html__('Default Spacing', 'mikago'),
              'padding-custom' => esc_html__('Custom Padding', 'mikago'),
            ),
            'dependency'   => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'             => 'titlebar_top_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Top', 'mikago'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),
          array(
            'id'             => 'titlebar_bottom_padding',
            'type'           => 'text',
            'title'          => esc_html__('Padding Bottom', 'mikago'),
            'attributes' => array(
              'placeholder'     => '100px',
            ),
            'dependency'   => array( 'title_bar_padding', '==', 'padding-custom' ),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Background Options', 'mikago'),
            'dependency' => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'      => 'titlebar_bg_overlay_color',
            'type'    => 'color_picker',
            'title'   => esc_html__('Overlay Color', 'mikago'),
            'dependency' => array( 'need_title_bar', '==', 'false' ),
          ),
          array(
            'id'    => 'title_color',
            'type'  => 'color_picker',
            'title' => esc_html__('Title Color', 'mikago'),
            'dependency'   => array('banner_type', '==', 'default-title'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Breadcrumbs', 'mikago'),
          ),
         array(
            'id'      => 'need_breadcrumbs',
            'type'    => 'switcher',
            'title'   => esc_html__('Hide Breadcrumbs', 'mikago'),
            'label'   => esc_html__('If you want to hide breadcrumbs in your banner, please turn this ON.', 'mikago'),
            'default'    => false,
          ),
        )
      ),

    ),
  );

  // ------------------------------
  // Footer Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'footer_section',
    'title'    => esc_html__('Footer', 'mikago'),
    'icon'     => 'fa fa-ellipsis-h',
    'sections' => array(

      // footer widgets
      array(
        'name'     => 'footer_widgets_tab',
        'title'    => esc_html__('Widget Area', 'mikago'),
        'icon'     => 'fa fa-th',
        'fields'   => array(

          // Footer Widget Block
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Footer Widget Block', 'mikago')
          ),
          array(
            'id'    => 'footer_widget_block',
            'type'  => 'switcher',
            'title' => esc_html__('Enable Widget Block', 'mikago'),
            'info' => __('If you turn this ON, then Goto : Appearance > Widgets. There you can see <strong>Footer Widget 1,2,3 or 4</strong> Widget Area, add your widgets there.', 'mikago'),
            'default' => true,
          ),
          array(
            'id'    => 'footer_widget_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Widget Layouts', 'mikago'),
            'info' => esc_html__('Choose your footer widget theme-layouts.', 'mikago'),
            'default' => 4,
            'options' => array(
              1   => MIKAGO_CS_IMAGES . '/footer/footer-1.png',
              2   => MIKAGO_CS_IMAGES . '/footer/footer-2.png',
              3   => MIKAGO_CS_IMAGES . '/footer/footer-3.png',
              4   => MIKAGO_CS_IMAGES . '/footer/footer-4.png',
              5   => MIKAGO_CS_IMAGES . '/footer/footer-5.png',
              6   => MIKAGO_CS_IMAGES . '/footer/footer-6.png',
              7   => MIKAGO_CS_IMAGES . '/footer/footer-7.png',
              8   => MIKAGO_CS_IMAGES . '/footer/footer-8.png',
              9   => MIKAGO_CS_IMAGES . '/footer/footer-9.png',
            ),
            'radio'       => true,
            'dependency'  => array('footer_widget_block', '==', true),
          ),
           array(
            'id'    => 'mikago_ft_bg',
            'type'  => 'image',
            'title' => esc_html__('Footer Background', 'mikago'),
            'info'  => esc_html__('Upload your footer background.', 'mikago'),
            'add_title' => esc_html__('footer background', 'mikago'),
            'dependency'  => array('footer_widget_block', '==', true),
          ),

        )
      ),

      // footer copyright
      array(
        'name'     => 'footer_copyright_tab',
        'title'    => esc_html__('Copyright Bar', 'mikago'),
        'icon'     => 'fa fa-copyright',
        'fields'   => array(

          // Copyright
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Copyright Layout', 'mikago'),
          ),
         array(
            'id'    => 'hide_copyright',
            'type'  => 'switcher',
            'title' => esc_html__('Hide Copyright?', 'mikago'),
            'default' => false,
            'on_text' => esc_html__('Yes', 'mikago'),
            'off_text' => esc_html__('No', 'mikago'),
            'label' => esc_html__('Yes, do it!', 'mikago'),
          ),
          array(
            'id'    => 'footer_copyright_layout',
            'type'  => 'image_select',
            'title' => esc_html__('Select Copyright Layout', 'mikago'),
            'info' => esc_html__('In above image, blue box is copyright text and yellow box is secondary text.', 'mikago'),
            'default'      => 'copyright-3',
            'options'      => array(
              'copyright-1'    => MIKAGO_CS_IMAGES .'/footer/copyright-1.png',
              ),
            'radio'        => true,
            'dependency'     => array('hide_copyright', '!=', true),
          ),
          array(
            'id'    => 'copyright_text',
            'type'  => 'textarea',
            'title' => esc_html__('Copyright Text', 'mikago'),
            'shortcode' => true,
            'dependency' => array('hide_copyright', '!=', true),
            'after'       => 'Helpful shortcodes: [current_year] [home_url] or any shortcode.',
          ),

          // Copyright Another Text
          array(
            'type'    => 'notice',
            'class'   => 'warning cs-mikago-heading',
            'content' => esc_html__('Copyright Secondary Text', 'mikago'),
             'dependency'     => array('hide_copyright', '!=', true),
          ),
          array(
            'id'    => 'secondary_text',
            'type'  => 'textarea',
            'title' => esc_html__('Secondary Text', 'mikago'),
            'shortcode' => true,
            'dependency'     => array('hide_copyright', '!=', true),
          ),

        )
      ),

    ),
  );

  // ------------------------------
  // Design
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_design',
    'title'  => esc_html__('Design', 'mikago'),
    'icon'   => 'fa fa-magic'
  );

  // ------------------------------
  // color section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_color_section',
    'title'    => esc_html__('Colors', 'mikago'),
    'icon'     => 'fa fa-eyedropper',
    'fields' => array(

      array(
        'type'    => 'heading',
        'content' => esc_html__('Color Options', 'mikago'),
      ),
      array(
        'type'    => 'subheading',
        'wrap_class' => 'color-tab-content',
        'content' => esc_html__('All color options are available in our theme customizer. The reason of we used customizer options for color section is because, you can choose each part of color from there and see the changes instantly using customizer. Highly customizable colors are in Appearance > Customize', 'mikago'),
      ),

    ),
  );

  // ------------------------------
  // Typography section
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_typo_section',
    'title'    => esc_html__('Typography', 'mikago'),
    'icon'     => 'fa fa-header',
    'fields' => array(

      // Start fields
      array(
        'id'                  => 'typography',
        'type'                => 'group',
        'fields'              => array(
          array(
            'id'              => 'title',
            'type'            => 'text',
            'title'           => esc_html__('Title', 'mikago'),
          ),
          array(
            'id'              => 'selector',
            'type'            => 'textarea',
            'title'           => esc_html__('Selector', 'mikago'),
            'info'           => wp_kses( __('Enter css selectors like : <strong>body, .custom-class</strong>', 'mikago'), array( 'strong' => array() ) ),
          ),
          array(
            'id'              => 'font',
            'type'            => 'typography',
            'title'           => esc_html__('Font Family', 'mikago'),
          ),
          array(
            'id'              => 'size',
            'type'            => 'text',
            'title'           => esc_html__('Font Size', 'mikago'),
          ),
          array(
            'id'              => 'line_height',
            'type'            => 'text',
            'title'           => esc_html__('Line-Height', 'mikago'),
          ),
          array(
            'id'              => 'css',
            'type'            => 'textarea',
            'title'           => esc_html__('Custom CSS', 'mikago'),
          ),
        ),
        'button_title'        => esc_html__('Add New Typography', 'mikago'),
        'accordion_title'     => esc_html__('New Typography', 'mikago'),
      ),

      // Subset
      array(
        'id'                  => 'subsets',
        'type'                => 'select',
        'title'               => esc_html__('Subsets', 'mikago'),
        'class'               => 'chosen',
        'options'             => array(
          'latin'             => 'latin',
          'latin-ext'         => 'latin-ext',
          'cyrillic'          => 'cyrillic',
          'cyrillic-ext'      => 'cyrillic-ext',
          'greek'             => 'greek',
          'greek-ext'         => 'greek-ext',
          'vietnamese'        => 'vietnamese',
          'devanagari'        => 'devanagari',
          'khmer'             => 'khmer',
        ),
        'attributes'         => array(
          'data-placeholder' => 'Subsets',
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( 'latin' ),
      ),

      array(
        'id'                  => 'font_weight',
        'type'                => 'select',
        'title'               => esc_html__('Font Weights', 'mikago'),
        'class'               => 'chosen',
        'options'             => array(
          '100'   => esc_html__('Thin 100', 'mikago'),
          '100i'  => esc_html__('Thin 100 Italic', 'mikago'),
          '200'   => esc_html__('Extra Light 200', 'mikago'),
          '200i'  => esc_html__('Extra Light 200 Italic', 'mikago'),
          '300'   => esc_html__('Light 300', 'mikago'),
          '300i'  => esc_html__('Light 300 Italic', 'mikago'),
          '400'   => esc_html__('Regular 400', 'mikago'),
          '400i'  => esc_html__('Regular 400 Italic', 'mikago'),
          '500'   => esc_html__('Medium 500', 'mikago'),
          '500i'  => esc_html__('Medium 500 Italic', 'mikago'),
          '600'   => esc_html__('Semi Bold 600', 'mikago'),
          '600i'  => esc_html__('Semi Bold 600 Italic', 'mikago'),
          '700'   => esc_html__('Bold 700', 'mikago'),
          '700i'  => esc_html__('Bold 700 Italic', 'mikago'),
          '800'   => esc_html__('Extra Bold 800', 'mikago'),
          '800i'  => esc_html__('Extra Bold 800 Italic', 'mikago'),
          '900'   => esc_html__('Black 900', 'mikago'),
          '900i'  => esc_html__('Black 900 Italic', 'mikago'),
        ),
        'attributes'         => array(
          'data-placeholder' => esc_html__('Font Weight', 'mikago'),
          'multiple'         => 'multiple',
          'style'            => 'width: 200px;'
        ),
        'default'             => array( '400' ),
      ),

      // Custom Fonts Upload
      array(
        'id'                 => 'font_family',
        'type'               => 'group',
        'title'              => esc_html__('Upload Custom Fonts', 'mikago'),
        'button_title'       => esc_html__('Add New Custom Font', 'mikago'),
        'accordion_title'    => esc_html__('Adding New Font', 'mikago'),
        'accordion'          => true,
        'desc'               => esc_html__('It is simple. Only add your custom fonts and click to save. After you can check "Font Family" selector. Do not forget to Save!', 'mikago'),
        'fields'             => array(

          array(
            'id'             => 'name',
            'type'           => 'text',
            'title'          => esc_html__('Font-Family Name', 'mikago'),
            'attributes'     => array(
              'placeholder'  => esc_html__('for eg. Arial', 'mikago')
            ),
          ),

          array(
            'id'             => 'ttf',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .ttf <small><i>(optional)</i></small>', 'mikago'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'mikago'),
              'button_title' => wp_kses(__('Upload <i>.ttf</i>', 'mikago'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'eot',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .eot <small><i>(optional)</i></small>', 'mikago'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'mikago'),
              'button_title' => wp_kses(__('Upload <i>.eot</i>', 'mikago'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'otf',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .otf <small><i>(optional)</i></small>', 'mikago'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'mikago'),
              'button_title' => wp_kses(__('Upload <i>.otf</i>', 'mikago'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'woff',
            'type'           => 'upload',
            'title'          => wp_kses(__('Upload .woff <small><i>(optional)</i></small>', 'mikago'), array( 'small' => array(), 'i' => array() )),
            'settings'       => array(
              'upload_type'  => 'font',
              'insert_title' => esc_html__('Use this Font-Format', 'mikago'),
              'button_title' =>wp_kses(__('Upload <i>.woff</i>', 'mikago'), array( 'i' => array() )),
            ),
          ),

          array(
            'id'             => 'css',
            'type'           => 'textarea',
            'title'          => wp_kses(__('Extra CSS Style <small><i>(optional)</i></small>', 'mikago'), array( 'small' => array(), 'i' => array() )),
            'attributes'     => array(
              'placeholder'  => esc_html__('for eg. font-weight: normal;', 'mikago'),
            ),
          ),

        ),
      ),
      // End All field

    ),
  );

  // ------------------------------
  // Pages
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_pages',
    'title'  => esc_html__('Pages', 'mikago'),
    'icon'   => 'fa fa-files-o'
  );

  // ------------------------------
  // Service Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'service_section',
    'title'    => esc_html__('Service', 'mikago'),
    'icon'     => 'fa fa-th-list',
    'fields' => array(

      // Team Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Service Single', 'mikago')
      ),
      array(
          'id'             => 'service_sidebar_position',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Position', 'mikago'),
          'options'        => array(
            'sidebar-right' => esc_html__('Right', 'mikago'),
            'sidebar-left' => esc_html__('Left', 'mikago'),
            'sidebar-hide' => esc_html__('Hide', 'mikago'),
          ),
          'default_option' => 'Select sidebar position',
          'info'          => esc_html__('Default option : Right', 'mikago'),
        ),
        array(
          'id'             => 'single_service_widget',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Widget', 'mikago'),
          'options'        => mikago_registered_sidebars(),
          'default_option' => esc_html__('Select Widget', 'mikago'),
          'dependency'     => array('service_sidebar_position', '!=', 'sidebar-hide'),
          'info'          => esc_html__('Default option : Main Widget Area', 'mikago'),
        ),
        array(
          'id'    => 'service_comment_form',
          'type'  => 'switcher',
          'title' => esc_html__('Comment Area/Form', 'mikago'),
          'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this OFF.', 'mikago'),
          'default' => true,
        ),
    ),
  );


  // ------------------------------
  // Project Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'project_section',
    'title'    => esc_html__('Project', 'mikago'),
    'icon'     => 'fa fa-medkit',
    'fields' => array(

      // Team Start
      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Project Single', 'mikago')
      ),
      array(
          'id'             => 'project_sidebar_position',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Position', 'mikago'),
          'options'        => array(
            'sidebar-right' => esc_html__('Right', 'mikago'),
            'sidebar-left' => esc_html__('Left', 'mikago'),
            'sidebar-hide' => esc_html__('Hide', 'mikago'),
          ),
          'default_option' => 'Select sidebar position',
          'info'          => esc_html__('Default option : Right', 'mikago'),
        ),
        array(
          'id'             => 'single_project_widget',
          'type'           => 'select',
          'title'          => esc_html__('Sidebar Widget', 'mikago'),
          'options'        => mikago_registered_sidebars(),
          'default_option' => esc_html__('Select Widget', 'mikago'),
          'dependency'     => array('project_sidebar_position', '!=', 'sidebar-hide'),
          'info'          => esc_html__('Default option : Main Widget Area', 'mikago'),
        ),
        array(
          'id'    => 'project_comment_form',
          'type'  => 'switcher',
          'title' => esc_html__('Comment Area/Form', 'mikago'),
          'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this OFF.', 'mikago'),
          'default' => true,
        ),

        array(
            'id'    => 'show_related',
            'type'  => 'switcher',
            'title' => esc_html__('Show Related Project?', 'mikago'),
            'default' => false,
            'on_text' => esc_html__('Yes', 'mikago'),
            'off_text' => esc_html__('No', 'mikago'),
            'label' => esc_html__('Yes, do it!', 'mikago'),
          ),
          array(
            'id'    => 'title',
            'type'  => 'text',
            'title' => esc_html__('Title Text', 'mikago'),
            'shortcode' => true,
            'dependency' => array('show_related', '!=', false),
            'after'       => 'Check related project,doen for client!',
          ),
          array(
            'id'    => 'title_desc',
            'type'  => 'textarea',
            'title' => esc_html__('Desctription Text', 'mikago'),
            'shortcode' => true,
            'dependency' => array('show_related', '!=', false),
            'after'       => 'Related Project Desctription',
          ),
          array(
            'id'    => 'project_limit',
            'type'  => 'text',
            'title' => esc_html__('Project Limit', 'mikago'),
            'shortcode' => true,
            'dependency' => array('show_related', '!=', false),
            'after'       => 'Related Project Limit',
          ),

    ),
  );

  // ------------------------------
  // Blog Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'blog_section',
    'title'    => esc_html__('Blog', 'mikago'),
    'icon'     => 'fa fa-edit',
    'sections' => array(

      // blog general section
      array(
        'name'     => 'blog_general_tab',
        'title'    => esc_html__('General', 'mikago'),
        'icon'     => 'fa fa-cog',
        'fields'   => array(

          // Layout
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Layout', 'mikago')
          ),
          array(
            'id'             => 'blog_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'mikago'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'mikago'),
              'sidebar-left' => esc_html__('Left', 'mikago'),
              'sidebar-hide' => esc_html__('Hide', 'mikago'),
            ),
            'default_option' => 'Select sidebar position',
            'help'          => esc_html__('This style will apply, default blog pages - Like : Archive, Category, Tags, Search & Author.', 'mikago'),
            'info'          => esc_html__('Default option : Right', 'mikago'),
          ),
          array(
            'id'             => 'blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'mikago'),
            'options'        => mikago_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'mikago'),
            'dependency'     => array('blog_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'mikago'),
          ),
          // Layout
          // Global Options
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Global Options', 'mikago')
          ),
          array(
            'id'         => 'theme_exclude_categories',
            'type'       => 'checkbox',
            'title'      => esc_html__('Exclude Categories', 'mikago'),
            'info'      => esc_html__('Select categories you want to exclude from blog page.', 'mikago'),
            'options'    => 'categories',
          ),
          array(
            'id'      => 'theme_blog_excerpt',
            'type'    => 'text',
            'title'   => esc_html__('Excerpt Length', 'mikago'),
            'info'   => esc_html__('Blog short content length, in blog listing pages.', 'mikago'),
            'default' => '55',
          ),
          array(
            'id'      => 'theme_metas_hide',
            'type'    => 'checkbox',
            'title'   => esc_html__('Meta\'s to hide', 'mikago'),
            'info'    => esc_html__('Check items you want to hide from blog/post meta field.', 'mikago'),
            'class'      => 'horizontal',
            'options'    => array(
              'category'   => esc_html__('Category', 'mikago'),
              'date'    => esc_html__('Date', 'mikago'),
              'author'     => esc_html__('Author', 'mikago'),
              'comments'      => esc_html__('Comments', 'mikago'),
              'Tag'      => esc_html__('Tag', 'mikago'),
            ),
            // 'default' => '30',
          ),
          // End fields

        )
      ),

      // blog single section
      array(
        'name'     => 'blog_single_tab',
        'title'    => esc_html__('Single', 'mikago'),
        'icon'     => 'fa fa-sticky-note',
        'fields'   => array(

          // Start fields
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Enable / Disable', 'mikago')
          ),
          array(
            'id'    => 'single_featured_image',
            'type'  => 'switcher',
            'title' => esc_html__('Featured Image', 'mikago'),
            'info' => esc_html__('If need to hide featured image from single blog post page, please turn this OFF.', 'mikago'),
            'default' => true,
          ),
           array(
            'id'    => 'single_author_info',
            'type'  => 'switcher',
            'title' => esc_html__('Author Info', 'mikago'),
            'info' => esc_html__('If need to hide author info on single blog page, please turn this On.', 'mikago'),
            'default' => false,
          ),
          array(
            'id'    => 'single_share_option',
            'type'  => 'switcher',
            'title' => esc_html__('Share Option', 'mikago'),
            'info' => esc_html__('If need to hide share option on single blog page, please turn this OFF.', 'mikago'),
            'default' => true,
          ),
          array(
            'id'    => 'single_comment_form',
            'type'  => 'switcher',
            'title' => esc_html__('Comment Area/Form ?', 'mikago'),
            'info' => esc_html__('If need to hide comment area and that form on single blog page, please turn this On.', 'mikago'),
            'default' => false,
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Sidebar', 'mikago')
          ),
          array(
            'id'             => 'single_sidebar_position',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Position', 'mikago'),
            'options'        => array(
              'sidebar-right' => esc_html__('Right', 'mikago'),
              'sidebar-left' => esc_html__('Left', 'mikago'),
              'sidebar-hide' => esc_html__('Hide', 'mikago'),
            ),
            'default_option' => 'Select sidebar position',
            'info'          => esc_html__('Default option : Right', 'mikago'),
          ),
          array(
            'id'             => 'single_blog_widget',
            'type'           => 'select',
            'title'          => esc_html__('Sidebar Widget', 'mikago'),
            'options'        => mikago_registered_sidebars(),
            'default_option' => esc_html__('Select Widget', 'mikago'),
            'dependency'     => array('single_sidebar_position', '!=', 'sidebar-hide'),
            'info'          => esc_html__('Default option : Main Widget Area', 'mikago'),
          ),
          // End fields

        )
      ),

    ),
  );

if (class_exists( 'WooCommerce' )){
  // ------------------------------
  // WooCommerce Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'woocommerce_section',
    'title'    => esc_html__('WooCommerce', 'mikago'),
    'icon'     => 'fa fa-shopping-cart',
    'fields' => array(

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Layout', 'mikago')
      ),
     array(
        'id'             => 'woo_product_columns',
        'type'           => 'select',
        'title'          => esc_html__('Product Column', 'mikago'),
        'options'        => array(
          2 => esc_html__('Two Column', 'mikago'),
          3 => esc_html__('Three Column', 'mikago'),
          4 => esc_html__('Four Column', 'mikago'),
        ),
        'default_option' => esc_html__('Select Product Columns', 'mikago'),
        'help'          => esc_html__('This style will apply, default woocommerce shop and archive pages.', 'mikago'),
      ),
      array(
        'id'             => 'woo_sidebar_position',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Position', 'mikago'),
        'options'        => array(
          'right-sidebar' => esc_html__('Right', 'mikago'),
          'left-sidebar' => esc_html__('Left', 'mikago'),
          'sidebar-hide' => esc_html__('Hide', 'mikago'),
        ),
        'default_option' => esc_html__('Select sidebar position', 'mikago'),
        'info'          => esc_html__('Default option : Right', 'mikago'),
      ),
      array(
        'id'             => 'woo_widget',
        'type'           => 'select',
        'title'          => esc_html__('Sidebar Widget', 'mikago'),
        'options'        => mikago_registered_sidebars(),
        'default_option' => esc_html__('Select Widget', 'mikago'),
        'dependency'     => array('woo_sidebar_position', '!=', 'sidebar-hide'),
        'info'          => esc_html__('Default option : Shop Page', 'mikago'),
      ),

      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Listing', 'mikago')
      ),
      array(
        'id'      => 'theme_woo_limit',
        'type'    => 'text',
        'title'   => esc_html__('Product Limit', 'mikago'),
        'info'   => esc_html__('Enter the number value for per page products limit.', 'mikago'),
      ),
      // End fields

      // Start fields
      array(
        'type'    => 'notice',
        'class'   => 'info cs-mikago-heading',
        'content' => esc_html__('Single Product', 'mikago')
      ),
      array(
        'id'             => 'woo_related_limit',
        'type'           => 'text',
        'title'          => esc_html__('Related Products Limit', 'mikago'),
      ),
      array(
        'id'    => 'woo_single_upsell',
        'type'  => 'switcher',
        'title' => esc_html__('You May Also Like', 'mikago'),
        'info' => esc_html__('If you don\'t want \'You May Also Like\' products in single product page, please turn this ON.', 'mikago'),
        'default' => false,
      ),
      array(
        'id'    => 'woo_single_related',
        'type'  => 'switcher',
        'title' => esc_html__('Related Products', 'mikago'),
        'info' => esc_html__('If you don\'t want \'Related Products\' in single product page, please turn this ON.', 'mikago'),
        'default' => false,
      ),
      // End fields

    ),
  );
}

  // ------------------------------
  // Extra Pages
  // ------------------------------
  $options[]   = array(
    'name'     => 'theme_extra_pages',
    'title'    => esc_html__('Extra Pages', 'mikago'),
    'icon'     => 'fa fa-clone',
    'sections' => array(

      // error 404 page
      array(
        'name'     => 'error_page_section',
        'title'    => esc_html__('404 Page', 'mikago'),
        'icon'     => 'fa fa-exclamation-triangle',
        'fields'   => array(

          // Start 404 Page
          array(
            'id'    => 'error_heading',
            'type'  => 'text',
            'title' => esc_html__('404 Page Heading', 'mikago'),
            'info'  => esc_html__('Enter 404 page heading.', 'mikago'),
          ),
          array(
            'id'    => 'error_subheading',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Sub Heading', 'mikago'),
            'info'  => esc_html__('Enter 404 page Sub heading.', 'mikago'),
          ),
          array(
            'id'    => 'error_page_content',
            'type'  => 'textarea',
            'title' => esc_html__('404 Page Content', 'mikago'),
            'info'  => esc_html__('Enter 404 page content.', 'mikago'),
            'shortcode' => true,
          ),
          array(
            'id'    => 'error_page_bg',
            'type'  => 'image',
            'title' => esc_html__('404 Page Background', 'mikago'),
            'info'  => esc_html__('Choose 404 page background styles.', 'mikago'),
            'add_title' => esc_html__('Add 404 Image', 'mikago'),
          ),
          array(
            'id'    => 'error_btn_text',
            'type'  => 'text',
            'title' => esc_html__('Button Text', 'mikago'),
            'info'  => esc_html__('Enter BACK TO HOME button text. If you want to change it.', 'mikago'),
          ),
          // End 404 Page

        ) // end: fields
      ), // end: fields section

      // maintenance mode page
      array(
        'name'     => 'maintenance_mode_section',
        'title'    => esc_html__('Maintenance Mode', 'mikago'),
        'icon'     => 'fa fa-hourglass-half',
        'fields'   => array(

          // Start Maintenance Mode
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('If you turn this ON : Only Logged in users will see your pages. All other visiters will see, selected page of : <strong>Maintenance Mode Page</strong>', 'mikago')
          ),
          array(
            'id'             => 'enable_maintenance_mode',
            'type'           => 'switcher',
            'title'          => esc_html__('Maintenance Mode', 'mikago'),
            'default'        => false,
          ),
          array(
            'id'             => 'maintenance_mode_page',
            'type'           => 'select',
            'title'          => esc_html__('Maintenance Mode Page', 'mikago'),
            'options'        => 'pages',
            'default_option' => esc_html__('Select a page', 'mikago'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          array(
            'id'             => 'maintenance_mode_title',
            'type'           => 'text',
            'title'          => esc_html__('Maintenance Mode Text', 'mikago'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          array(
            'id'             => 'maintenance_mode_text',
            'type'           => 'textarea',
            'title'          => esc_html__('Maintenance Mode Text', 'mikago'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          array(
            'id'             => 'maintenance_mode_bg',
            'type'           => 'background',
            'title'          => esc_html__('Page Background', 'mikago'),
            'dependency'   => array( 'enable_maintenance_mode', '==', 'true' ),
          ),
          // End Maintenance Mode

        ) // end: fields
      ), // end: fields section

    )
  );

  // ------------------------------
  // Advanced
  // ------------------------------
  $options[] = array(
    'name'   => 'theme_advanced',
    'title'  => esc_html__('Advanced', 'mikago'),
    'icon'   => 'fa fa-cog'
  );

  // ------------------------------
  // Misc Section
  // ------------------------------
  $options[]   = array(
    'name'     => 'misc_section',
    'title'    => esc_html__('Misc', 'mikago'),
    'icon'     => 'fa fa-recycle',
    'sections' => array(

      // custom sidebar section
      array(
        'name'     => 'custom_sidebar_section',
        'title'    => esc_html__('Custom Sidebar', 'mikago'),
        'icon'     => 'fa fa-reorder',
        'fields'   => array(

          // start fields
          array(
            'id'              => 'custom_sidebar',
            'title'           => esc_html__('Sidebars', 'mikago'),
            'desc'            => esc_html__('Go to Appearance -> Widgets after create sidebars', 'mikago'),
            'type'            => 'group',
            'fields'          => array(
              array(
                'id'          => 'sidebar_name',
                'type'        => 'text',
                'title'       => esc_html__('Sidebar Name', 'mikago'),
              ),
              array(
                'id'          => 'sidebar_desc',
                'type'        => 'text',
                'title'       => esc_html__('Custom Description', 'mikago'),
              )
            ),
            'accordion'       => true,
            'button_title'    => esc_html__('Add New Sidebar', 'mikago'),
            'accordion_title' => esc_html__('New Sidebar', 'mikago'),
          ),
          // end fields

        )
      ),
      // custom sidebar section

      // Custom CSS/JS
      array(
        'name'        => 'custom_css_js_section',
        'title'       => esc_html__('Custom Codes', 'mikago'),
        'icon'        => 'fa fa-code',

        // begin: fields
        'fields'      => array(
          // Start Custom CSS/JS
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Custom JS', 'mikago')
          ),
          array(
            'id'             => 'theme_custom_js',
            'type'           => 'textarea',
            'attributes' => array(
              'rows'     => 10,
              'placeholder'     => esc_html__('Enter your JS code here...', 'mikago'),
            ),
          ),
          // End Custom CSS/JS

        ) // end: fields
      ),

      // Translation
      array(
        'name'        => 'theme_translation_section',
        'title'       => esc_html__('Translation', 'mikago'),
        'icon'        => 'fa fa-language',

        // begin: fields
        'fields'      => array(

          // Start Translation
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Common Texts', 'mikago')
          ),
          array(
            'id'          => 'read_more_text',
            'type'        => 'text',
            'title'       => esc_html__('Read More Text', 'mikago'),
          ),
          array(
            'id'          => 'view_more_text',
            'type'        => 'text',
            'title'       => esc_html__('View More Text', 'mikago'),
          ),
          array(
            'id'          => 'share_text',
            'type'        => 'text',
            'title'       => esc_html__('Share Text', 'mikago'),
          ),
          array(
            'id'          => 'share_on_text',
            'type'        => 'text',
            'title'       => esc_html__('Share On Tooltip Text', 'mikago'),
          ),
          array(
            'id'          => 'author_text',
            'type'        => 'text',
            'title'       => esc_html__('Author Text', 'mikago'),
          ),
          array(
            'id'          => 'post_comment_text',
            'type'        => 'text',
            'title'       => esc_html__('Post Comment Text [Submit Button]', 'mikago'),
          ),
          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('WooCommerce', 'mikago')
          ),
          array(
            'id'          => 'add_to_cart_text',
            'type'        => 'text',
            'title'       => esc_html__('Add to Cart Text', 'mikago'),
          ),
          array(
            'id'          => 'details_text',
            'type'        => 'text',
            'title'       => esc_html__('Details Text', 'mikago'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Pagination', 'mikago')
          ),
          array(
            'id'          => 'older_post',
            'type'        => 'text',
            'title'       => esc_html__('Older Posts Text', 'mikago'),
          ),
          array(
            'id'          => 'newer_post',
            'type'        => 'text',
            'title'       => esc_html__('Newer Posts Text', 'mikago'),
          ),

          array(
            'type'    => 'notice',
            'class'   => 'info cs-mikago-heading',
            'content' => esc_html__('Single Portfolio Pagination', 'mikago')
          ),
          array(
            'id'          => 'prev_port',
            'type'        => 'text',
            'title'       => esc_html__('Prev Case Text', 'mikago'),
          ),
          array(
            'id'          => 'next_port',
            'type'        => 'text',
            'title'       => esc_html__('Next Case Text', 'mikago'),
          ),
          // End Translation

        ) // end: fields
      ),

    ),
  );

 
  // ------------------------------
  // backup                       -
  // ------------------------------
  $options[]   = array(
    'name'     => 'backup_section',
    'title'    => 'Backup',
    'icon'     => 'fa fa-shield',
    'fields'   => array(

      array(
        'type'    => 'notice',
        'class'   => 'warning',
        'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'mikago'),
      ),

      array(
        'type'    => 'backup',
      ),

    )
  );

  return $options;

}
add_filter( 'cs_framework_options', 'mikago_options' );