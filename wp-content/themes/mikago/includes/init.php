<?php
/*
 * All Mikago Theme Related Functions Files are Linked here
 * Author & Copyright:blue_design
 * URL: http://themeforest.net/user/blue_design
 */

/* Theme All Mikago Setup */
require_once( MIKAGO_FRAMEWORK . '/theme-support.php' );
require_once( MIKAGO_FRAMEWORK . '/backend-functions.php' );
require_once( MIKAGO_FRAMEWORK . '/frontend-functions.php' );
require_once( MIKAGO_FRAMEWORK . '/enqueue-files.php' );
require_once( MIKAGO_CS_FRAMEWORK . '/custom-style.php' );
require_once( MIKAGO_CS_FRAMEWORK . '/config.php' );

/* Install Plugins */
require_once( MIKAGO_FRAMEWORK . '/theme-options/plugins/activation.php' );

/* Breadcrumbs */
require_once( MIKAGO_FRAMEWORK . '/theme-options/plugins/breadcrumb-trail.php' );

/* Aqua Resizer */
require_once( MIKAGO_FRAMEWORK . '/theme-options/plugins/aq_resizer.php' );

/* Bootstrap Menu Walker */
require_once( MIKAGO_FRAMEWORK . '/core/wp_bootstrap_navwalker.php' );

/* Sidebars */
require_once( MIKAGO_FRAMEWORK . '/core/sidebars.php' );

if ( class_exists( 'WooCommerce' ) ) :
	require_once( MIKAGO_FRAMEWORK . '/woocommerce-extend.php' );
endif;